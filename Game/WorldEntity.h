//**************************************************************************************************
//
// WorldEntity.h: interface for the CWorldEntity class.
//
// Description: World entity base class
//
// Created by: Keith Ditchburn 12/03/2003
//
//**************************************************************************************************
#if !defined(AFX_WORLDENTITY_H__AC3A83C8_5654_42C6_B3C1_8394825C885E__INCLUDED_)
#define AFX_WORLDENTITY_H__AC3A83C8_5654_42C6_B3C1_8394825C885E__INCLUDED_

#pragma once

#include "toolbox.h"
#include "Vector3.h"

class CWorldEntity
{
public:
	enum ESide
	{
		eSideNeutral,
		eSidePlayer,
		eSideEnemy
	};
private:
	int m_graphicId;
	int m_animationFrames;
	float m_currentAnimationFrame;
	int m_collisionCost; // damage to others caused by us
	int m_health; // our health

	int m_maxHealth; // maximum health - I think only used by player so could refactor down

	int m_lifeTime;
	int m_spriteWidth;
	int m_spriteHeight;
	float m_animationSpeed;
	int m_animationMode;
	bool m_animationIncrement;

	// Positions are centre of entity
	CVector3 m_centrePosition;
	CVector3 m_lastCentrePosition;

	CVector3 m_rotation; // yaw,pitch and roll

	float m_speed;
	float m_rotationSpeed;
	float m_collisionCircleRadiusSq;


	DWORD m_lastUpdateTime;
	ESide m_side;

	bool m_moved;
	bool m_active;

	char *m_name;

	int m_bulletTemplateId;
	int m_explosionTemplateId;

	bool m_invincible;
	CVector3 worldPos;
protected:
	DWORD m_lastFireTime;
	int m_fireRate;
	bool m_onlyFireForward;

	void CalculateWorldPosition(CVector3 *worldPos,float dt);

	inline int GetLifeTime() const {return m_lifeTime;}
	inline int GetExplosionTemplateId() const {return m_explosionTemplateId;}
	inline int GetBulletTemplateId() const {return m_bulletTemplateId;}
	void MoveForward();
	void DieNow();
private:
	void Explode();
	virtual void UpdateSpecific()=0;
public:

	CWorldEntity(const char*);
	virtual ~CWorldEntity()=0;

	inline void SetFireRate(int rate){m_fireRate=rate;};
	inline void SetOnlyFireForward(bool set) {m_onlyFireForward=set;}

	inline void SetLifeTime(int lifeTime) {m_lifeTime=lifeTime;}

	inline void SetBulletTemplateId(int id){m_bulletTemplateId=id;}
	inline void SetExplosionTemplateId(int id){m_explosionTemplateId=id;}

	bool TakeDamage(int cost);
	void SetGraphicId(int newId);

	void SetToLastPosition();

	void SetStartCentrePosition(const CVector3 &pos);
	void SetCentrePosition(const CVector3 &pos);

	void SetRotationY(float y);

	inline const CVector3 & GetCentrePosition() const {return m_centrePosition;}
	inline const CVector3 & GetLastCentrePosition() const {return m_lastCentrePosition;}

	CVector3 & GetRotation();
	void RotateAroundAxisY(float rot);

	void Update();
	bool Fire();

	bool CheckForCollision(CWorldEntity *otherEntity);

	void Reset();

	//virtual void Move(const CVector3& dir) {OutputDebugString("Not implemented\n");} // only for player
	virtual void CantMoveThere(bool collidedWithPlayer);
	virtual void Render(float dt);
	virtual void ResetSpecific(){m_active=false;}
	virtual int GetScoreReward() const {return 0;}

	inline void SetSide(ESide newSide) {m_side=newSide;}
	inline ESide GetSide() const {return m_side;}

	inline void SetHealth(int health) {m_health=min(health,m_maxHealth);}
	inline void SetMaxHealth(int maxHealth) {m_maxHealth=maxHealth;}
	inline int GetHealth() const {return m_health;}
	inline int GetMaxHealth() const {return m_maxHealth;}

	inline void SetActive(bool set) {m_active=set;}

	inline int GetWidth() const {return m_spriteWidth;}
	inline int GetHeight() const {return m_spriteHeight;}
	inline float GetHalfWidth() const {return 0.5f*GetWidth();}
	inline float GetHalfHeight() const {return 0.5f*GetHeight();}

	inline void SetRotationSpeed(float sp) {m_rotationSpeed=sp;}
	inline float GetRotationSpeed() const {return m_rotationSpeed;}

	inline void SetSpeed(float sp) {m_speed=sp;}
	inline float GetSpeed() const {return m_speed;}

	inline void SetCollisionCost(int cost) {m_collisionCost=cost;}
	inline int GetCollisionCost() const {return m_collisionCost;}

	inline bool CanBeCollidedWith() const {if (m_collisionCost!=0 && m_active) return true;return false;}
	inline float GetCollisionCircleRadiusSq() const {return m_collisionCircleRadiusSq;}

	inline int GetGraphicId() const {return m_graphicId;}
	inline bool GetActive() const {return m_active;}

	inline const char* GetName() const {return m_name;}


	virtual void HandlePreDeathActions(){}
	void MakeInvincible(bool set) {m_invincible=set;}

};

#endif // !defined(AFX_WORLDENTITY_H__AC3A83C8_5654_42C6_B3C1_8394825C885E__INCLUDED_)
