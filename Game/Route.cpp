#include "Common.h"
#include "route.h"


CRoute::CRoute(const char *name) : m_numWayPoints(0), m_wayPoints(0)
{
	assert(name);
	m_name = new char[strlen(name) + 1];
	strcpy(m_name, name);
}

CRoute::~CRoute(void)
{
	delete []m_name;
	if (m_wayPoints)
		delete []m_wayPoints;
}

void CRoute::Initialise(int numPoints)
{
	if (m_wayPoints)
		delete []m_wayPoints;

	m_wayPoints=new CVector3[numPoints];
	memset(m_wayPoints, 0, sizeof(CVector3) * numPoints);

	m_numWayPoints=numPoints;
}

void CRoute::AddWayPoint(int index,const CVector3 &pnt)
{
	assert(index >= 0 && index < m_numWayPoints);
	m_wayPoints[index] = pnt;
}
