#pragma once

#include <SDL.h>

class CRectangle;

// Sprite class
// A sprite can have many surfaces e.g. for animation, direction etc.
// Can also have collision masks
// Perhaps z value?
class CSprite
{
private:

    char *m_name;

    SDL_Surface* m_surface;
	int m_currentSurface;

	int m_numFrames;
	int m_frameSize;
	float m_frameSpeed;

	int m_spriteWidth;
	int m_spriteHeight;

	int m_frameMode;
public:

	CSprite(const char*name);
	~CSprite(void);

	void AddSurface(SDL_Surface* surface, bool createCollisionMask, int numFrames,
                        int frameSize, float frameSpeed, int mode);

	void Render(SDL_Surface *destination, int x, int y, SDL_Rect* rect=0);
	void RenderAnimate(SDL_Surface *destination, int x, int y, int frameNumber);

	// gets
	inline int GetAnimationData(float *frameSpeed, int *frameMode) const
	{
		*frameSpeed=m_frameSpeed;
		*frameMode=m_frameMode;
		return m_numFrames;
	}

	inline const char* GetName() const {return m_name;}

	void Get2DDimensions(int* width, int* height) const;

private:
	void GetFrameRect(int frameNumber,SDL_Rect *rect);

};
