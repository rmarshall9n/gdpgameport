#include "Common.h"
#include "ai.h"
#include "WorldEntityManager.h"
#include "WorldModel.h"
#include "WorldEntity.h"
#include "WorldEntityEnemy.h"
#include "Wave.h"
#include "Route.h"
#include "PowerUpManager.h"

#include "Visualisation.h"
#include "BulletSystem.h"
#include "ExplosionManager.h"
#include "Level.h"

#include <string.h>

const DWORD kWaitTimeAfterWaveCleared=2*1000;
const DWORD kTimeAtEndOfLevel=8*1000;

CAi::CAi(void): m_maxEnemies(0),m_enemyEntityStartIndex(0),m_currentWave(-1),m_currentLevel(0),
    m_currentWaveStatus(eWaveStatusDead),m_timeOfLastWave(0),m_paused(false)
{
	m_enemyTemplates.reserve(64);
	m_currentEnemyTemplateId=0;
	m_waveVector.reserve(32);
	m_routeVector.reserve(32);
	m_levelVector.reserve(8);
}

CAi::~CAi(void)
{
//ry	EsOutput(_T("AI  CWorldEntityEnemy Vector grew to %d\n"),m_enemyTemplates.size());

	std::vector <CWorldEntityEnemy*>::iterator p;
	for (p=m_enemyTemplates.begin();p!=m_enemyTemplates.end();++p)
		delete (*p);
	m_enemyTemplates.clear();

//ry	EsOutput(_T("AI  m_waveVector Vector grew to %d\n"),m_waveVector.size());

	std::vector <CWave*>::iterator q;
	for (q=m_waveVector.begin();q!=m_waveVector.end();++q)
		delete (*q);
	m_waveVector.clear();

//ry	EsOutput(_T("AI  m_routeVector Vector grew to %d\n"),m_routeVector.size());

	std::vector <CRoute*>::iterator r;
	for (r=m_routeVector.begin();r!=m_routeVector.end();++r)
		delete (*r);
	m_routeVector.clear();

	std::vector <CLevel*>::iterator l;
	for (l=m_levelVector.begin();l!=m_levelVector.end();++l)
		delete (*l);
	m_levelVector.clear();
}

void CAi::Reset()
{
	m_timeOfLastWave=0;
	m_currentWave=0;
	//	m_nextWave=0;
	m_paused=false;
	m_currentWaveStatus=eWaveStatusDead;
	m_currentLevel=0;
	m_levelVector[m_currentLevel]->BeginLevel();
}

// there are a number of types of enemy so an array of graphic ids is passed in, these are then set when fireing
void CAi::Initialise(int enemyEntityStartIndex, int maxEnemies)
{
	m_maxEnemies=maxEnemies;
	m_enemyEntityStartIndex=enemyEntityStartIndex;
}

CWorldEntityEnemy* CAi::SpawnEnemy(int enemyTemplateId)
{
	assert(enemyTemplateId>=0);

	for (int i=m_enemyEntityStartIndex;i<m_enemyEntityStartIndex+m_maxEnemies;i++)
	{
		CWorldEntityEnemy* ent=(CWorldEntityEnemy*)WORLD.GetEntity(i);
		assert(ent);
		if (!ent->GetActive())
		{
			// Copy values from template
			CWorldEntityEnemy *enemyTemplate=m_enemyTemplates[enemyTemplateId];
			assert(enemyTemplate);

			ent->InitFromTemplate(enemyTemplate);

			return ent;
		}
	}

	return NULL;
}

void CAi::Update(CPowerUpManager *gPowerUpManager)
{
	assert(gPowerUpManager);

	if (m_paused)
		return;

	Uint32 timeNow = SDL_GetTicks();

	switch(m_currentWaveStatus)
	{
	case eWaveStatusSpawning:
		if (m_waveVector[m_currentWave]->Update())
		{
			m_currentWaveStatus=eWaveStatusActive;
			m_timeOfLastWaveSpawning=SDL_GetTicks();
		}
		break;
	case eWaveStatusActive:
		{
			bool spawnNextWave=false;

			if (m_waveVector[m_currentWave]->IsWaveFinished())
			{
				m_currentWaveStatus=eWaveStatusDead;
				spawnNextWave=true;

			}
			else
			{
				// We can spawn a new wave before the previous one is finished depending on the settings
				if (timeNow-m_timeOfLastWaveSpawning>m_levelVector[m_currentLevel]->GetTimeUntilNextWave())
				{
					// skip dead delay and go straight to spawn
					m_currentWaveStatus=eWaveStatusSpawnNew;
					spawnNextWave=true;
				}
			}

			if (spawnNextWave)
			{
				m_timeOfLastWaveFinished=SDL_GetTicks();
				m_levelVector[m_currentLevel]->AdvanceToNextWave();
			}
		}
		break;

	case eWaveStatusDead:
		// all members of wave are dead - player killed them before end of wave time - wait a few seconds
		if (timeNow-m_timeOfLastWaveFinished>kWaitTimeAfterWaveCleared)
			m_currentWaveStatus=eWaveStatusSpawnNew;
		break;

	case eWaveStatusSpawnNew:
		{
			if (m_levelVector[m_currentLevel]->HaveAllWavesBeenLaunched())
			{
				// All the waves have been spawned and the last one (the boss) has died
				// Level up by killing any existing enemies, waiting a mo and then loading a new one
				m_currentWaveStatus=eWaveStatusLevelEnd;

				KillAllRemainingEnemies();

				m_timeWhenLevelFinished=timeNow;

				break;
			}

			// Launch the wave
			LaunchWave(m_levelVector[m_currentLevel]->GetCurrentWaveId());

			// Set the chance of power up
			gPowerUpManager->SetChanceOfPowerUp(m_levelVector[m_currentLevel]->GetChanceOfPowerUp());

			m_currentWaveStatus=eWaveStatusSpawning;

			break;
		}


	case eWaveStatusLevelEnd:
		// Level is finished - just waiting for a moment for enemies to die etc.
		if (timeNow-m_timeWhenLevelFinished>kTimeAtEndOfLevel)
		{
			// start next level
			m_currentLevel++;
			if (m_currentLevel>=(int)m_levelVector.size())
			{
//ry				EsOutput(_T("***** All levels finished - Restarting ******\n"));
				m_currentLevel=0;
				m_levelVector[m_currentLevel]->BeginLevel();
			}
			//m_nextWave=0;
			m_currentWaveStatus=eWaveStatusDead;
		}
		break;
	default:
//ry		EsError(_T("Unknown wave status enum"));
            break;
	}
}

int CAi::AddEnemyTemplate(const TEnemyEntityChunk& data)
{
	assert(data.entityName);
	assert(data.bulletName);
	assert(data.graphicName);
	assert(data.explosionName);

	// Retrieve graphic id from viz first
	int entityGraphicId=VIZ.GetSpriteIdByName(data.graphicName);
	if (entityGraphicId==-1)
	{
//ry		EsOutput(_T("CAi: enemy graphic not found in viz\n"));
		return -1;
	}

	// Retrieve bullet id from bullet system
	int bulletId=WORLD.GetBulletSystem()->GetBulletTemplateIdByName(data.bulletName);
	if (bulletId==-1)
	{
//ry		EsOutput(_T("CAi: bullet not found\n"));
		return -1;
	}

	// Retrieve explosion id from explosion manager
	int explosionId=WORLD.GetExplosionManager()->GetExplosionTemplateIdByName(data.explosionName);
	if (bulletId==-1)
	{
//ry		EsOutput(_T("CAi: explosion not found\n"));
		return -1;
	}

	CWorldEntityEnemy *newEnemyTemplate=new CWorldEntityEnemy(data.entityName);

	newEnemyTemplate->SetCollisionCost(data.collisionCost);
	newEnemyTemplate->SetGraphicId(entityGraphicId);
	newEnemyTemplate->SetExplosionTemplateId(explosionId);
	newEnemyTemplate->SetBulletTemplateId(bulletId);
	newEnemyTemplate->SetHealth(data.health);
	newEnemyTemplate->SetSpeed(data.moveSpeed);
	newEnemyTemplate->SetRotationSpeed(data.rotSpeed);
	newEnemyTemplate->SetOnlyFireForward(data.onlyFireForward==1);
	newEnemyTemplate->SetFireRate(data.fireRate);
	newEnemyTemplate->SetChanceOfFiring(data.chanceOfFiring);
	newEnemyTemplate->SetScoreReward(data.score);

	m_enemyTemplates.push_back(newEnemyTemplate);

	return (int)(m_enemyTemplates.size()-1);
}


int CAi::AddEnemyTemplate(const char *entityName,
        const char *graphicName,
        const char *bulletName,
        const char *explosionName,
        const int collisionCost,
        const int health,
        const float moveSpeed,
        const float rotSpeed,
        const int onlyFireForward, // 1 or 0
        const int fireRate, // ms
        const int chanceOfFiring,
        const int score)
{
	assert(entityName);
	assert(graphicName);
	assert(bulletName);
	assert(explosionName);

	// Retrieve graphic id from viz first
	int entityGraphicId=VIZ.GetSpriteIdByName(graphicName);
	if (entityGraphicId==-1)
	{
//ry		EsOutput(_T("CAi: enemy graphic not found in viz\n"));
		return -1;
	}

	// Retrieve bullet id from bullet system
	int bulletId=WORLD.GetBulletSystem()->GetBulletTemplateIdByName(bulletName);
	if (bulletId==-1)
	{
//ry		EsOutput(_T("CAi: bullet not found\n"));
		return -1;
	}

	// Retrieve explosion id from explosion manager
	int explosionId=WORLD.GetExplosionManager()->GetExplosionTemplateIdByName(explosionName);
	if (bulletId==-1)
	{
//ry		EsOutput(_T("CAi: explosion not found\n"));
		return -1;
	}

	CWorldEntityEnemy *newEnemyTemplate=new CWorldEntityEnemy(entityName);

	newEnemyTemplate->SetCollisionCost(collisionCost);
	newEnemyTemplate->SetGraphicId(entityGraphicId);
	newEnemyTemplate->SetExplosionTemplateId(explosionId);
	newEnemyTemplate->SetBulletTemplateId(bulletId);
	newEnemyTemplate->SetHealth(health);
	newEnemyTemplate->SetSpeed(moveSpeed);
	newEnemyTemplate->SetRotationSpeed(rotSpeed);
	newEnemyTemplate->SetOnlyFireForward(onlyFireForward==1);
	newEnemyTemplate->SetFireRate(fireRate);
	newEnemyTemplate->SetChanceOfFiring(chanceOfFiring);
	newEnemyTemplate->SetScoreReward(score);

	m_enemyTemplates.push_back(newEnemyTemplate);

	return (int)(m_enemyTemplates.size()-1);
}

int CAi::GetEnemyTemplateIdByName(const char* name) const
{
	assert(name);

	std::vector <CWorldEntityEnemy*>::const_iterator p;
	int count=0;
	for (p=m_enemyTemplates.begin();p!=m_enemyTemplates.end();++p)
	{
		if (strcmp(name,(*p)->GetName())==0)
			return count;

		count++;
	}
	return -1;
}

int CAi::AddWave(TWaveData *waveData)
{
	assert(waveData);


    const float caanooScreenWidth = 320.0f;
    const float caanooScreenHeight = 240.0f;

    const float ratioX=(caanooScreenWidth/500.0f);
    const float ratioY=(caanooScreenHeight/600.0f);

	for (int i=0;i<waveData->numWayPoints;i++)
	{
		waveData->wayPoints[i].x *= ratioX;
		waveData->wayPoints[i].y *= ratioY;
	}

	CWave *newWave=new CWave;
	newWave->Initialise(waveData);

	m_waveVector.push_back(newWave);

	return (int)(m_waveVector.size()-1);
}


void CAi::LaunchWave(int index)
{
	assert(index>=0 && index<(int)m_waveVector.size());
	m_currentWave=index;
	m_waveVector[m_currentWave]->Start();
}

int CAi::AddRoute(const TRouteChunk &route)
{
	CRoute *newRoute=new CRoute(route.routeName);
	newRoute->Initialise(route.numWayPoints);
	for (int i=0;i<route.numWayPoints;i++)
		newRoute->AddWayPoint(i,route.wayPoints[i]);
	m_routeVector.push_back(newRoute);

	return (int)(m_routeVector.size()-1);
}

int CAi::AddRoute(char *routeName,
        int numWayPoints,
        CVector3 *wayPoints)
{
	CRoute *newRoute=new CRoute(routeName);
	newRoute->Initialise(numWayPoints);
	for (int i=0;i<numWayPoints;i++)
		newRoute->AddWayPoint(i,wayPoints[i]);
	m_routeVector.push_back(newRoute);

	return (int)(m_routeVector.size()-1);
}


void CAi::AddLevel(CLevel *level)
{
	assert(level);
	m_levelVector.push_back(level);
}

int CAi::GetRouteIdByName(const char* name)
{
	assert(name);

	std::vector <CRoute*>::const_iterator p;
	int count=0;
	for (p=m_routeVector.begin();p!=m_routeVector.end();++p)
	{
		if (strcmp(name,(*p)->GetName())==0)
			return count;

		count++;
	}
	return -1;
}

void CAi::KillAllRemainingEnemies()
{
	std::vector <CWave*>::iterator q;
	for (q=m_waveVector.begin();q!=m_waveVector.end();++q)
	{
		(*q)->Kill();
	}
}
