//**************************************************************************************************
//
// WorldEntityEnemy.h: interface for the CWorldEntityEnemy class.
//
// Description: Enemy entity. Moves around the world randomly.
//
// Created by: Keith Ditchburn 15/03/2003
//
//**************************************************************************************************

#if !defined(AFX_WORLDENTITYENEMY_H__0E1F1741_984A_4476_B3B8_84450F05B94B__INCLUDED_)
#define AFX_WORLDENTITYENEMY_H__0E1F1741_984A_4476_B3B8_84450F05B94B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WorldEntity.h"



class CWorldEntityEnemy : public CWorldEntity
{
private:
	enum EEnemyMode
	{
		eEnemyModeRotating,
		eEnemyModeMoving
	};

	void ChangeMode(EEnemyMode newMode);
	bool RotateTowardDestination();
	void GetNewDestination();
	void UpdateSpecific();
	void SetDestination(const CVector3& dest);
	void ReachedWavePoint();

	CVector3 m_destination;
	CVector3 m_lastDestination;
	EEnemyMode m_currentMode;
	DWORD m_timeOfModeChange;

	CVector3* m_wayPoints;
	int m_numWayPoints;
	int m_currentWayPoint;
	int m_chanceOfFiring;
	int m_scoreReward;

public:
	void SetChanceOfFiring(int chance){m_chanceOfFiring=chance;}

	void CantMoveThere(bool collidedWithPlayer);
	bool AreWeAtDestination();


	CWorldEntityEnemy(const char *name);
	~CWorldEntityEnemy();

	void ResetSpecific();

	void InitFromTemplate(const CWorldEntityEnemy *other);

	void TravelWayPoints(CVector3 *pnts,int numPoints);

	void SetScoreReward(int sw) {m_scoreReward=sw;}
	int GetScoreReward() const {return m_scoreReward;}
};

#endif // !defined(AFX_WORLDENTITYENEMY_H__0E1F1741_984A_4476_B3B8_84450F05B94B__INCLUDED_)
