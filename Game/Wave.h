#pragma once

#include "Vector3.h"

struct TWaveData
{
	char *name;
	unsigned int entryRate; // in ticks
	int numEnemies;
	int *enemyTemplateIds;
	int numWayPoints;
	CVector3 *wayPoints;
};

class CWorldEntityEnemy;

// Handle waves of enemies
class CWave
{
private:
	TWaveData *m_waveData;

	int m_numEntitiesSpawned;
	unsigned int m_tickOfLastSpawning;

	void SpawnEnemy();

	CWorldEntityEnemy **m_entitiesInWave;
public:
	CWave(void);
	~CWave(void);

	void Initialise(TWaveData *waveData);
	void Start();
	bool Update();

	bool IsWaveFinished();
	void Kill();
};
