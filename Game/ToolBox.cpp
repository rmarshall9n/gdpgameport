#include "Common.h"
#include ".\toolbox.h"

char gPath[MAX_PATH];

// Replacement for splitpath for WINCE
#define NUL '\0'
void psplit(char *path, char *drv, char *dir, char *fname, char *ext)
{
      char ch, *ptr, *p;

      /* convert slashes to backslashes for searching       */

      for (ptr = path; *ptr; ++ptr)
      {
            if ('/' == *ptr)
                  *ptr = '\\';
      }

      /* look for drive spec                                */

      if (NULL != (ptr = strchr(path, ':')))
      {
            ++ptr;
            if (drv)
            {
                  strncpy(drv, path, ptr - path);
                  drv[ptr - path] = NUL;
            }
            path = ptr;
      }
      else if (drv)
            *drv = NUL;

      /* find rightmost backslash or leftmost colon         */

      if (NULL == (ptr = strrchr(path, '\\')))
            ptr = (strchr(path, ':'));

      if (!ptr)
      {
            ptr = path;             /* obviously, no path   */
            if (dir)
                  *dir = NUL;
      }
      else
      {
            ++ptr;                  /* skip the delimiter   */
            if (dir)
            {
                  ch = *ptr;
                  *ptr = NUL;
                  strcpy(dir, path);
                  *ptr = ch;
            }
      }

      if (NULL == (p = strrchr(ptr, '.')))
      {
            if (fname)
                  strcpy(fname, ptr);
            if (ext)
                  *ext = NUL;
      }
      else
      {
            *p = NUL;
            if (fname)
                  strcpy(fname, ptr);
            *p = '.';
            if (ext)
                  strcpy(ext, p);
      }
}

CToolBox::CToolBox(void)
{
}

CToolBox::~CToolBox(void)
{
}


void CToolBox::GetRootDirectory(int bufSize,char *buf)
{
	// WindowsCE does not have GetCurrentDirectory so create a replacement
#if defined(CAANOO)
//ry	char *charVersion=SH::CStringHelpers::tocharString("Program Files");
//ry	_tcscpy_s(buf,bufSize,charVersion);
//ry	free(charVersion);
#else
	GetCurrentDirectory(bufSize,buf);
#endif
}

// replacement for splitpath
void CToolBox::SplitThePath(const char *path, char *drv, char *dir, char *fname, char *ext)
{
	#if defined(CAANOO)
	//ry
	/*std::string pathStr=SH::CStringHelpers::toNarrowString(path);

	char pathChar[MAX_PATH];
	strncpy(pathChar,pathStr.c_str(),MAX_PATH);

	char drvChar[MAX_PATH];
	char dirChar[MAX_PATH];
	char fnameChar[MAX_PATH];
	char exchar[MAX_PATH];

	psplit(pathChar,drvChar,dirChar,fnameChar,exchar);

	// TBD: a bit painful perhaps
	MultiByteToWideChar( CP_ACP , 0 , drvChar , MAX_PATH ,drv , MAX_PATH ) ;
	MultiByteToWideChar( CP_ACP , 0 , dirChar , MAX_PATH ,dir , MAX_PATH ) ;
	MultiByteToWideChar( CP_ACP , 0 , fnameChar , MAX_PATH ,fname , MAX_PATH ) ;
	MultiByteToWideChar( CP_ACP , 0 , exchar , MAX_PATH ,ext , MAX_PATH ) ;
*/
	#else
	_splitpath(path,drv,dir,fname,ext);
	#endif
}


// Note: Uses static to create path
char* CToolBox::MakePath(const char *pathIn)
{
	strncpy(gPath,pathIn,strlen(pathIn));
	gPath[strlen(pathIn)]=0;

	PointPath(MAX_PATH,gPath);
	return gPath;
}

// Path depends on running on WINCE, EMULATOR etc.
void CToolBox::PointPath(int bufSize,char *path)
{
#if defined(WINCE)

#if defined(EMULATOR)
	const char root[]=_T("\\storage card\\");
	char buf[MAX_PATH];
	_tcsncpy(buf,path,strlen(path));
	buf[strlen(path)]=0;

	_tcsncpy(path,root,strlen(root));
	path[strlen(root)]=0;

	_tcscat(path,buf);
#else
	const char root[]=_T("\\Program Files\\dataPPC\\");

	char buf[MAX_PATH];
	_tcsncpy(buf,path,strlen(path));
	buf[strlen(path)]=0;

	_tcsncpy(path,root,strlen(root));
	path[strlen(root)]=0;

	_tcscat(path,buf);
#endif

#else
	// nothing to do for PC platform
#endif
}


// PC screen is 500 by 600, PPC is 240 by 320
const float ratioX=(240.0f/500.0f);
const float ratioY=(320.0f/600.0f);

float CToolBox::ConvertValueFloatX(float xin)
{
#if defined(WINCE)
	return (xin*ratioX);
#else
	return xin;
#endif
}

float CToolBox::ConvertValueFloatY(float yin)
{
#if defined(WINCE)
	return (yin*ratioY);
#else
	return yin;
#endif
}

int CToolBox::ConvertValueX(int xin)
{
#if defined(WINCE)
	return (int)(xin*ratioX);
#else
	return xin;
#endif
}

int CToolBox::ConvertValueY(int yin)
{
#if defined(WINCE)
	return (int)(ratioY*yin);
#else
	return yin;
#endif
}

float CToolBox::RandRangeFloat(float min,float max)
{
	float range=max-min;

	int r=rand()%1000;

	float result=min+((range*r)*0.001f);

	return result;
}
