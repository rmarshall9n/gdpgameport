#pragma once

struct TWaveSettings
{
	int waveId;
	int chanceOfPowerUp;
	DWORD timeUntilNextWave;
};

/**
 * \class CLevel
 * \brief Data for a level
 * \author Keith Ditchburn \date 01 October 2005
*/
class CLevel
{
private:
	std::vector<TWaveSettings> m_waveVector;
	int m_currentWave;	
public:
	void AddWaveId(int waveId,DWORD timeUntilNextWave,int chanceOfPowerUp);
	
	void AdvanceToNextWave();
	bool HaveAllWavesBeenLaunched();

	void BeginLevel() {m_currentWave=0;}
	int GetCurrentWaveId() const {return m_waveVector[m_currentWave].waveId;}
	
	DWORD GetTimeUntilNextWave() const 
	{
		if(m_currentWave>=(int)m_waveVector.size()) // happens at end of level
			return 0;
		return m_waveVector[m_currentWave].timeUntilNextWave;
	}

	int GetChanceOfPowerUp() const 
	{
		if(m_currentWave>=(int)m_waveVector.size()) // happens at end of level
			return 0;
		return m_waveVector[m_currentWave].chanceOfPowerUp;
	}
	
	CLevel(void);
	~CLevel(void);
};
