#include "common.h"
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "textfile.h"
#include "memoryUtils.h"

namespace Utility {

	const int kMaxLineLength=MAX_PATH;

	CTextFile::CTextFile(const CFilename &filename)
	{
		m_mode=eTextFileUnopend;
		m_filename=new CFilename(filename);
		EsCheckAlloc(m_filename);
		m_fp=NULL;
		m_commentsVector.reserve(4);
		m_dataSeperatorsVector.reserve(4);
	}

	CTextFile::CTextFile(const char *filename)
	{
		m_mode=eTextFileUnopend;
		m_filename=new CFilename(filename);
		EsCheckAlloc(m_filename);
		m_fp=NULL;
		m_commentsVector.reserve(6);
		m_dataSeperatorsVector.reserve(4);
	}

	CTextFile::~CTextFile(void)
	{
		if (m_mode!=eTextFileUnopend)
		{
			EsOutput(("Text file not closed - closing\n"));
			fclose(m_fp);
		}

		delete m_filename;

		for (size_t i=0;i<m_commentsVector.size();i++)
		{
			SAFE_FREE(m_commentsVector[i]);
		}
	}

	/**
	* \brief Open file for read of write, returns sucess
	* \date 20/12/2004
	*/
	bool CTextFile::Open(EOpenChoice choice)
	{
		if (m_mode!=eTextFileUnopend)
		{
			EsOutput(("File already open\n"));
			return false;
		}

		EsAssert(m_fp==NULL);

		switch(choice)
		{
		case eOpenForWrite:
			m_fp=fopen(m_filename->GetPathAndFilename(),("wt"));
			if (m_fp==NULL)
			{
				EsOutput(("Could not open file for write\n"));
				return false;
			}
			m_mode=eTextFileWriting;
			break;
		case eOpenForRead:
			m_fp=fopen(m_filename->GetPathAndFilename(),("rt"));
			if (m_fp==NULL)
			{
				EsOutput(("Could not open file %s for read\n"),m_filename->GetPathAndFilename());
				return false;
			}
			m_mode=eTextFileReading;
			break;
		default:
			EsError(("Unkown file choice\n"));
			return false;
			break;
		}

		return true;
	}

	/**
	* \brief Write a line
	* \date 20/12/2004
	*/
	void CTextFile::WriteLine(const char *str,...)
	{
		EsAssert(m_mode==eTextFileWriting);
		EsAssert(m_fp);
//ry		static char tmp[4096];

		// Parse the variable list
//ry		va_list ptr;
//ry		va_start(ptr,str);
		//size_t len=_vstprintf(tmp,str,ptr);
//ry		EsCheckSt(FORMATTED_STRING_VALIST(tmp,4096*sizeof(char),str,ptr));

		// This is where we finally convert into ascii
//ry		size_t written=fwrite(TOCHAR(tmp),1,strlen(TOCHAR(tmp)),m_fp);

		/*if (written!=len)
		{
			EsOutput(("Error writing\n"));
		}
		*/
	}

	/**
	* \brief add a comment character(s)
	* \date 21/12/2004
	*/
	void CTextFile::AddComment(const char *comment)
	{
		EsAssert(comment);
		EsAssert(strlen(comment)>0);

		char *newComment=CMemory::DuplicateString(comment);
		m_commentsVector.push_back(newComment);
	}

	/**
	* \brief add a data seperator character
	* \date 22/12/2004
	*/
	void CTextFile::AddDataSeperator(char seperator)
	{
		m_dataSeperatorsVector.push_back(seperator);
	}


	/**
	* \brief Get a line into the buffer ignoring comments if flag set
	* returns false on error
	* \date 21/12/2004
	*/
	bool CTextFile::ReadLine(char *lineBuffer,int maxChars,bool ignoreCommentedLines)
	{
		EsAssert(m_mode==eTextFileReading);
		EsAssert(m_fp);
		EsAssert(lineBuffer);

		char *ret=fgets(lineBuffer,maxChars,m_fp);
		if (ret==NULL) return false;

		while(ShouldLineBeIgnored(lineBuffer))
		{
			ret=fgets(lineBuffer,maxChars,m_fp);
			if (ret==NULL) return false;
		}

		// If there is an end of line marker (10) extract it
		if (lineBuffer[strlen(lineBuffer)-1]==10)
			lineBuffer[strlen(lineBuffer)-1]=0;

		return true;
	}

	/**
	* \brief Determine if line should be ignored (starts with a comment)
	* \date 21/12/2004
	*/
	bool CTextFile::ShouldLineBeIgnored(char*line)
	{
		EsAssert(line);

		// first check for eol
		if (line[0]=='\n') // is this ok with char?
			return true;

		ExtractFillCharacters(line);

		for (size_t i=0;i<strlen(line);i++)
		{
			// Check for comment
			for (size_t it=0;it<m_commentsVector.size();it++)
			{
				// Find first occurance of comment in the string
				char* res=strstr(line+i,m_commentsVector[it]);

				// If first occurance is current position then comment happened before any
				// real information, hence the line should be ignored
				if (res==line+i)
					return true;
			}
		}
		return false;
	}

	/**
	* \brief When reading keyword the following text is the value, return pointer to start of value
	* or NULL if keyword not found
	* \date 21/12/2004
	*/
	char* CTextFile::ExtractKeywordValue( char *line,const char *keyword)
	{
		char *pntr=NULL;
        pntr=strstr(line,keyword);
		if (pntr)
		{
			// extract keyword
			pntr=pntr+strlen(keyword);

			// now trim any spaces
			while(*pntr==' ')
				pntr++;

			return pntr;
		}

		return NULL;
	}

	/**
	* \brief determine if line contains keyword
	* \date 22/12/2004
	*/
	bool CTextFile::ContainsKeyword( char *line,const char *keyword)
	{
		char *pntr=NULL;
        pntr=strstr(line,keyword);
		if (pntr)
			return true;

		return false;
	}

	/**
	* \brief load vector from values contained in line
	* \param vec - the vector to be filled
	* \return success
	* \date 22/12/2004
	*/
	bool CTextFile::LoadVector(CVector3 *vec,char *line)
	{
		EsAssert(vec);
		EsAssert(line);

		char *pntr=line;

		pntr=LoadFloat(&vec->x,pntr);
		if (!pntr) return false;

		pntr=LoadFloat(&vec->y,pntr);
		if (!pntr) return false;

		pntr=LoadFloat(&vec->z,pntr);
		return true;
	}

	/**
	* \brief load float from values contained in line
	* returns pointer to next character in line
	* \date 22/12/2004CVector3
	*/
	char* CTextFile::LoadFloat(float *fl,char *line)
	{
		EsAssert(fl);
		EsAssert(line);
		char data[kMaxLineLength];
		char *pntr=GetData(line,data);

#if defined(WINCE)
		// WNCE does not have _tstof
		std::string s=SH::CStringHelpers::toNarrowString(line,kMaxLineLength);

		*fl=(float)atof(s.c_str());
//		wcstombs_s
#else
		if (pntr)

			*fl=(float)atof(data);
#endif

		return pntr;
	}

	/**
	* \brief load 3 floats from values contained in line
	* returns success
	* \date 22/12/2004
	*/
	bool CTextFile::LoadThreeFloats(float *f1,float *f2,float *f3,char *line)
	{
		EsAssert(f1);
		EsAssert(f2);
		EsAssert(f3);
		EsAssert(line);

		char *pntr=line;

		pntr=LoadFloat(f1,pntr);
		if (!pntr) return false;
		pntr=LoadFloat(f2,pntr);
		if (!pntr) return false;
		pntr=LoadFloat(f3,pntr);
		if (!pntr) return false;

		return true;
	}

	/**
	* \brief load 2 floats from values contained in line
	* returns success
	* \date 22/12/2004
	*/
	bool CTextFile::LoadTwoFloats(float *f1,float *f2,char *line)
	{
		EsAssert(f1);
		EsAssert(f2);
		EsAssert(line);

		char *pntr=line;

		pntr=LoadFloat(f1,pntr);
		if (!pntr) return false;
		pntr=LoadFloat(f2,pntr);
		if (!pntr) return false;

		return true;
	}

	/**
	* \brief load 3 ints from values contained in line
	* returns success
	* \date 09/01/2005
	*/
	bool CTextFile::LoadThreeInts(int *i1,int *i2,int *i3,char *line)
	{
		EsAssert(i1);
		EsAssert(i2);
		EsAssert(i3);
		EsAssert(line);

		char *pntr=line;

		pntr=LoadInt(i1,pntr);
		if (!pntr) return false;
		pntr=LoadInt(i2,pntr);
		if (!pntr) return false;
		pntr=LoadInt(i3,pntr);
		if (!pntr) return false;

		return true;
	}

	/**
	* \brief load int from value contained in line
	* returns pointer to next character in line
	* \date 22/12/2004
	*/
	char* CTextFile::LoadInt(int *in,char *line)
	{
		char data[kMaxLineLength];
		char *pntr=GetData(line,data);

#if defined(WINCE)
		// WNCE does not have _tstoi
		std::string s=SH::CStringHelpers::toNarrowString(line,kMaxLineLength);

		*in=atoi(s.c_str());
#else
		if (pntr)
			*in=atoi(data);
#endif
		return pntr;
	}

	/**
	* \brief get some data from the line, fill into data pointer
	* \return pointer to character after data
	* \date 22/12/2004
	*/
	char * CTextFile::GetData(char *line, char *data)
	{
		// Should now be at start of data
		if (strlen(line)==0)
		{
			EsOutput(("CTextFile::GetData: No data\n"));
			return NULL;
		}

		// Data is seperated by characters in m_dataSeperatorsVector
		// First of all remove any spaces and tabs etc.
		ExtractFillCharacters(line);

		// Should now be at start of data
		if (strlen(line)==0)
		{
			EsOutput(("CTextFile::GetData: No data\n"));
			return NULL;
		}

		size_t i=0;
		for (i=0;i<strlen(line);i++)
		{
			// Check for data seperator
			for (size_t it=0;it<m_dataSeperatorsVector.size();it++)
			{
				if (line[i]==m_dataSeperatorsVector[it])
				{
					// Finished - add 0
					data[i]=0;

					// return pointer to char beyond this seperator
					return line+i+1;
				}
			}

			data[i]=line[i];
		}

		// Reached end of line so finished
		data[i]=0;

		// no data left but return pointer to 0
		return line+i;
	}

	/**
	* \brief remove any fill characters from the start and end of line
	* \date 22/12/2004
	*/
	void CTextFile::ExtractFillCharacters(char *line)
	{
		// Skip fill characters at start
		size_t starchar=0;
		while(starchar<strlen(line) && IsFillCharacter(line[starchar]))
			++starchar;

		size_t endChar=strlen(line)-1;

		// Look for last fill char
		while(endChar>0 && IsFillCharacter(line[endChar]))
			--endChar;

		// Copy over
		size_t c=0;
		for (c=0;c<=endChar-starchar;c++)
			line[c]=line[starchar+c];

		// Zero terminated
		line[c]=0;
	}

	/**
	* \brief detect if a fill char
	* \date 22/12/2004
	*/
	bool CTextFile::IsFillCharacter(char c)
	{
		if (c==' ' || c=='\t')
			return true;

		return false;
	}
	/**
	* \brief load a string from line into buf - stop when data delimiator met or eol
	* \date 22/12/2004
	*/
	bool CTextFile::LoadAString(char *buf,int bufLength,char *line)
	{
		EsAssert(buf);
		EsAssert(line);

		size_t i=0;
		for (i=0;i<strlen(line);i++)
		{
			// Check for data seperator
			for (size_t it=0;it<m_dataSeperatorsVector.size();it++)
			{
				if (line[i]==m_dataSeperatorsVector[it])
				{
					// Finished - add 0
					buf[i]=0;
					return (strlen(buf)>0);
				}
			}

			buf[i]=line[i];
		}

		// Finished - add 0
		buf[i]=0;

		return (strlen(buf)>0);
	}
}
