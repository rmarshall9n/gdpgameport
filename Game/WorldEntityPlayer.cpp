//**************************************************************************************************
//
// WorldEntityPlayer.cpp: implementation of the CWorldEntityPlayer class.
//
// Description: Player controlled entity
//
// Created by: Keith Ditchburn 15/03/2003
//
//**************************************************************************************************
#include "Common.h"
#include "WorldModel.h"
#include "WorldEntityPlayer.h"
#include "Visualisation.h"
#include "map.h"
#include "tick.h"

//const float kConstantSpeed=0.35f;

const float kPlayerSize=1.0f;
const float kFireBarHeight=4.0f;

/**************************************************************************************************
**************************************************************************************************/
CWorldEntityPlayer::CWorldEntityPlayer(const char *name) : CWorldEntity(name),m_score(0),m_lives(0)
{

}

/**************************************************************************************************
**************************************************************************************************/
CWorldEntityPlayer::~CWorldEntityPlayer()
{

}

/**************************************************************************************************
Desc: Update player position by reference to the camera
Date: 15/03/2003
**************************************************************************************************/
void CWorldEntityPlayer::UpdateSpecific()
{
	// Gradually drift down screen
	//CVector3 pos=GetCentrePosition();

	// dt here will always be 1.0 which is ok but this should be linked to the
	// speed the background is scrolling - the drift

	// Moved the addition up into the world model update loop to solve key press issues
	//pos.y+=gMap->GetDriftRate();
//	pos.y=min(pos.y,gMap->GetScreenHeight()-GetHalfHeight()-kFireBarHeight);
	//SetCentrePosition(pos);
}

// Move us - based on our speed etc. in this direction
// direction is normalised vector
void CWorldEntityPlayer::Move(const CVector3& dir)
{
	//static float lastDtUpdate=0;
	//static int lastTickUpdate=0;

	CVector3 pos=GetCentrePosition();

	pos+=(dir*GetSpeed());

	// Check for out of bounds
	int w=WORLD.GetMap()->GetScreenWidth();
	int h=WORLD.GetMap()->GetScreenHeight();

	pos.x=min(pos.x,w-GetHalfWidth());
	pos.x=max(pos.x,GetHalfWidth());
	pos.y=min(pos.y,h-GetHalfHeight()-kFireBarHeight);
	pos.y=max(pos.y,GetHalfHeight());

	SetCentrePosition(pos);
}
