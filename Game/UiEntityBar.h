#pragma once
#include "uientity.h"

/**
 * \class CUiEntityBar
 * \brief A graphical bar for health, loading etc.
 * \author Keith Ditchburn \date 24 September 2005
*/
class CUiEntityBar :
	public CUiEntity
{
private:
	int m_value;
	int m_minValue;
	int m_maxValue;	
	int m_spriteId;
	int m_width;
	int m_height;
	float m_mapping;
	bool m_vertical;
public:
	CUiEntityBar(int min,int max,bool m_vertical,int spriteId);
	~CUiEntityBar(void);

	void Update();
	void Render(float dt);

	void SetValue(int newValue) {assert(newValue<=m_maxValue);m_value=newValue;}
};
