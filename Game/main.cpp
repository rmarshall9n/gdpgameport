#include <SDL.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>

// precompiled library header
#include "Common.h"

// Component headers
#include "Visualisation.h"
#include "WorldModel.h"
#include "Ui.h"

#include <iostream>
#include <string>

#include "Logger.h"

int main( int argc, char* args[] )
{
    fprintf (LOGFile, "Starting.\n");

    if (SDL_Init (SDL_INIT_EVERYTHING) < 0) {
		fprintf (LOGFile, "Couldn't initialize SDL: %s\n", SDL_GetError());
	}

	if(TTF_Init() == -1)
    {
        fprintf (LOGFile, "Couldn't initialize SDL TTF fonts:\n");
    }

	// Create Audio
    if(Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
    {
        fprintf (LOGFile, "Couldn't initialize SDL mixer:\n");
    }

    // create the screen
    const int screenWidth = 320;
    const int screenHeight = 240;

    SDL_Surface *screen = SDL_SetVideoMode(screenWidth, screenHeight, 16, SDL_SWSURFACE);

    if (screen == NULL) {
		fprintf (LOGFile, "Couldn't set  video mode: %s\n", SDL_GetError ());
	}

	SDL_WM_SetCaption( "CAANOO Game Demo", NULL );
    SDL_ShowCursor(SDL_DISABLE);


    // create visualiser
    CREATE_VIZ();
	if (!VIZ.Initialise(screen)) {
        fprintf (LOGFile, "Couldn't initialize Visualiser.\n");
	}

    // create ui
    CREATE_UI();

    // create world
    CREATE_WORLD();
	if(!WORLD.Initialise(screenWidth,screenHeight)) {
        fprintf (LOGFile, "Couldn't initialize World.\n");
	}


    fprintf (LOGFile, "Loading Level 1.\n");
    // load level
    if (!WORLD.LoadLevel("Game/Data/level1.txt")){
        fprintf (LOGFile, "Level 1 not loaded.\n");
    }


    fprintf (LOGFile, "Level 1 Loaded.\n");

    while(!WORLD.IsExiting())
    {
        WORLD.Update();
        if(WORLD.IsExiting())
            break;

        WORLD.Render();
        SDL_Flip(screen);
    }

    // SHUTDOWN
    fprintf (LOGFile, "Shut down: %s\n", SDL_GetError());
    fclose(LOGFile);

    DESTROY_WORLD();
    DESTROY_UI();
    DESTROY_VIZ();

    Mix_CloseAudio();
    TTF_Quit();

    SDL_Quit();

    return 0;
}
