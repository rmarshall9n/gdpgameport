#pragma once

//ry#include "UiEntityTypes.h"
#include <vector>
#include <assert.h>

class CUiScreen;

/**
 * \class CUi
 * \brief The main interface to the UI
 * \author Keith Ditchburn \date 24 September 2005
*/
class CUi
{
private:
	std::vector<CUiScreen*> m_entityVector;

	static CUi *m_instance;
	CUi(void);
	~CUi(void);
public:
	// Singleton functions
	static void CreateInstance();
	static void DestroyInstance();
	static CUi &GetInstance() {assert(m_instance);return *m_instance;}

	int AddScreen(const char *name);
	CUiScreen *GetScreenById(int id);

	void TurnScreenOn(int screenId);
	void TurnScreenOff(int screenId);

	void Update();
	void Render(float dt);
};

#define CREATE_UI	CUi::CreateInstance
#define DESTROY_UI	CUi::DestroyInstance
#define UI	CUi::GetInstance()
