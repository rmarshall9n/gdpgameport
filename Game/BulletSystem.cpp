#include "Common.h"
#include "bulletsystem.h"
#include "WorldModel.h"
#include "Visualisation.h"
#include "ExplosionManager.h"

#include <string.h>

CBulletSystem::CBulletSystem(void): m_maxBullets(0),m_bulletEntityStartIndex(0)
{
	m_bulletTemplates.reserve(32);
}

CBulletSystem::~CBulletSystem(void)
{
//ry	EsOutput(_T("Bullet template Vector grew to %d\n"),m_bulletTemplates.size());

	std::vector <CWorldEntityBullet*>::iterator p;
	for (p=m_bulletTemplates.begin();p!=m_bulletTemplates.end();++p)
		delete (*p);
	m_bulletTemplates.clear();
}

void CBulletSystem::Reset()
{
//	std::vector <CWorldEntityBullet*>::iterator p;
//	for (p=m_bulletTemplates.begin();p!=m_bulletTemplates.end();++p)
//		(*p)->DieNow();
}

// bulletEntityStartIndex is the start of the bullets in the world entity vector
// it is fixed as nothing is ever removed from the world entity vector

// there are a number of types of bullet so an array of graphic ids is passed in, these are then set when fireing
// Explosion graphic is for when bullet dies
void CBulletSystem::Initialise(int bulletEntityStartIndex, int maxBullets)
{
	m_maxBullets=maxBullets;
	m_bulletEntityStartIndex=bulletEntityStartIndex;
}

// fire a bullet
// position is centre
void CBulletSystem::Fire(const CVector3& startPos,const CVector3 &rot,
						 int bulletTemplateId,CWorldEntity::ESide side)
{
	// Get next free bullet entity instance
	for (int i=m_bulletEntityStartIndex;i<m_bulletEntityStartIndex+m_maxBullets;i++)
	{
		// This must be a bullet so ok to do this:
		CWorldEntity *base=WORLD.GetEntity(i);
		if (!base->GetActive())
		{
			CWorldEntityBullet* ent=(CWorldEntityBullet*)base;

			// this is a free one and it must be a bullet

			// Copy values from template
			CWorldEntityBullet *bulletTemplate=GetBulletTemplateById(bulletTemplateId);
			assert(bulletTemplate);

			ent->InitFromTemplate(bulletTemplate);

			// Set non template valuesCVector3
			ent->SetStartCentrePosition(startPos);
			ent->SetRotationY(rot.y);
			ent->SetActive(true);
			ent->SetSide(side);

			//ent->SetDieAnimation(m_explosionGraphic); TBD

			return;
		}
	}

	//OutputDebugString("No free bullet spaces\n");
}

// Add a bullet type to our bullet templates
int CBulletSystem::AddBulletTemplate(const char *name, const char *graphicName, const char *explosionName, const float speed, const int damage, const int multi)
{
	// Retrieve graphic id from viz first
	int graphicId=VIZ.GetSpriteIdByName(graphicName);
	if (graphicId==-1)
	{
//ry		EsOutput(_T("CBulletSystem: bullet graphic not found in viz\n"));
		return -1;
	}

	// Retrieve explosion id
	int explosionID=WORLD.GetExplosionManager()->GetExplosionTemplateIdByName(explosionName);
	if (explosionID==-1)
	{
//ry		EsOutput(_T("CBulletSystem: explosion graphic %s not found in viz\n"),data.explosionName);
		return -1;
	}


	CWorldEntityBullet *newBulletTemplate=new CWorldEntityBullet(name);
	newBulletTemplate->SetCollisionCost(damage);
	newBulletTemplate->SetSpeed(speed);
	newBulletTemplate->SetGraphicId(graphicId);
	newBulletTemplate->SetHealth(1);
	newBulletTemplate->SetExplosionTemplateId(explosionID);
	newBulletTemplate->SetMulti(multi);

	m_bulletTemplates.push_back(newBulletTemplate);
	return (int)(m_bulletTemplates.size()-1);
}

int CBulletSystem::AddBulletTemplate(const TBulletEntityChunk& data)
{
	assert(data.name);
	assert(data.graphicName);

	// Retrieve graphic id from viz first
	int graphicId=VIZ.GetSpriteIdByName(data.graphicName);
	if (graphicId==-1)
	{
//ry		EsOutput(_T("CBulletSystem: bullet graphic not found in viz\n"));
		return -1;
	}

	// Retrieve explosion id
	int explosionID=WORLD.GetExplosionManager()->GetExplosionTemplateIdByName(data.explosionName);
	if (explosionID==-1)
	{
//ry		EsOutput(_T("CBulletSystem: explosion graphic %s not found in viz\n"),data.explosionName);
		return -1;
	}


	CWorldEntityBullet *newBulletTemplate=new CWorldEntityBullet(data.name);
	newBulletTemplate->SetCollisionCost(data.damage);
	newBulletTemplate->SetSpeed(data.speed);
	newBulletTemplate->SetGraphicId(graphicId);
	newBulletTemplate->SetHealth(1);
	newBulletTemplate->SetExplosionTemplateId(explosionID);
	newBulletTemplate->SetMulti(data.multi);

	m_bulletTemplates.push_back(newBulletTemplate);
	return (int)(m_bulletTemplates.size()-1);
}

int CBulletSystem::GetBulletTemplateIdByName(const char *name)
{
	std::vector <CWorldEntityBullet*>::const_iterator p;
	int count=0;
	for (p=m_bulletTemplates.begin();p!=m_bulletTemplates.end();++p)
	{
		if (strcmp(name,(*p)->GetName())==0)
			return count;

		count++;
	}
	return -1;
}

CWorldEntityBullet *CBulletSystem::GetBulletTemplateById(int id) const
{
	assert(id>=0 && id<(int)m_bulletTemplates.size());
	return m_bulletTemplates[id];
}
