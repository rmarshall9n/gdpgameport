#pragma once
#include "worldentity.h"

class CWorldEntityBullet :public CWorldEntity
{
private:
	void UpdateSpecific();

	int m_multi;

public:
	CWorldEntityBullet(const char *name);
	~CWorldEntityBullet(void);

	void InitFromTemplate(const CWorldEntityBullet* other);

	void SetMulti(int m) {m_multi=m;}
	int GetMulti() const {return m_multi;}

	void HandlePreDeathActions();

};
