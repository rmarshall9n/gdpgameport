#pragma once

class CRectangle
{
public:
	int m_left,m_right,m_top,m_bottom;

	CRectangle(): m_left(0),m_right(0),m_top(0),m_bottom(0){}
	CRectangle(int l,int r,int t,int b) : m_left(l), m_right(r), m_top(t), m_bottom(b) {}
	CRectangle(const CRectangle &rct) : m_left(rct.m_left),m_right(rct.m_right), 
		m_top(rct.m_top), m_bottom(rct.m_bottom) {}

	void Set(int l,int r,int t,int b){
		m_left=l;m_right=r;m_top=t;m_bottom=b;
	}

	int Width() const {return m_right-m_left;}
	int Height() const {return m_bottom-m_top;}

	/**
	 * \brief Clips this rectangle to clipTo rect
	 * Note: may leave rectangle in dodgy state (as exits early for speed)
	 * \return true if still a valid rectangle
	*/
	bool Clip(const CRectangle &clipTo)
	{
		m_left=MAX(m_left,clipTo.m_left);
		m_right=MIN(m_right,clipTo.m_right);
		if (m_left>=m_right)
			return false;

		m_top=MAX(m_top,clipTo.m_top);
		m_bottom=MIN(m_bottom,clipTo.m_bottom);
		if (m_top>=m_bottom)
			return false;

		return true;
	};

	~CRectangle(){};
};
