#include "common.h"
#include ".\soundmanager.h"

CSoundManager::CSoundManager(void)
{
	m_mute=false;
}

CSoundManager::~CSoundManager(void)
{
}

bool CSoundManager::LoadSound(const char *filename, Mix_Chunk* sound)
{
#if defined(WINCE)
	// Not yet supported but pretend it is
	*id=0;
	return true;
#else
	sound = Mix_LoadWAV(filename);

    if(sound == NULL)
        return false;

    return true;
#endif
}

void CSoundManager::PlaySound(int id,bool loop) const
{
#if defined(WINCE)
	// Not yet supported but pretend it is
	return;
#endif
	if (m_mute)
		return;

	//HAPI->StopSound(id); // Removed this as was causing sound to not play after first attempt - not sure why
	bool ret=HAPI->PlayASound(id,loop);
	assert(ret);
}

void CSoundManager::StopSound(int id)
{
#if defined(WINCE)
	// Not yet supported but pretend it is
	return;
#endif
	HAPI->StopSound(id);
}
