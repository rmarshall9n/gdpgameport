#include "common.h"
#include "powerupmanager.h"
#include "WorldModel.h"
#include "WorldEntity.h"
#include "WorldEntityBullet.h"

CPowerUpManager::CPowerUpManager(void) : m_maxPowerUps(0), m_powerUpEntityStartIndex(0),m_healthPowerUpGraphic(0),
m_chanceOfPowerUp(0),m_minimumHealthGiven(0),m_rangeOfHealthGiven(0)
{
}

CPowerUpManager::~CPowerUpManager(void)
{
}

void CPowerUpManager::Initialise(int powerUpStartIndex,int maxPowerUps,int healthPowerUpGraphic)
{
	m_healthPowerUpGraphic=healthPowerUpGraphic;
	m_powerUpEntityStartIndex=powerUpStartIndex;
	m_maxPowerUps=maxPowerUps;
}

// An enemy unit has exploded - possible power up
void CPowerUpManager::ChanceOfPowerUp(const CVector3 &pos)
{
	if (m_chanceOfPowerUp==0 || rand()%m_chanceOfPowerUp==0)
	{
		// Look through our range for a free power up
		for (int i=m_powerUpEntityStartIndex;i<m_powerUpEntityStartIndex+m_maxPowerUps;i++)
		{
			CWorldEntity *base=WORLD.GetEntity(i);
			if (!base->GetActive())
			{
				// this is a free one and it must be a power up (bullet type)
				CWorldEntityBullet* ent=(CWorldEntityBullet*)base;

				// negative gives positive value
				// TBD: range needs putting in level file
				int healthGain=-(rand()%m_rangeOfHealthGiven+m_minimumHealthGiven);

				ent->SetCollisionCost(healthGain);
				ent->SetSpeed(1.0f);
				ent->SetRotationY(PI); // down
				ent->SetGraphicId(m_healthPowerUpGraphic);
				//ent->SetExplosionTemplateId(-1); // does not explode
				ent->SetStartCentrePosition(pos);
				ent->SetSide(CWorldEntity::eSideEnemy);
				ent->SetHealth(1);
				ent->SetActive(true);
				return;
			}
		}
	}
}
