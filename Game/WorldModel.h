//**************************************************************************************************
//
// WorldModel.h: interface for the CWorldModel class.
//
// Description: Provides the interfaces to the world model component
//
// Created by: Keith Ditchburn 12/03/2003
//
//**************************************************************************************************
#pragma once

#include "WorldEntityManager.h"
#include "SDL.h"
#include "SDL_Mixer.h"

class CWorldEntityManager;
class CWorldEntity;
class CMap;
class CBulletSystem;
class CExplosionManager;
class CPowerUpManager;
class CAi;
class CTick;

enum EUiInput
{
	eMoveForward,
	eMoveBackward,
	eMoveLeft,
	eMoveRight,
	eRotateLeft,
	eRotateRight,
	eFire,
	ePause,
	eAiPause,
	eShowFps,
	eInvincible,
	eContinue
};

enum EGameMode
{
	eGameModeFrontScreen,
	eGameModePlaying,
	eGameModeGameOver
};

class CWorldModel
{
private:
    SDL_Joystick* m_joystick;

	void ChangeGameMode(EGameMode newMode);

	int m_playerGraphic;
	int m_backgroundGraphic;
	int m_healthBarGraphic;
	int m_healthBarBorderGraphic;
	int m_healthPowerUpGraphic;
	int m_fireBarGraphic;
	int m_numbersGraphic;
	int m_hapiLogoGraphic;

	int m_firePower;

	Mix_Chunk* m_explosionSound;
	Mix_Chunk* m_laserSound;
	Mix_Chunk* m_hitSound;
	Mix_Chunk* m_collectSound;
	Mix_Chunk* m_clickSound;

	bool m_showFps;
	Uint32 m_previousGameTime;
	bool m_invincible;

	EGameMode m_gameMode;


	// Some managers and components
	CWorldEntityManager *m_worldEntityManager;
	CMap *m_map;
	CBulletSystem *m_bulletSystem;
	CExplosionManager *m_explosionManager;
	CPowerUpManager *m_powerUpManager;
	CAi *m_ai;
	CTick *m_tick;

    bool m_isExiting;

	// Screens
	int m_hudScreenId;
	int m_startScreenId;

	// Screen elements
	int m_hudHealthBarId;
	int m_hudHealthBarBorderId;
	int m_hudFireBarId;

	CVector3 m_playerTickMove;
	bool m_playerFire;

	// Private functions
	bool LoadStockAssets();
	void HandleKeyPresses();
	bool HasBulletCollidedWithEntity(int bulletId, int entityId);
	void UiInput(EUiInput type);

    bool LoadHardCodedData();
	void LoadHardCodedWaveData();
	void LoadHardCodedLevelData();

	void RenderNumber(int x,int y,int number,int numDigits);
	void Reset();

	static CWorldModel *m_instance;
	CWorldModel();
	~CWorldModel();

public:
	// Singleton functions
	static void CreateInstance();
	static void DestroyInstance();
	static CWorldModel &GetInstance() {assert(m_instance); return *m_instance;}

	bool LoadLevel(const char *filename);
	bool Initialise(int screenWidth,int screenHeight);

	void Render();
	void Update();

	bool IsExiting() const {return m_isExiting;}

	// Slightly smelly gets but avoids potentially slow passing of values down hierarchies
	CWorldEntity *GetEntity(int index) const {return m_worldEntityManager->GetEntity(index);}
	CBulletSystem* GetBulletSystem() const {return m_bulletSystem;}
	CExplosionManager *GetExplosionManager() const {return m_explosionManager;}
	CAi* GetAI() const {return m_ai;}
	CTick *GetTick() const {return m_tick;}
	CMap* GetMap() const {return m_map;}
};

#define CREATE_WORLD	CWorldModel::CreateInstance
#define DESTROY_WORLD	CWorldModel::DestroyInstance
#define WORLD	CWorldModel::GetInstance()
