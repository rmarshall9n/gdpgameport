#include "common.h"
#include "uientity.h"
#include "Colour.h"

CUiEntity::CUiEntity(const CColour *col) : m_screenX(0),m_screenY(0)
{
	if (col)
		m_colour=new CColour(*col);
	else
		m_colour=new CColour;
}

CUiEntity::~CUiEntity(void)
{
	delete m_colour;
}

void CUiEntity::ChangeColour(const CColour *newCol)
{
	assert(m_colour);
	assert(newCol);

	m_colour->m_red=newCol->m_red;
	m_colour->m_green=newCol->m_green;
	m_colour->m_blue=newCol->m_blue;
	m_colour->m_alpha=newCol->m_alpha;
}
