#pragma once

#include "UiEntityTypes.h"

class CColour;

/**
 * \class CUiEntity
 * \brief A UI entity base class
 * \author Keith Ditchburn \date 24 September 2005
*/
class CUiEntity
{
private:
	int m_screenX;
	int m_screenY;
	CColour *m_colour;
public:
	CUiEntity(const CColour *col);
	virtual ~CUiEntity(void);

	void ChangeColour(const CColour *newCol);

	void SetPosition(int x,int y) {m_screenX=x;m_screenY=y;}

	int GetScreenX() const {return m_screenX;}
	int GetScreenY() const {return m_screenY;}

	CColour *GetColour() const {return m_colour;}

	virtual void Update()=0;
	virtual void Render(float dt)=0;
};
