#include "Common.h"
#include "ui.h"
#include "UiScreen.h"

CUi* CUi::m_instance=0;

// Singleton functions
void CUi::CreateInstance()
{
	if (m_instance!=0)
	{
		//ry EsError("Trying to create a second instance of ui!!");
		return;
	}

	m_instance=new CUi();
}


void CUi::DestroyInstance()
{
	if (m_instance==0)
	{
//ry		EsError("Instance already destroyed!!");
		return;
	}

	delete m_instance;
	m_instance=0;
}

CUi::CUi(void)
{
	m_entityVector.reserve(8);
}

CUi::~CUi(void)
{
	std::vector <CUiScreen*>::iterator p;
	for (p=m_entityVector.begin();p!=m_entityVector.end();++p)
		delete (*p);

	m_entityVector.clear();
}

void CUi::TurnScreenOn(int screenId)
{
	// Turn all others off (if they are on)

	for (size_t i=0;i<m_entityVector.size();i++)
		TurnScreenOff((int)i);

	CUiScreen *sc=GetScreenById(screenId);
	assert(sc);

	sc->TurnOn();
}

void CUi::TurnScreenOff(int screenId)
{
	CUiScreen *sc=GetScreenById(screenId);
	assert(sc);

	if (sc->GetIsOn())
		sc->TurnOff();
}


int CUi::AddScreen(const char *name)
{
	assert(name);
	CUiScreen *newScreen=new CUiScreen(name);
	assert(newScreen);

	m_entityVector.push_back(newScreen);

	return (int)(m_entityVector.size()-1);
}

CUiScreen *CUi::GetScreenById(int id)
{
	assert(id>=0 && id<(int)m_entityVector.size());

	return m_entityVector[id];
}

void CUi::Update()
{
	std::vector <CUiScreen*>::const_iterator p;
	for (p=m_entityVector.begin();p!=m_entityVector.end();++p)
		(*p)->Update();
}

void CUi::Render(float dt)
{
	std::vector <CUiScreen*>::const_iterator p;
	for (p=m_entityVector.begin();p!=m_entityVector.end();++p)
	{
		if ((*p)->GetIsOn())
			(*p)->Render(dt);
	}
}
