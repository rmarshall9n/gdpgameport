#include "common.h"
#include ".\MemoryUtils.h"
#include "Filename.h"

namespace Utility{

/**
 * \brief  Make a copy of passed string, caller must free memory
 * \date  24/06/01
*/
char* CMemory::DuplicateString(const char* string)
{
	// I think it should be illegal to try to duplicate a NULL string
	EsAssert(string);
	if (!string)
		return NULL;

	size_t len = strlen(string) + 1;
	char *ret = (char*)malloc(len * sizeof(char));
	EsCheckAlloc(ret);
	EsAssert(ret);

	memcpy(ret, string, len * sizeof(char));

	return ret ;
}

/**
 * \brief  Make a copy of passed string, caller must free memory
 * \date  24/06/01
*/
char* CMemory::DuplicateStringChar(const char* string)
{
	// I think it should be illegal to try to duplicate a NULL string
	EsAssert(string);
	if (!string)
		return NULL;

	size_t len=strlen(string)+1;
	char *ret = (char*)malloc(len*sizeof(char)); // ok as char
	EsCheckAlloc(ret);

	memcpy(ret,string,len*sizeof(char)); // ok as char

#ifdef DEBUG
	// sanity check for null termination
	EsAssert(ret[len]==0);
	// and for ret
	EsAssert(ret);
	// and len - why not
	EsAssert(len>0);
#endif

	return ret ;
}

/**
 * \brief  Loads file into a memory block malloced here, pointer returned
 * On error returns null
 * \date  21/12/2002
*/
/*
BYTE* CMemory::LoadFileIntoMemory(const CFilename &filename,DWORD32 *numBytesRead)
{
	return NULL;
}
*/
// This was commented out causing T2 problems - WHY>
BYTE* CMemory::LoadFileIntoMemory(const CFilename &filename,DWORD32 *numBytesRead)
{
	EsAssert(numBytesRead);

	*numBytesRead=0;

	// Load file into memory
	FILE *file;

	file=fopen(filename.GetPathAndFilename(),"rb");

	if (!file)
		return NULL;

	// Get size
	fseek(file, 0L, SEEK_END);
	LONG32 fsize = ftell(file);

	EsAssert(fsize>0);

	BYTE* memory=(BYTE*)malloc(fsize);
	EsCheckAlloc(memory);

	// rewind file
	fseek(file, 0L, SEEK_SET);

	// read entire file in
	fread(memory, 1, fsize, file);
	fclose(file);

	*numBytesRead=fsize;

	return memory;
}

}
