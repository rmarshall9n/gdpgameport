#include "Common.h"
#include "explosionmanager.h"
#include "Visualisation.h"
#include "WorldModel.h"

#include<string.h>

CExplosionManager::CExplosionManager(void)
{
	m_explosionTemplates.reserve(32);
	m_maxExplosions=0;
	m_explosionEntityStartIndex=0;
}

CExplosionManager::~CExplosionManager(void)
{
//ry	EsOutput(_T("explosion template Vector grew to %d\n"),m_explosionTemplates.size());

	std::vector <CWorldEntityExplosion*>::iterator p;
	for (p=m_explosionTemplates.begin();p!=m_explosionTemplates.end();++p)
		delete (*p);
	m_explosionTemplates.clear();
}

void CExplosionManager::Reset()
{
//	std::vector <CWorldEntityExplosion*>::iterator p;
//	for (p=m_explosionTemplates.begin();p!=m_explosionTemplates.end();++p)
//		(*p)->DieNow();
}

void CExplosionManager::Initialise(int explosionEntityStartIndex, int maxExplosions)
{
	m_maxExplosions=maxExplosions;
	m_explosionEntityStartIndex=explosionEntityStartIndex;
}

int CExplosionManager::AddExplosionTemplate(const TExplosionEntityChunk& data)
{
	assert(data.name);
	assert(data.graphicName);

	// Retrieve graphic id from viz first
	int graphicId=VIZ.GetSpriteIdByName(data.graphicName);
	if (graphicId==-1)
	{
//ry		EsOutput(_T("CExplosionSystem: explosion graphic %s not found in viz\n"),data.graphicName);
		return -1;
	}

	CWorldEntityExplosion *newExplosionTemplate=new CWorldEntityExplosion(data.name);
	newExplosionTemplate->SetCollisionCost(0);
	newExplosionTemplate->SetSpeed(0);
	newExplosionTemplate->SetGraphicId(graphicId);
	newExplosionTemplate->SetHealth(1);
	// Set a life time to time of animation
	float dum;
	int dum2;
	newExplosionTemplate->SetLifeTime(VIZ.GetSpriteAnimationData(graphicId,&dum,&dum2));

	m_explosionTemplates.push_back(newExplosionTemplate);

	return (int)(m_explosionTemplates.size()-1);
}

int CExplosionManager::AddExplosionTemplate(const char *name, const char *graphicName)
{
	assert(name);
	assert(graphicName);

	// Retrieve graphic id from viz first
	int graphicId=VIZ.GetSpriteIdByName(graphicName);
	if (graphicId==-1)
	{
//ry		EsOutput(_T("CExplosionSystem: explosion graphic %s not found in viz\n"),data.graphicName);
		return -1;
	}

	CWorldEntityExplosion *newExplosionTemplate=new CWorldEntityExplosion(name);
	newExplosionTemplate->SetCollisionCost(0);
	newExplosionTemplate->SetSpeed(0);
	newExplosionTemplate->SetGraphicId(graphicId);
	newExplosionTemplate->SetHealth(1);
	// Set a life time to time of animation
	float dum;
	int dum2;
	newExplosionTemplate->SetLifeTime(VIZ.GetSpriteAnimationData(graphicId,&dum,&dum2));

	m_explosionTemplates.push_back(newExplosionTemplate);

	return (int)(m_explosionTemplates.size()-1);
}

CWorldEntityExplosion* CExplosionManager::GetExplosionTemplateById(int id) const
{
	assert(id>=0 && id<(int)m_explosionTemplates.size());
	return m_explosionTemplates[id];
}

int CExplosionManager::GetExplosionTemplateIdByName(const char*name)
{
	std::vector <CWorldEntityExplosion*>::const_iterator p;
	int count=0;
	for (p=m_explosionTemplates.begin();p!=m_explosionTemplates.end();++p)
	{
		if (strcmp(name,(*p)->GetName())==0)
			return count;

		count++;
	}
	return -1;
}

// Position is centre of explosion
void CExplosionManager::Explode(const CVector3 &pos,int explosionTemplateId)
{
	// Get next free explosion entity instance
	for (int i=m_explosionEntityStartIndex;i<m_explosionEntityStartIndex+m_maxExplosions;i++)
	{
		// This must be an explosion so ok to do this:
		CWorldEntityExplosion* ent=(CWorldEntityExplosion*)WORLD.GetEntity(i);
		if (!ent->GetActive())
		{
			// this is a free one and it must be an explosion

			// Copy values from template
			CWorldEntityExplosion *expTemplate=GetExplosionTemplateById(explosionTemplateId);
			assert(expTemplate);

			ent->InitFromTemplate(expTemplate);

			// Set non template values
			ent->SetStartCentrePosition(pos);

			ent->SetActive(true);

			return;
		}
	}

	//OutputDebugString("No free bullet spaces\n");
}
