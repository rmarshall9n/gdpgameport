#include "Common.h"
#include ".\surface.h"

#include <string.h>

//#define ONLY_ON_OFF_TRANSPARENCY

inline int GetDataOffset(int x,int y,int w) {return (x+y*w)*4;}

/**
 * \brief Constructor: create from passed data
 * \param
*/
CSurface::CSurface(BYTE *data,int width,int height,bool deleteOnExit): m_data(data),
	m_width(width),m_height(height),m_deleteDataOnExit(deleteOnExit)
{
	assert(m_data);

	m_hasAlpha=false;

	// Determine if texture has alpha - if not set flag for speed up
	// Load time would be quicker if this were put into the data TBD
	BYTE *temp=data;
	int i=0;
	while(i<width*height && !m_hasAlpha)
	{
		BYTE a=temp[3];
		if (a<255)
			m_hasAlpha=true;
		temp+=4;
		i++;
	}

	if (!m_hasAlpha){}
//ry		EsOutput(_T("Does not contain alpha - speed boost here\n"));

}

/**
 * \brief Constructor: create new blank surface
 * \param
*/
CSurface::CSurface(int width,int height): m_width(width),m_height(height), m_deleteDataOnExit(true)
{
	m_data=new BYTE[width*height*4];
	assert(m_data);
}



/**
 * \brief Destructor
 */
CSurface::~CSurface(void)
{
	if (m_deleteDataOnExit)
	{
		if (m_data)
		{
			delete []m_data;
			m_data=0;
		}
	}
}

// Fill with a colour
void CSurface::Fill(BYTE r,BYTE g,BYTE b,BYTE a)
{
	// 4 bytes per colour a8r8g8b8

	// Pack colour into a DWORD, 4 bytes or 32 bits laid out like:  0xXXRRGGBB
	DWORD c=0x00000000;

	c= c | (DWORD)a<<24;
	c= c | (DWORD)r<<16;
	c= c | (DWORD)g<<8;
	c= c | b;

	// Set 4 bytes at a time
	DWORD *pnter=(DWORD*)m_data;
	for (int y=0;y<m_height;++y)
	{
		for (int x=0;x<m_width;++x)
		{
			memcpy(pnter,&c,4);
			++pnter;
		}
	}

	c=c;
}

void CSurface::Blit(CSurface *destination,int dx,int dy,CRectangle *sourceRectIn)
{
	assert(m_data);
	assert(destination);

	// Create rect to clip against
	CRectangle destRect(0,destination->GetWidth(),0,destination->GetHeight());

	// Create rect to be clipped, convert to clip space
	CRectangle sourceRect(0+dx,m_width+dx,0+dy,m_height+dy);

	if (sourceRectIn)
	{
		sourceRect.Set(sourceRectIn->m_left+dx,sourceRectIn->m_right+dx,
			sourceRectIn->m_top+dy,sourceRectIn->m_bottom+dy);
	}

	if (sourceRect.Clip(destRect))
	{
		// Once clipped have to set back to real space
		sourceRect.m_left-=dx;
		sourceRect.m_right-=dx;
		sourceRect.m_top-=dy;
		sourceRect.m_bottom-=dy;

		Blit(destination->GetData(),destination->GetWidth(),dx,dy,sourceRect);
	}
}

// We are the source
void CSurface::BlitTransparent(CSurface *destination,int dx,int dy,CRectangle *sourceRectIn)
{
	assert(m_data);
	assert(destination);

	// Create rect to clip against
	CRectangle destRect(0,destination->GetWidth(),0,destination->GetHeight());

	// Create rect to be clipped, convert to clip space
	CRectangle sourceRect(0+dx,m_width+dx,0+dy,m_height+dy);

	if (sourceRectIn)
	{
		sourceRect.m_right=sourceRect.m_left+sourceRectIn->Width();
		sourceRect.m_bottom=sourceRect.m_top+sourceRectIn->Height();
	}

	if (sourceRect.Clip(destRect))
	{
		// Once clipped have to set back to real space
		sourceRect.m_left-=dx;
		sourceRect.m_right-=dx;
		sourceRect.m_top-=dy;
		sourceRect.m_bottom-=dy;

		// And if only part of a surface factor that in
		if (sourceRectIn)
		{
			sourceRect.m_left+=sourceRectIn->m_left;
			sourceRect.m_right+=sourceRectIn->m_left;
			sourceRect.m_top+=sourceRectIn->m_top;
			sourceRect.m_bottom+=sourceRectIn->m_top;
		}

		if (m_hasAlpha) // added
			BlitTransparent(destination->GetData(),destination->GetWidth(),dx,dy,sourceRect);
		else
			Blit(destination->GetData(),destination->GetWidth(),dx,dy,sourceRect);
	}
}

/**
 * \brief Blit into destination starting at x,y. Use the area passed in or all if null
 * \param No alpha
 * \return
*/
void CSurface::Blit(BYTE *destination,int destPitch,int dx,int dy,
					const CRectangle &sourceRect)
{
	assert(m_data);
	assert(destination);

	// These can sometimes be < 0 when something emerges from off the screen
	dx=MAX(dx,0);
	dy=MAX(dy,0);

	// Work out the start position on the destination
	int offset=GetDataOffset(dx,dy,destPitch);
	BYTE *destPnter=destination+offset;
//ry	ReleaseAssert(destPnter>0);

	// Work out the start position on the source
	BYTE *sourcePnter=m_data+GetDataOffset(sourceRect.m_left,sourceRect.m_top,m_width);
//ry	ReleaseAssert(sourcePnter>0);

	// Copy line at a time
	for (int y=0;y<sourceRect.Height();++y)
	{
		memcpy(destPnter,sourcePnter,sourceRect.Width()*4);

		// Move pointers to next line down
		destPnter+=(destPitch*4);
		sourcePnter+=(m_width*4);
	}
}



/**
 * \brief As above but use transparency
 * \param sw (soruce rect width) cannot be taken from the rect as it is the pitch and the rect may be smaller
 * than pitch of the surface
 * \return
*/
void CSurface::BlitTransparent(BYTE *destination,int destPitch,int dx,int dy,
							   const CRectangle &sourceRect)
{
	assert(m_data);
	assert(destination);

	// These can sometimes be < 0 when something emerging from off the screen
	dx=MAX(dx,0);
	dy=MAX(dy,0);

	// Work out the start position on the destination
	int offset=GetDataOffset(dx,dy,destPitch);
	BYTE *destPnter=destination+offset;

	// Work out the start position on the source
	offset=GetDataOffset(sourceRect.m_left,sourceRect.m_top,m_width);
	BYTE *sourcePnter=m_data+offset;

	// Increment values when reach end of x scanline
	int endOfLineDestIncrement=(destPitch-sourceRect.Width())*4;
	int endOfLineSourceIncrement=(m_width-sourceRect.Width())*4;

	// Each colour is 4 bytes, need to add source to destination based on the alpha value of the source
	for (int y=0;y<sourceRect.Height();++y)
	{
		for (int x=0;x<sourceRect.Width();++x)
		{
			BYTE a=sourcePnter[3];

			// a varies from 0 for totally transparent to 255 for not at all
			// If totally transparent do not copy, if 255 overwrite, in between is semi-transparency (may not need to support)
			// For simple on/off transparency just assign the values
			if (a>0)
			{
				if (a==255)
				{
					memcpy(destPnter,sourcePnter,4);
				}
				else
				{
					BYTE b=sourcePnter[0];
					BYTE g=sourcePnter[1];
					BYTE r=sourcePnter[2];


					// For handling degrees of transparency:

					/*float alpha=(float)a/255.0f;
					float ialpha=1.0f-alpha;

					destPnter[0]=(BYTE)(ialpha*destPnter[0] + alpha*b);
					destPnter[1]=(BYTE)(ialpha*destPnter[1] + alpha*g);
					destPnter[2]=(BYTE)(ialpha*destPnter[2] + alpha*r);*/


					// better way
					destPnter[0]=destPnter[0]+((a*(b-destPnter[0])) >> 8);
					destPnter[1]=destPnter[1]+((a*(g-destPnter[1])) >> 8);
					destPnter[2]=destPnter[2]+((a*(r-destPnter[2])) >> 8);
					// no need to set alpha
				}
			}

			destPnter+=4;
			sourcePnter+=4;
		}

		// Down a line
		destPnter+=endOfLineDestIncrement;
		sourcePnter+=endOfLineSourceIncrement;

	}
}

// Replace rgba with nr..
void CSurface::Recolour(BYTE r,BYTE g,BYTE b, BYTE a, BYTE nr,BYTE ng,BYTE nb,BYTE na)
{
	assert(m_data);


	// Each colour is 4 bytes, need to add source to destination based on the alpha value of the source

	// Work out the start position on the source
	BYTE *sourcePnter=m_data;

	int count=0;
	for (int y=0;y<=GetHeight();++y)
	{
		for (int x=0;x<GetWidth();++x)
		{
			BYTE bs=sourcePnter[0];
			BYTE gs=sourcePnter[1];
			BYTE rs=sourcePnter[2];
			BYTE as=sourcePnter[3];

			if (rs==r && gs==g && bs==b && as==a)
			{
				sourcePnter[0]=nb;
				sourcePnter[1]=ng;
				sourcePnter[2]=nr;
				sourcePnter[3]=na;
				count++;
			}

			sourcePnter+=4;
		}
//		sourcePnter=m_data+GetDataOffset(sourceRect.m_left,sourceRect.m_top+y,sourcePitch);
	}

//ry	EsOutput(_T("Replaced %d pixels\n"),count);
}
