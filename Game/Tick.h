#pragma once

#include "Toolbox.h"

#include "SDL.h"

#define TIME_TO_MEASURE_FPS	4000

/**
 * \class CTick
 * \brief Current tick etc.
 * \author Keith Ditchburn \date 25 September 2005
*/
class CTick
{
private:
	unsigned int m_tick;
	unsigned int m_timeBetweenTicks;	// Milliseconds
	unsigned int m_timeOfLastTick;	// Millisecondsa
	bool m_paused;
	bool m_interpolationOn;

	// Frame rate calcs
	DWORD m_frameRate;
	DWORD m_numFrames;
	DWORD m_lastRenderTime;
public:
	CTick(unsigned int timeBetweenTicks) : m_tick(0),m_timeBetweenTicks(timeBetweenTicks),m_timeOfLastTick(0),
        m_paused(true),m_interpolationOn(true),m_frameRate(0),m_numFrames(0),m_lastRenderTime(0)
	{

	}
	~CTick(void){}

	void StartRender()
	{
		unsigned int timeNow = SDL_GetTicks();
		DWORD timeSinceLastRender=timeNow-m_lastRenderTime;
		m_numFrames++;

		if (timeSinceLastRender>TIME_TO_MEASURE_FPS)
		{
			// Every TIME_TO_MEASURE_FPS milliseconds calculate frame rate
			m_frameRate=(DWORD)((float)m_numFrames/(0.001f*timeSinceLastRender));
			m_numFrames=0;
			m_lastRenderTime=timeNow;
		}
	}

	DWORD GetFramesPerSecond() const {return m_frameRate;}
	DWORD GetTicksPerSecond() const {return 1000/m_timeBetweenTicks;}

	void Pause(bool set) {m_paused=set;}
	unsigned int GetTick() const {return m_tick;}
	bool IsPaused() const {return m_paused;}
	bool IsInterpolationOff() const
	{
		// Had real trouble with interp on PPC so turned off just for it
#if defined(WINCE)
		return true;
#else
		return !m_interpolationOn;
#endif
	}

	// Time throught the tick, 0.0 to 1.0
	float GetDt() const
	{
		if (m_paused)
			return 0;

		unsigned int timeNow = SDL_GetTicks();
		unsigned int timeSinceTicked = timeNow - m_timeOfLastTick;

		float dt=(float)timeSinceTicked/m_timeBetweenTicks;

		dt=min(dt,1.0f); // can go over 1 when paused
	//	assert(dt<=1.0f);
		return dt;
	}

	// Returns true if ticked
	bool Update()
	{
		if (m_paused)
			return false;

		unsigned int timeNow = SDL_GetTicks();
		unsigned int timeSinceTicked=timeNow-m_timeOfLastTick;
		if (timeSinceTicked>=m_timeBetweenTicks)
		{
			m_tick++;
			m_timeOfLastTick=timeNow;
			//m_timeOfLastTick+=m_lengthOfOneTick; // test TBD

			// Work out if should interpolate positions
			// only interpolate if we are rendering fast enough - added for PPC but relevant to PC as well
#if !defined(WINCE) // now turned off on PPC - see note above
			m_interpolationOn=true;
			if (GetFramesPerSecond()<GetTicksPerSecond()+8)
				m_interpolationOn=false;
#endif

			return true;
		}

		return false;
	}
};
