#include "Common.h"
#include "WorldModel.h"
#include "WorldEntityBackground.h"
#include "map.h"
#include "visualisation.h"

CWorldEntityBackground::CWorldEntityBackground(const char *name) : CWorldEntity(name),m_splitY(0)
{
}

CWorldEntityBackground::~CWorldEntityBackground()
{
}

void CWorldEntityBackground::Initialise()
{
	// For vertical scroll will need two sprites drawing
	// Unless the texture is smaller than 2* the screen area
	// I will ignore this for the time being

	// must be called after graphic id is set
	assert(GetGraphicId()>=0);

	int textureHeight=GetHeight();

	// for time being assume the texture is bigger than the height of the screen
	assert(textureHeight>=WORLD.GetMap()->GetScreenHeight());

	// So use two textures to allow scrolling
	m_splitY=0.0f;
}

void CWorldEntityBackground::UpdateSpecific()
{
	m_splitY+=WORLD.GetMap()->GetDriftRate();
	if (m_splitY>=(float)GetHeight())
		m_splitY-=(float)GetHeight();
}

void CWorldEntityBackground::Render(float dt)
{
	// factor in dt by making split float change
	float lerpSplit=m_splitY-1.0f+dt;

	// must just take into account the loop over (m_splitY is 0)
	if (lerpSplit<0)
		lerpSplit=(float)GetHeight()-1.0f-dt;

    SDL_Rect rect;
	rect.x = 0;
	rect.w = WORLD.GetMap()->GetScreenWidth();

	// render in two parts
    // BACKGROUND 2 (BOTTOM)
	// Bottom one first from m_splitY down to screenHeight
	rect.y = 0;
	rect.h = WORLD.GetMap()->GetScreenHeight() - (long)lerpSplit;

    int y = (int)floor(lerpSplit + 0.5f);
	VIZ.RenderSprite(GetGraphicId(), 0, y, &rect);


    // BACKGROUND 1 (TOP)
	// Change rect
	rect.y = (int)floor(GetHeight() - lerpSplit);
    rect.h = (long)lerpSplit;
	// Start at y=0
	VIZ.RenderSprite(GetGraphicId(), 0, 0, &rect);
}
