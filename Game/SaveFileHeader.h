//**************************************************************************************************
//
// SaveFileHeader.h
//
// Description: Describes structure of save file header tag
//
// Created by: Keith Ditchburn 23/02/2002
//
//**************************************************************************************************

#pragma once

#ifndef __SAVE_FILE_HEADER_H
#define __SAVE_FILE_HEADER_H

#pragma once

#include "common.h"

enum ESaveFileType
{
	eIllegalSaveFileType=-1,
	eStandardSaveFileType
};

// Had to redefine filetime to handle 64 bit issues
struct MYFILETIME {
    DWORD32 dwLowDateTime;
    DWORD32 dwHighDateTime;
};

struct TSaveFileHeader
{
	char saveFileId[6]; // kjdsf always
	MYFILETIME fileModified;
	int version;
	ESaveFileType type;
	//DWORD32 pad1;
	//DWORD32 pad2;
	// For 64 bit DWORD32 (unsigned LONG32) is 64 bits not the required 32!
	DWORD32 pad1;
	DWORD32 pad2;
	char pad3;
	char pad4;
	int pad5;
	int pad6;
	char pad7[512];
};

#endif
