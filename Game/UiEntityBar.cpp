#include "common.h"
#include "uientitybar.h"
#include "visualisation.h"

CUiEntityBar::CUiEntityBar(int min,int max,bool vertical,int spriteId) : CUiEntity(0),m_value(min),m_minValue(min),
    m_maxValue(max),m_spriteId(spriteId),m_vertical(vertical)
{
	// Get sprite dimensions
	VIZ.GetSprite2DDimensions(m_spriteId,&m_width,&m_height);

	if (m_vertical)
		m_mapping=(float)m_height/(m_maxValue-m_minValue);
	else
		m_mapping=(float)m_width/(m_maxValue-m_minValue);
}

CUiEntityBar::~CUiEntityBar(void)
{
}

void CUiEntityBar::Update()
{
}

void CUiEntityBar::Render(float dt)
{
	int value=m_value;
	value=max(value,m_minValue);
	value=min(value,m_maxValue);

	int newValue=(int)(value*m_mapping);

    SDL_Rect rect;
    rect.x = 0;
    rect.h = m_height;

	if (m_vertical)
	{
		int topOffset=m_height-newValue;

        rect.y = topOffset;
        rect.w = m_width;

		//SDL_Rectrect(0,m_width,topOffset,m_height);
		VIZ.RenderSprite(m_spriteId,GetScreenX(),GetScreenY()+topOffset,&rect);
	}
	else
	{
	    rect.y = 0;
		rect.w = newValue;

		//ry CRectangle rect(0,newValue,0,m_height);
		VIZ.RenderSprite(m_spriteId,GetScreenX(),GetScreenY(),&rect);
	}
}
