#pragma once

#include "LevelChunkData.h"

class CWorldEntityEnemy;
class CWave;
class CRoute;
class CLevel;
class CPowerUpManager;
struct TWaveData;

/*
	Handles the AI part of the game. The AI has levels, waves and route classes
	Each level can have a number of waves of enemies. Each wave of enemies follows a route.
*/
class CAi
{
private:
	enum EWaveStatus
	{
		eWaveStatusSpawning,
		eWaveStatusActive,
		eWaveStatusDead,
		eWaveStatusSpawnNew,
		eWaveStatusLevelEnd
	};

	int m_maxEnemies;
	int m_enemyEntityStartIndex;
	int m_currentEnemyTemplateId;
	int m_currentWave;

	int m_currentLevel;

	EWaveStatus m_currentWaveStatus;

	unsigned int m_timeOfLastWave;
	unsigned int m_timeOfLastWaveSpawning;
	unsigned int m_timeOfLastWaveFinished;
	unsigned int m_timeWhenLevelFinished;

	std::vector<CWorldEntityEnemy*> m_enemyTemplates;
	std::vector<CWave*> m_waveVector;
	std::vector<CRoute*> m_routeVector;
	std::vector<CLevel*> m_levelVector;

	void LaunchWave(int index);
	bool m_paused;
public:
	CAi(void);
	~CAi(void);

	void TogglePause() {m_paused=!m_paused;}

	void Initialise(int enemyEntityStartIndex, int maxEnemies);
	void Update(CPowerUpManager *gPowerUpManager);

	int AddEnemyTemplate(const TEnemyEntityChunk& data);

	int AddEnemyTemplate(const char *entityName,
        const char *graphicName,
        const char *bulletName,
        const char *explosionName,
        const int collisionCost,
        const int health,
        const float moveSpeed,
        const float rotSpeed,
        const int onlyFireForward, // 1 or 0
        const int fireRate, // ms
        const int chanceOfFiring,
        const int score);

	int GetEnemyTemplateIdByName(const char* name) const;

	int AddWave(TWaveData *waveData);


	int AddRoute(const TRouteChunk &route);
	int AddRoute(char *routeName,
        int numWayPoints,
        CVector3 *wayPoints);

	int GetRouteIdByName(const char* name);

	void AddLevel(CLevel *level);

	CWorldEntityEnemy* SpawnEnemy(int enemyTemplateId);
	void KillAllRemainingEnemies();
	void Reset();

	bool HasLevelEnded() const {return (m_currentWaveStatus==eWaveStatusLevelEnd);}
	int GetCurrentLevel() const {return m_currentLevel;}
};
