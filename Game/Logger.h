#ifndef _LOGGER_H_
#define _LOGGER_H_

#include "stdio.h"

class CLogger
{
private:

    FILE* m_file;

    CLogger();
    CLogger(const CLogger& logger);
    ~CLogger();

public:
    static CLogger& GetInstance();
    static void Destroy();

    FILE* GetLogFile(){ return m_file; }
    void LogError(char* error);
    void LogMessage(char* error);
};

static CLogger* m_instance = 0;

#define LOG CLogger::GetInstance()
#define LOGFile CLogger::GetInstance().GetLogFile()

#endif // _LOGGER_H_
