#pragma once

#include "worldentity.h"

// explosions!
class CWorldEntityExplosion :
	public CWorldEntity
{
private:
	void UpdateSpecific();
public:
	CWorldEntityExplosion(const char *name);
	~CWorldEntityExplosion(void);

	void InitFromTemplate(const CWorldEntityExplosion* other);
};
