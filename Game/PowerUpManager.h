#pragma once

#include "Vector3.h"

class CPowerUpManager
{
private:
	int m_maxPowerUps;
	int m_powerUpEntityStartIndex;
	int m_healthPowerUpGraphic;

	int m_chanceOfPowerUp;
	int m_minimumHealthGiven;
	int m_rangeOfHealthGiven;

public:
	CPowerUpManager(void);
	~CPowerUpManager(void);

	void Initialise(int powerUpStartIndex,int maxPowerUps,int healthPowerUpGraphic);

	void SetChanceOfPowerUp(int chance){m_chanceOfPowerUp=chance;}
	void SetHealthRange(int min,int range){m_minimumHealthGiven=min;m_rangeOfHealthGiven=range;}
	void ChanceOfPowerUp(const CVector3 &pos);
};
