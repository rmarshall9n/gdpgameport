#include "common.h"

#if !defined(CAANOO)
#include <direct.h>
#endif

//ry void SetCurrentDirectory(const char *dir)
/*{
	_chdir(dir);
}
*/
void GetCurrentDirectory(size_t bufSize,char *buf)
{
    #if !defined(CAANOO)
	_getcwd(buf,bufSize);
    #endif
}

/*//ry
int FormattedStringVaList(char *dest,size_t numChars,
			const char* format,va_list args)
		{
#if defined(WINCE)
			int numWritten=_vstprintf(dest,format,args);
#else
			int numWritten=vsnprintf(dest,numChars,format,args);
#endif
			//if (numWritten<0)
				//OutputDebugString(_T("ERROR in CStringHelpers::FormattedStringVaList  - _vstprintf_s failed\n"));
			return numWritten;
		}
*/
void EsOutput(const char* str,...)
{
//ry	char tmp[2048];

	// Parse the variable list
//ry	va_list ptr ;
//ry	va_start(ptr,str) ;

//ry	FormattedStringVaList(tmp,2048,str,ptr);

//ry	printf(tmp);
}

// This used to use windows MessageBox
void UserMessage(const char* message,const char *title)
{
	EsOutput("User Message: %s : %s",title,message);
}
