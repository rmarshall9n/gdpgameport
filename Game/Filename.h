#pragma once

namespace Utility{

	/**
	* \class CFilename
	* \brief Small class to sort out problems with filenames etc. that I have had in the past
	* \author Keith Ditchburn 21/06/2002
	*/
	class CFilename
	{
	private:
		void ReleaseMemory();
		char *m_path;
		char *m_filename;
		char *m_pathAndFilename;
		char *m_filenameMinusExtension;
	public:
		enum EFileType
		{
			eFileTypeUnknown=-1,
			eFileTypeBitmap,
			eFileTypeTarga,
			eFileTypeTif,
			eFileTypeJpg,
			eFileTypePng,
			eFileTypeGif, // cannot be saved to
			eFileType3DS,
			eFileTypeX,
			eFileTypeT2Project,
			eFileTypeT2Material,
			eFileTypeBt,
			eFileTypeIco,
			eFileTypeWmf,
			eFileTypePcx,
			eFileTypeDds,
			eFileTypeDib,
			eFileTypeTxt,
			eNumFileTypes
		};

		inline const char* GetPathAndFilename() const {return m_pathAndFilename;}
		inline const char * GetPath() const {return m_path;}
		inline const char * GetFilename() const {return m_filename;}

		bool operator == (const CFilename& c) const;
		bool operator != (const CFilename& c) const;
		bool operator < (const CFilename& c) const;
		bool operator > (const CFilename& c) const;

		bool TrimPathToPath(const char*path);
	//	void Save();
	//	void Load();

		void ChangeFilenameOnly(const char *newFilename);
		void GetExtension(char *ext,size_t bufSize) const;
		void AppendToFilename(const char *text);
		void SetToUniqueTempFilename();
		void Set(const CFilename &filename);
		EFileType GetFileType() const;
		void ChangeExtension(const char *newExt);
		void ChangeExtension(EFileType type);
		bool DoesFileExist() const;
	//	void ReadFromSaveBlock(CDataBlockUnpack *block);
		//void WriteToSaveBlock(CDataBlockPack *block) const;
		inline const char* GetFilenameMinusExtension() const {return m_filenameMinusExtension;};
		void ChangeFilename(const char *newFilename);
		void ChangePath(const char *newPath);
		void SetViaPathAndFilename(const char *path,const char *filename);
		void SetViaPathAndFilename(const char *pathAndFilename);
		void ConvertTo83Format(int uniqueId);

		// Various constructors
		CFilename();
		CFilename(CFilename *filename);
		CFilename(const CFilename &filename);
		CFilename(const char *pathAndFilename);
		CFilename(const char *path,const char *filename);
	//	CFilename(CDataBlockUnpack *block);

		~CFilename();

		// Utility filename functions - no instance needed
		static CFilename::EFileType GetFileTypeFromExtension(const char *ext);
		static bool GetExtensionFromFileType(CFilename::EFileType,char *ext,size_t bufSize);

		static char* ExtractFilenameFromPath(const char * const passedPath);
		static char* ExtractFilenameFromPathNoExtension(const char *const passedPath);
		static char* ExtractPathFromPathAndFilename(const char *pathAndFilename);
		static char* ExtractExtensionFromPath(const char *passedPath);

		static bool DoesFileExist(const char *const filename);
		static bool DoesFileExist(const CFilename &filename);
		static bool DoesPathExist(const char *const path);

		static char* GetUniqueTemporaryFilename();
		//static void RemoveUndeletedTemporaryFiles();
		//static bool CopyFileToFile(CFilename *from,CFilename *to);
		//static bool CopyFileToFile(const char *fromPath,const char *toPath);
		static bool MakePathRelative(const char *path,char *relative);
		static char* ConvertRelativeToFull(const char *path);
	};

}

