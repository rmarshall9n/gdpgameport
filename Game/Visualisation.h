#pragma once

#include <SDL.h>
#include <SDL_ttf.h>

class CSprite;

// Visualisation component interfaces
class CVisualisation
{
private:
	std::vector <CSprite*> m_spriteVector;
	SDL_Surface* m_screenSurface;

	inline CSprite *GetSpriteById(int id) const
	{
		if (id < 0 || id > (int)m_spriteVector.size()) return 0;
		return m_spriteVector[id];
	}

	static CVisualisation *m_instance;

    TTF_Font* m_font;

	CVisualisation(void);
	~CVisualisation(void);
public:

	// Singleton functions
	static void CreateInstance();
	static void DestroyInstance();
	static CVisualisation &GetInstance() {assert(m_instance);return *m_instance;}

	bool Initialise(SDL_Surface* surface);
	bool CreateSprite(const char* filename,const char *name,int *id,bool createCollisionMask,
                        int numFrames=0,int frameSize=0,float frameSpeed=1.0f,int mode=0);

    bool CreateColourKeyedSprite(const char* filename,const char *name,int *id,bool createCollisionMask,
                        int numFrames=0,int frameSize=0,float frameSpeed=1.0f,int mode=0);


	void ClearScreenToColour(BYTE r,BYTE g,BYTE b);
	int GetSpriteIdByName(const char* name);

	void GetSprite2DDimensions(int m_graphicId,int *m_spriteWidth,int *m_spriteHeight) const;
	int GetSpriteAnimationData(int graphicId,float *frameSpeed,int *frameMode) const;

	bool RenderSprite(int id,int x,int y,SDL_Rect *rect=0);
	bool RenderSpriteAnimate(int id,int x,int y,int frameNumber);
	void RenderText(int x, int y, char *text, BYTE r=255,BYTE g=255,BYTE b=255,BYTE a=255);
};

#define CREATE_VIZ	CVisualisation::CreateInstance
#define DESTROY_VIZ	CVisualisation::DestroyInstance
#define VIZ	CVisualisation::GetInstance()
