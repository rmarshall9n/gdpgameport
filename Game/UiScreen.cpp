#include "common.h"
#include "uiscreen.h"
#include "UiEntity.h"
#include "UiEntityText.h"
#include "UiEntityBar.h"
#include "UiEntityImage.h"

#include <string.h>

CUiScreen::CUiScreen(const char*name) : m_enabled(false)
{
	m_name=new char[strlen(name)+1];
	strcpy(m_name,name);

	m_entityVector.reserve(32);
}

CUiScreen::~CUiScreen(void)
{
	delete m_name;

	std::vector <CUiEntity*>::iterator p;
	for (p=m_entityVector.begin();p!=m_entityVector.end();++p)
		delete (*p);

	m_entityVector.clear();
}

int CUiScreen::AddTextEntity(int x,int y,const char* text,const CColour *col)
{
	assert(text);
	CUiEntityText *newTextEntity=new CUiEntityText(text,col);
	newTextEntity->SetPosition(x,y);
	m_entityVector.push_back(newTextEntity);
	return (int)(m_entityVector.size()-1);
}

int CUiScreen::AddBarEntity(int x,int y,bool vertical,int min,int max,int spriteId)
{
	CUiEntityBar *newBarEntity=new CUiEntityBar(min,max,vertical,spriteId);
	newBarEntity->SetPosition(x,y);
	m_entityVector.push_back(newBarEntity);
	return (int)(m_entityVector.size()-1);
}

int CUiScreen::AddImageEntity(int x,int y,int spriteId)
{
	CUiEntityImage *newImageEntity=new CUiEntityImage(spriteId);
	newImageEntity->SetPosition(x,y);
	m_entityVector.push_back(newImageEntity);
	return (int)(m_entityVector.size()-1);
}

CUiEntity* CUiScreen::GetEntityById(int id)
{
	assert(id>=0 && id<(int)m_entityVector.size());
	return m_entityVector[id];
}

void CUiScreen::Update()
{
	std::vector <CUiEntity*>::const_iterator p;
	for (p=m_entityVector.begin();p!=m_entityVector.end();++p)
		(*p)->Update();
}

void CUiScreen::Render(float dt)
{
	std::vector <CUiEntity*>::const_iterator p;
	for (p=m_entityVector.begin();p!=m_entityVector.end();++p)
		(*p)->Render(dt);
}

// Pointers can be null if not needed to change
void CUiScreen::ChangeTextEntity(int id,const char* newText,const CColour *newCol)
{
	CUiEntity *ent=GetEntityById(id);
	assert(ent);

	if (newCol)
		ent->ChangeColour(newCol);

	if (newText)
	{
		// Dangerous cast
		CUiEntityText *textEntity=(CUiEntityText*)ent;
		textEntity->ChangeText(newText);
	}
}

void CUiScreen::ChangePosition(int id,int x,int y)
{
	CUiEntity *ent=GetEntityById(id);
	assert(ent);

	ent->SetPosition(x,y);
}

void CUiScreen::SetBarValue(int id,int newValue)
{
	CUiEntity *ent=GetEntityById(id);
	assert(ent);

	// Dangerous cast
	CUiEntityBar *barEntity=(CUiEntityBar*)ent;
	barEntity->SetValue(newValue);
}
