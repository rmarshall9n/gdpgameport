#pragma once

#include <SDL_Mixer.h>

// Basic class to wrap HAPI sound calls
class CSoundManager
{
private:
	bool m_mute;
public:
	CSoundManager(void);
	~CSoundManager(void);

	bool LoadSound(const char *filename, Mix_Chunk* sound);
	void PlaySound(int id, bool loop=false) const;
	void StopSound(int id);
};
