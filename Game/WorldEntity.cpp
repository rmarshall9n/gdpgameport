// WorldEntity.cpp: implementation of the CWorldEntity class.
//
//////////////////////////////////////////////////////////////////////
#include "Common.h"
#include "WorldModel.h"
#include "WorldEntity.h"
#include "Visualisation.h"
#include "BulletSystem.h"
#include "ExplosionManager.h"
#include "Tick.h"
#include "Rectangle.h"
#include "String.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWorldEntity::CWorldEntity(const char*name)
{
	m_name=new char[strlen(name)+1];
	strcpy(m_name,name);

	m_graphicId=-1;
	m_centrePosition=CVector3(0,0,0);
	m_lastCentrePosition=CVector3(0,0,0);
	m_collisionCost=0;
	m_rotation=CVector3(0,0,0);
	m_active=true;
	m_moved=true;
	m_speed=0.0f;
	m_rotationSpeed=0.0f;
	m_health=1;
	m_maxHealth=10000;

	m_spriteWidth=0;
	m_spriteHeight=0;
	m_lastFireTime=0;
	m_fireRate=100; // milliseconds
	m_onlyFireForward=true;

	m_collisionCircleRadiusSq=0;

	m_side=CWorldEntity::eSideNeutral;

	// if -1 to start with will never count down
	m_lifeTime=-1;

	m_animationFrames=0;
	m_currentAnimationFrame=0.0f;
	m_animationSpeed=1.0f;
	m_animationMode=0;
	m_animationIncrement=true;

	m_lastUpdateTime=0;

	m_bulletTemplateId=-1;
	m_explosionTemplateId=-1;


	m_invincible=false;
}

CWorldEntity::~CWorldEntity()
{
	delete[] m_name;
}

void CWorldEntity::SetGraphicId(int newId)
{
	m_graphicId=newId;

	// We are 2D so retrieve dimensions form viz
	VIZ.GetSprite2DDimensions(m_graphicId,&m_spriteWidth,&m_spriteHeight);

	// Radius for collisions:
	//m_collisionCircleRadiusSq=sqrt((float)(m_spriteWidth*m_spriteWidth + m_spriteHeight*m_spriteHeight));
	//m_collisionCircleRadiusSq*=0.5f;
	m_collisionCircleRadiusSq=(float)pow(0.5f*m_spriteWidth,2)+(float)pow(0.5f*m_spriteHeight,2);

	// duh - but needed to speed up later real time calcs.
//	m_collisionCircleRadiusSq*=m_collisionCircleRadiusSq;

	m_currentAnimationFrame=0.0f;

	m_animationFrames=VIZ.GetSpriteAnimationData(m_graphicId,&m_animationSpeed,&m_animationMode);

	m_animationIncrement=true;
	//if (GetCollisionCost()>0) // some things are not involved in collisions
}

// Set the centre position for this entity
void CWorldEntity::SetCentrePosition(const CVector3 &pos)
{
	m_lastCentrePosition=m_centrePosition;
	m_centrePosition=pos;
	m_moved=true;
}

/**************************************************************************************************
Desc: Avoids last position probs
Date: 21/03/2003
**************************************************************************************************/
void CWorldEntity::SetStartCentrePosition(const CVector3 &pos)
{
	m_centrePosition=m_lastCentrePosition=pos;
	m_moved=true;
}

// You should only override this fn if not doing animation
void CWorldEntity::Render(float dt)
{
	int x=0;
	int y=0;


	if (!m_moved || WORLD.GetTick()->IsInterpolationOff())
	{
		//x=(int)(m_centrePosition.x-GetHalfWidth());
		//y=(int)(m_centrePosition.y-GetHalfHeight());
		worldPos.x=m_centrePosition.x-GetHalfWidth();
		worldPos.y=m_centrePosition.y-GetHalfHeight();
	}
	else
	{
		//if (m_moved)
		{
			CalculateWorldPosition(&worldPos,dt);
			m_moved=false;
		}
	}

	// Round to nearest screen integer
	x=(int)floor(worldPos.x+0.5f);
	y=(int)floor(worldPos.y+0.5f);

	if (m_animationFrames>0)
	{
		VIZ.RenderSpriteAnimate(m_graphicId,x,y,(int)m_currentAnimationFrame);
	}
	else
	{
		VIZ.RenderSprite(m_graphicId,x,y);
	}
}

/**************************************************************************************************
Desc:
Date: 15/03/2003
**************************************************************************************************/
void CWorldEntity::RotateAroundAxisY(float rot)
{
	m_rotation.y+=rot;
	if (m_rotation.y>PI*2)
		m_rotation.y-=PI*2;
	else if (m_rotation.y<0)
		m_rotation.y+=PI*2;

	m_moved=true;
}
/**************************************************************************************************
Desc:
Date: 15/03/2003
**************************************************************************************************/
CVector3 & CWorldEntity::GetRotation()
{
	return m_rotation;
}

void CWorldEntity::SetRotationY(float y)
{
	m_rotation.y=y;
	m_moved=true;
}

/**************************************************************************************************
Desc: See if we are within range of a point, passed a radius but we can use our own as well
Date: 15/03/2003
**************************************************************************************************
bool CWorldEntity::IsWithinRangeOf(const CVector3 &pnt, float radius)
{
	CVector3 dif=m_position-pnt;

	float len=D3DXVec3Length(&dif);

	assert(len>=0);

	if (D3DXVec3Length(&dif)<radius)
		return true;

	return false;
}
*/

/**************************************************************************************************
Desc: Collisions are checked for each update. If occured we may have to not move into that space
Date: 15/03/2003
**************************************************************************************************/
void CWorldEntity::CantMoveThere(bool collidedWithPlayer)
{
	SetToLastPosition();
	m_moved=true;
}

void CWorldEntity::SetToLastPosition()
{
	m_centrePosition=m_lastCentrePosition;
	m_moved=true;
}

/**************************************************************************************************
Desc:
Date: 21/03/2003
**************************************************************************************************
void CWorldEntity::CalculateWorldMatrix(D3DXMATRIXA16 *matWorld,float dt)
{
	assert(dt>=0.0f && dt<=1.0f);

	D3DXMatrixIdentity(matWorld );

	// Interpolate between last position and current position based on dt
	CVector3 diff=m_centrePosition-m_lastCentrePosition;

	CVector3 lerpPos=m_centrePosition+(diff*dt);

	// Position is centre of sprite. We need top left.
	D3DXVECTOR2 pos(lerpPos.x-GetHalfWidth(),lerpPos.y-GetHalfHeight());

	D3DXMatrixTransformation2D(matWorld,NULL,NULL,NULL,NULL,0,&pos);
}*/

void CWorldEntity::CalculateWorldPosition(CVector3 *worldPos,float dt)
{
	assert(dt>=0.0f && dt<=1.0f);

	// Interpolate between last position and current position based on dt
	CVector3 diff=m_centrePosition-m_lastCentrePosition;

	CVector3 lerpPos=m_centrePosition+(diff*dt);

	// Position is centre of sprite. We need top left.
	//D3DXVECTOR2 pos(lerpPos.x-GetHalfWidth(),lerpPos.y-GetHalfHeight());
	//D3DXMatrixTransformation2D(matWorld,NULL,NULL,NULL,NULL,0,&pos);

	worldPos->x=lerpPos.x-GetHalfWidth();
	worldPos->y=lerpPos.y-GetHalfHeight();
	worldPos->z=0;
}

// returns true if fired
bool CWorldEntity::Fire()
{
	assert(GetActive());

	unsigned int timeNow = SDL_GetTicks();

	if (timeNow-m_lastFireTime<(DWORD)m_fireRate)
		return false;

	m_lastFireTime=timeNow;

	CVector3 pos=GetCentrePosition();
	CVector3 rot=GetRotation();

	CVector3 dir;
	if (!m_onlyFireForward)
	{
		// down is 180 - apply +-20deg random - could be a variable?
		int deg=180+(rand()%40)-20;

		rot.x=0.0f;
		rot.y=DEGTORAD(deg);
		rot.z=0;
	}

	// fire a bullet from current pos in current dir
	float cosAngle=static_cast<float>(cos(rot.y));
	float sinAngle=static_cast<float>(sin(rot.y));

	dir.x=sinAngle;
	dir.y=cosAngle;
	dir.z=0.0f;

	// Due to screen origin being top left:
	dir.y=-dir.y;

	// We are positioned in the middle of the sprite
	// Move 'forward' to front edge of sprite in facing direction
	pos.x+=dir.x*GetHalfWidth();
	pos.y+=dir.y*GetHalfHeight();

	// bit of random
	if (m_onlyFireForward)
	{
		int ran=rand()%3;
		pos.x+=(float)(ran-1);
	}

	WORLD.GetBulletSystem()->Fire(pos,rot,m_bulletTemplateId,m_side);

	return true;
}

// To rotate one vector into another, you need to define an axis of rotation.
	// You do this by computing the cross product of your start and end vectors:
	//CVector3 rotationAxis=D3DXVec3Cross(forward,direction);
	//Now you need to construct a matrix that represents this rotation:
	//D3DXMatrixRotationAxis( &matRot, &rotationAxis, angle );
	// If you use this matrix to transform your model, it will face whatever direction you want.

/**************************************************************************************************
Desc:
Date: 15/03/2003
**************************************************************************************************/
void CWorldEntity::MoveForward()
{
	CVector3 pos=GetCentrePosition();
	CVector3 rot=GetRotation();

	float cosAngle=static_cast<float>(cos(rot.y));
	float sinAngle=static_cast<float>(sin(rot.y));

	CVector3 dir;

	dir.x=sinAngle;
	dir.y=cosAngle;
	dir.z=0.0f;

	// Due to screen origin being top left:
	dir.y=-dir.y;

	pos+=dir*GetSpeed();

	SetCentrePosition(pos);
}

// Determine if a collision occured between us and otherEntity (normally bullet)
bool CWorldEntity::CheckForCollision(CWorldEntity *otherEntity)
{
	assert(otherEntity);

	// Can do distance check first but note

	// Get distance between
	CVector3 ourPos=GetCentrePosition();
	CVector3 otherPos=otherEntity->GetCentrePosition();

	// Distance between is length of line
	//float dist=D3DXVec3LengthSq(&CVector3(otherPos-ourPos));

	float distSq=(float)pow(otherPos.x-ourPos.x,2)+(float)pow(otherPos.y-ourPos.y,2);
	float collisionDistanceSq=GetCollisionCircleRadiusSq()+otherEntity->GetCollisionCircleRadiusSq();

	if (distSq<collisionDistanceSq)
	{
		ourPos=GetCentrePosition();
		otherPos=otherEntity->GetCentrePosition();

		CRectangle us;
		us.m_left=(long)(ourPos.x-GetHalfWidth());
		us.m_right=(long)(us.m_left+GetWidth());
		us.m_top=(long)(ourPos.y-GetHalfHeight());
		us.m_bottom=(long)(us.m_top+GetHeight());

		// 10% smaller (5% each side)
		float wAdjust=0.5f*(0.1f*GetWidth());
		float hAdjust=0.5f*(0.1f*GetHeight());

		us.m_left+=(long)wAdjust;
		us.m_right-=(long)wAdjust;
		us.m_top+=(long)hAdjust;
		us.m_bottom-=(long)hAdjust;

		CRectangle other;
		other.m_left=(long)(otherPos.x-otherEntity->GetHalfWidth());
		other.m_right=(long)(other.m_left+otherEntity->GetWidth());
		other.m_top=(long)(otherPos.y-otherEntity->GetHalfHeight());
		other.m_bottom=(long)(other.m_top+otherEntity->GetHeight());

		// 10% smaller (5% each side)
		wAdjust=0.5f*(0.1f*otherEntity->GetWidth());
		hAdjust=0.5f*(0.1f*otherEntity->GetHeight());

		other.m_left+=(long)wAdjust;
		other.m_right-=(long)wAdjust;
		other.m_top+=(long)hAdjust;
		other.m_bottom-=(long)hAdjust;

		if (us.m_right<other.m_left)
			return false;

		if (us.m_left>other.m_right)
			return false;

		if (us.m_top>other.m_bottom)
			return false;

		if (us.m_bottom<other.m_top)
			return false;

		return true;
	}

	return false;
}

// Kill request - may enter die sequence
void CWorldEntity::Explode()
{
	if (m_explosionTemplateId!=-1)
	{
		// may eventually pass more things about us to affect explosion
		WORLD.GetExplosionManager()->Explode(GetCentrePosition(),m_explosionTemplateId);

		// HACK - for big things e.g. the boss
		// tbd
		if (GetWidth()==256)
		{
			for (int i=0;i<8;i++)
			{
				CVector3 pos=GetCentrePosition();

				pos.x+=(rand()%256)-128;
				pos.y+=(rand()%256)-128;

				WORLD.GetExplosionManager()->Explode(pos,m_explosionTemplateId);
			}
		}
	}

	DieNow();
}

// General updates
void CWorldEntity::Update()
{
	if (!GetActive())
		return;

	unsigned int timeNow = SDL_GetTicks();

	//if (timeNow-m_lastUpdateTime<1000)
	//	return;

	m_lastUpdateTime=timeNow;

	if (m_lifeTime!=-1)
	{
		m_lifeTime--;
		if (m_lifeTime==0)
		{
			HandlePreDeathActions();
			DieNow();
			return;
		}
	}
	if (m_animationFrames>0)
	{
		if (m_animationIncrement)
			m_currentAnimationFrame+=m_animationSpeed;
		else
			m_currentAnimationFrame-=m_animationSpeed;

		if (m_currentAnimationFrame>=m_animationFrames || m_currentAnimationFrame<0)
		{
			switch (m_animationMode)
			{
				case 0: // loop
					m_currentAnimationFrame=0;
				break;
				case 1: // Reverse
					if (m_currentAnimationFrame>=m_animationFrames)
						m_currentAnimationFrame=(float)m_animationFrames-1;
					else if (m_currentAnimationFrame<0)
						m_currentAnimationFrame=1;

					m_animationIncrement=!m_animationIncrement;
				break;
				default:
//ry					EsError(_T("Unknown mode"));
				break;
			}

		}
	}

	UpdateSpecific();

}



// Set inactive and set some vars to initial values
// If called direct avoids explosion
void CWorldEntity::DieNow()
{
	m_active=false;
	//m_graphicId=-1;

	Reset();
}

// Called when about to be recreated
void CWorldEntity::Reset()
{
	//m_lifeTime=-1;
	//m_animationFrames=0;
	m_currentAnimationFrame=0;
	//SetHealth(m_maxHealth);
	ResetSpecific();
}

// returns true if dead
bool CWorldEntity::TakeDamage(int cost)
{
	int newHealth=GetHealth()-cost;
	SetHealth(newHealth);
	if (m_health<=0)
	{
		// for test can make invincible
		if (m_invincible)
		{
			m_health=m_maxHealth; // some default value
		}
		else
		{
			Explode();
			return true;
		}
	}

	return false;
}
