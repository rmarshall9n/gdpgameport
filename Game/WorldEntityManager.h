#pragma once

#include "Vector3.h"

class CWorldEntity;
class CWorldEntityPlayer;

class CEntityTypeRange
{
private:
	friend class CWorldEntityManager;
	int m_start;
	int m_length;
public:
	CEntityTypeRange():m_start(0),m_length(0) {}
	~CEntityTypeRange(){}

	inline int GetStart() const {return m_start;}
	inline int GetLength() const {return m_length;}
	inline int GetEnd() const {return m_start+m_length;}

	inline void Initialise(int start,int length)
	{
		assert(length>0);

		m_start=start;
		m_length=length;
	};
};

// Handle creation / deletion and access to world entities
// Also manages area of the vector for certain types of things, e.g. bullets, enemies
class CWorldEntityManager
{
private:
	std::vector<CWorldEntity*> m_entityVector;

	CEntityTypeRange *m_bulletRange;
	CEntityTypeRange *m_enemyRange;
	CEntityTypeRange *m_explosionRange;
	CEntityTypeRange *m_powerUpRange;

	int m_playerIndex;
public:
	CWorldEntityManager(void);
	~CWorldEntityManager(void);

	inline CWorldEntity *GetEntity(int index) const
	{
		assert(index>=0 && index<(int)m_entityVector.size());
		return m_entityVector[index];
	}

	void Initialise(int maxEntities,int maxBullets,int maxEnemies,int maxExplosions,int maxPowerUps,int backgroundGraphicId);
	CWorldEntity* CreateBackground(int graphicId);
	CWorldEntity* CreatePlayer(int graphicId,const CVector3 &startPos,int bulletType);

	inline CEntityTypeRange *GetBulletRange() const {assert(m_bulletRange); return m_bulletRange;}
	inline CEntityTypeRange *GetEnemyRange() const {assert(m_enemyRange); return m_enemyRange;}
	inline CEntityTypeRange *GetExplosionRange() const {assert(m_explosionRange); return m_explosionRange;}
	inline CEntityTypeRange *GetPowerUpRange() const {assert(m_powerUpRange); return m_powerUpRange;}

	inline int GetPlayerIndex() const {return m_playerIndex;}
	inline CWorldEntityPlayer *GetPlayerEntity() const {return (CWorldEntityPlayer*)GetEntity(m_playerIndex);}

	bool AreEntitiesEnemies(int ent1,int ent2) const;

	// Because I cannot be arsed to write all the pass through stuff!
	std::vector<CWorldEntity*> &GetEntityVector() {return m_entityVector;}

	void Reset();

};
