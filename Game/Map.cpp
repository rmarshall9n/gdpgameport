//**************************************************************************************************
//
// Map.cpp: implementation of the CMap class.
//
// Description: Handles parsing of map
//
// Created by: Keith Ditchburn 22/03/2003
//
//**************************************************************************************************
#include "Common.h"
#include "Map.h"

const int kMapWidth=128;
const int kMapHeight=128;

/**************************************************************************************************
**************************************************************************************************/
CMap::CMap()
{
	m_mapData=NULL;
	m_screenWidth=0;
	m_screenHeight=0;

#if defined(WINCE)
	m_driftRate=1.0f; // speed things drift down the screen
#else
	m_driftRate=2.0f; // speed things drift down the screen
#endif
}

/**************************************************************************************************
**************************************************************************************************/
CMap::~CMap()
{
	if (m_mapData)
		delete []m_mapData;
}

/**************************************************************************************************
Desc:
Date: 22/03/2003
**************************************************************************************************/
bool CMap::LoadMap(const char *filename)
{
	return false;
	//assert(filename);
	//
	//LPDIRECT3DTEXTURE9 texture;

	//// Load the map texture
	//HRESULT hr=D3DXCreateTextureFromFileEx(gD3dDevice,filename,0,0,1,0,D3DFMT_A8R8G8B8,
	//	D3DPOOL_MANAGED,D3DX_FILTER_NONE,D3DX_FILTER_NONE,0,NULL,NULL,&texture);

	//if (FAILED(hr))
	//	return false;

	//// Read map data in
	//if (!GetDataFromTexture(texture))
	//{
	//	UserMessage("Map data is not in correct format","Error");
	//	texture->Release();
	//	return false;
	//}

	//// Finished with texture
	//texture->Release();
	//

	//return true;
}

/**************************************************************************************************
Desc: Copy the texture data for lookup purposes and speed
Date: 22/06/2002
**************************************************************************************************
bool CMap::GetDataFromTexture(LPDIRECT3DTEXTURE9 texture)
{
	D3DSURFACE_DESC surfaceDesc;
	texture->GetLevelDesc(0,&surfaceDesc);

	// Only ever work with 32 bit format
	assert(surfaceDesc.Format==D3DFMT_A8R8G8B8);

	if (surfaceDesc.Width!=kMapWidth)
		return false;

	if (surfaceDesc.Height!=kMapHeight)
		return false;

	if (m_mapData)
		delete []m_mapData;

	// Create colour buffer
	m_mapData=new EMapValue[surfaceDesc.Width*surfaceDesc.Height];

	D3DLOCKED_RECT locked;
	if (FAILED(texture->LockRect(0,&locked,NULL,0)))
		return false;

	BYTE *bytePointer=(BYTE*)locked.pBits;

	DWORD count=0;
	for (DWORD y=0;y<surfaceDesc.Height;y++)
	{
		for (DWORD x=0;x<surfaceDesc.Width;x++)
		{
			DWORD index=(x*4+(y*(locked.Pitch)));

			// B
			BYTE b=bytePointer[index];
			// G
			BYTE g=bytePointer[index+1];
			// R
			BYTE r=bytePointer[index+2];
			// Alpha
			BYTE a=bytePointer[index+3];

			// Extra one
			if (r==255 && g==255 && b==255)
				m_mapData[count]=eMapValueWhite;
			else
			if (r>0)
				m_mapData[count]=eMapValueRed;
			else if (g>0)
				m_mapData[count]=eMapValueGreen;
			else if (b>0)
				m_mapData[count]=eMapValueEnemy;
			else
				m_mapData[count]=eMapValueEmpty;

			count++;
		}
	}

	texture->UnlockRect(0);

	return true;
}*/

EMapValue CMap::GetMapValue(int x, int y)
{
	assert(x>=0 && x<=kMapWidth);
	assert(y>=0 && y<=kMapHeight);

	return m_mapData[y*kMapWidth+x];

}

void CMap::SetScreenDimensions(int w,int h)
{
	m_screenWidth=w;
	m_screenHeight=h;
}
