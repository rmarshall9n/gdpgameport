#include "common.h"
#include "UiEntityText.h"
#include "Colour.h"
#include "Visualisation.h"

#include <string.h>
#include <assert.h>

const unsigned int kMaxUiTextString=2048;

CUiEntityText::CUiEntityText(const char* text,const CColour *col) : CUiEntity(col)
{
	assert(strlen(text) < kMaxUiTextString);

	// Since text can change allocate it just once
	m_text = new char[kMaxUiTextString];
	strcpy(m_text,text);
}

CUiEntityText::~CUiEntityText(void)
{

	delete []m_text;
}

void CUiEntityText::ChangeText(const char *newText)
{
	assert(newText);
	assert(m_text);
	assert(strlen(newText)<kMaxUiTextString);

	strcpy(m_text,newText);
}

void CUiEntityText::Update()
{
}

void CUiEntityText::Render(float dt)
{
	VIZ.RenderText(GetScreenX(),GetScreenY(),m_text,
		GetColour()->m_red,GetColour()->m_green,GetColour()->m_blue,GetColour()->m_alpha);
}

