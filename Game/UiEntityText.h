#ifndef UIENTITYTEXT_H
#define UIENTITYTEXT_H

#include "UiEntity.h"

class CColour;

/**
 * \class CUiEntityText
 * \brief Represents a text UI entity
 * \author Keith Ditchburn \date 24 September 2005
*/
class CUiEntityText : public CUiEntity
{
private:
	char* m_text;
public:
	CUiEntityText(const char* text,const CColour *col);
	~CUiEntityText(void);

	void Update();
	void Render(float dt);

	void ChangeText(const char* newText);
};

#endif // UIENTITYTEXT_H
