#include "Common.h"
#include "LevelChunkData.h"

// The main chunk keywords
char gChunkKeywords[eNumLevelChunkKeywords][kMaxLineLength] =
{
	"NO CHUNK",
    "Graphic",
    "Bullet",
    "Explosion",
    "Enemy",
    "Route",
    "Wave"
};

// Each chunks keywords
char gGraphicsKeywords[eNumGraphicEntity][kMaxLineLength] =
{
	"filename",
    "name",
    "frames",
    "frameSize",
    "frameSpeed",
    "frameMode"
};

char gBulletKeywords[eNumBulletEntity][kMaxLineLength] =
{
	"name",
	"graphic",
	"explosion",
	"speed",
	"damage",
	"multi"
};

char gExplosionKeywords[eNumExplosionEntity][kMaxLineLength] =
{
	"name",
	"graphicName"
};

char gEnemyEntityKeywords[eNumEnemyEntity][kMaxLineLength] =
{
	"name",
	"graphic",
	"bullet",
	"explosion",
	"collisionCost",
	"health",
	"speed",
	"rotSpeed",
	"onlyFireForward",
	"fireRate",
	"chanceOfFiring",
	"score"
};

char gRouteKeywords[eNumRoute][kMaxLineLength]=
{
	"name",
	"numWayPoints",
	"wp"
};
