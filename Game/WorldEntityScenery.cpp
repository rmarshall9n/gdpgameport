// WorldEntityScenery.cpp: implementation of the CWorldEntityScenery class.
//
//////////////////////////////////////////////////////////////////////
#include "Common.h"
#include "WorldEntityScenery.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWorldEntityScenery::CWorldEntityScenery(const char *name): CWorldEntity(name)
{

}

CWorldEntityScenery::~CWorldEntityScenery()
{

}

/**************************************************************************************************
Desc: This object does nothing
Date: 15/03/2003
**************************************************************************************************/
void CWorldEntityScenery::UpdateSpecific()
{

}

