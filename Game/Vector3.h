#pragma once

#include <math.h>

/**
 * \class CVector3
 * \brief a 3D vector
 * \author Keith Ditchburn \date 16 October 2005
*/
class CVector3
{
public:
	float x,y,z;

	// Constructors
	CVector3(void) : x(0),y(0),z(0) {}
	CVector3(float xin,float yin,float zin) : x(xin),y(yin),z(zin) {}
	CVector3(const CVector3 &other) : x(other.x), y(other.y), z(other.z) {}

	// Operators
	const CVector3& operator -=(const CVector3 &rhs)
	{
		x-=rhs.x;
		y-=rhs.y;
		z-=rhs.z;
		return *this;
	};

	const CVector3& operator +=(const CVector3 &rhs)
	{
		x+=rhs.x;
		y+=rhs.y;
		z+=rhs.z;
		return *this;
	};

	const CVector3& operator /=(const CVector3 &rhs)
	{
		x/=rhs.x;
		y/=rhs.y;
		z/=rhs.z;
		return *this;
	};

	const CVector3& operator *=(const CVector3 &rhs)
	{
		x*=rhs.x;
		y*=rhs.y;
		z*=rhs.z;
		return *this;
	};

	const CVector3 operator -(const CVector3 &rhs) const
	{
		CVector3 ret(*this);
		ret-=rhs;
		return ret;
	}

	const CVector3 operator +(const CVector3 &rhs) const
	{
		CVector3 ret(*this);
		ret+=rhs;
		return ret;
	}

	const CVector3 operator /(const CVector3 &rhs) const
	{
		CVector3 ret(*this);
		ret/=rhs;
		return ret;
	}

	const CVector3 operator *(const CVector3 &rhs) const
	{
		CVector3 ret(*this);
		ret*=rhs;
		return ret;
	}

	const CVector3 operator *(const float &fl) const
	{
		CVector3 ret(*this);
		ret.x*=fl;
		ret.y*=fl;
		ret.z*=fl;
		return ret;
	}

	// Functions
	inline float SquaredLength() const {return x*x+y*y+z*z;}
	inline float Length() const {return (float)sqrt(x*x+y*y+z*z);}
	inline void Normalise()
	{
		float len=Length();
		if(len==0)
			return;

		x/=len;
		y/=len;
		z/=len;
	}
	inline float Dot(const CVector3 &with) const
	{
		return (x * with.x) + (y * with.y) + (z * with.z);
	}

	inline CVector3 Cross(const CVector3 &with) const
	{
		CVector3 ret(*this);

		ret.x = y * with.z - with.y * z;
		ret.y = z * with.x - with.z * x;
		ret.z = x * with.y - with.x * y;

		return ret;
	}

	~CVector3(void){};
};
