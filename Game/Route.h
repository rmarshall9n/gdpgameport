#pragma once

#include "Vector3.h"

// AI route to travel aorund the screen
class CRoute
{
private:
	int m_numWayPoints;
	CVector3 *m_wayPoints;
	char *m_name;
public:
	CRoute(const char *name);
	~CRoute(void);

	void AddWayPoint(int index,const CVector3 &pnt);
	void Initialise(int numPoints);

	inline const CVector3 &GetWayPoint(int index) const {assert(index>=0 && index<m_numWayPoints);return m_wayPoints[index];}
	inline int GetNumWayPoints() const {return m_numWayPoints;}

	inline const char* GetName() const {return m_name;}
};
