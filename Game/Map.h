//**************************************************************************************************
//
// Map.h: interface for the CMap class.
//
// Description: Handles parsing of map
//
// Created by: Keith Ditchburn 22/03/2003
//
//**************************************************************************************************

#pragma once


enum EMapValue
{
	eMapValueEmpty,
	eMapValueRed,
	eMapValueGreen,
	eMapValueEnemy,
	eMapValueWhite
};

class CMap
{
private:
	EMapValue *m_mapData;
	//bool GetDataFromTexture(LPDIRECT3DTEXTURE9 texture);

	int m_screenWidth;
	int m_screenHeight;

	float m_driftRate;

public:
	EMapValue GetMapValue(int x,int y);
	bool LoadMap(const char* filename);
	CMap();
	~CMap();

	void SetScreenDimensions(int w,int h);
	int GetScreenWidth() const {return m_screenWidth;}
	int GetScreenHeight() const {return m_screenHeight;}
	float GetDriftRate() const {return m_driftRate;}
};


