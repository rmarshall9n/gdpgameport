#pragma once

#include "UiEntityTypes.h"

class CUiEntity;
class CColour;

/**
 * \class CUiScreen
 * \brief A screen contains a set of UI elements
 * \author Keith Ditchburn \date 24 September 2005
*/
class CUiScreen
{
private:
	std::vector<CUiEntity*> m_entityVector;
	bool m_enabled;
	char *m_name;
public:
	CUiScreen(const char *name);
	~CUiScreen(void);

	int AddTextEntity(int x,int y,const char* text,const CColour *col);
	int AddBarEntity(int x,int y,bool vertical,int min,int max,int spriteId);
	int AddImageEntity(int x,int y,int spriteId);

	void ChangeTextEntity(int id,const char* newText,const CColour *newCol);
	void SetBarValue(int id,int newValue);

	CUiEntity* GetEntityById(int id);

	void Update();
	void Render(float dt);

	void TurnOn() {m_enabled=true;}
	void TurnOff() {m_enabled=false;}
	bool GetIsOn() const {return m_enabled;}

	void ChangePosition(int id,int x,int y);
};
