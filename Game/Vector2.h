#pragma once

/**
 * \class CVector2
 * \brief a 2D vector
 * \author Keith Ditchburn \date 01 October 2005
*/
class CVector2
{
public:
	float x,y;

	// Constructors
	CVector2(void) : x(0),y(0) {}
	CVector2(float xin,float yin) : x(xin),y(yin) {}
	CVector2(const CVector2 &other) : x(other.x), y(other.y) {}

	// Operators
	const CVector2 & CVector2::operator -=(const CVector2 &rhs)
	{
		x-=rhs.x;
		y-=rhs.y;
		return *this;
	};

	const CVector2 & CVector2::operator +=(const CVector2 &rhs)
	{
		x+=rhs.x;
		y+=rhs.y;
		return *this;
	};

	const CVector2 & CVector2::operator /=(const CVector2 &rhs)
	{
		x/=rhs.x;
		y/=rhs.y;
		return *this;
	};

	const CVector2 & CVector2::operator *=(const CVector2 &rhs)
	{
		x*=rhs.x;
		y*=rhs.y;
		return *this;
	};

	const CVector2 operator -(const CVector2 &rhs)
	{
		CVector2 ret(*this);
		ret-=rhs;
		return ret;
	}

	const CVector2 operator +(const CVector2 &rhs)
	{
		CVector2 ret(*this);
		ret+=rhs;
		return ret;
	}

	const CVector2 operator /(const CVector2 &rhs)
	{
		CVector2 ret(*this);
		ret/=rhs;
		return ret;
	}

	const CVector2 operator *(const CVector2 &rhs)
	{
		CVector2 ret(*this);
		ret*=rhs;
		return ret;
	}

	// Functions
	inline float SquaredLength() const {return x*x+y*y;}
	inline float Length() const {return sqrt(x*x+y*y);}
	inline void Normalise() 
	{
		float len=Length();
		if(len==0) 
			return;

		x/=len;
		y/=len;
	}
	~CVector2(void){};
};
