//**************************************************************************************************
//
// WorldEntityEnemy.cpp: implementation of the CWorldEntityEnemy class.
//
// Description: Enemy entity. Moves around the world randomly.
//
// Created by: Keith Ditchburn 15/03/2003
//
//**************************************************************************************************
#include "Common.h"
#include "WorldModel.h"
#include "WorldEntityEnemy.h"
#include "map.h"

#include "String.h"
#include <SDL.h>

/**************************************************************************************************
**************************************************************************************************/
CWorldEntityEnemy::CWorldEntityEnemy(const char *name): CWorldEntity(name)
{
	m_wayPoints=NULL;
	m_numWayPoints=0;
	m_currentWayPoint=0;
	m_chanceOfFiring=1;
	m_scoreReward=0;

	ResetSpecific();
}

void CWorldEntityEnemy::ResetSpecific()
{
	SetActive(false);
	//m_active=false;
	//m_numWayPoints=0;CVector3
	//SAFE_DELETE_ARRAY(m_wayPoints);

	//int x=(rand()%(gMap->GetScreenWidth()));
	//int y=(rand()%(gMap->GetScreenHeight()/4));

	//CVector3 pos;
	//pos.x=static_cast<float>(x);
	//pos.y=static_cast<float>(y);
	//pos.z=0;

	//SetCentrePosition(pos);

	////GetNewDestination();
	//ChangeMode(eEnemyModeRotating);
}

void CWorldEntityEnemy::InitFromTemplate(const CWorldEntityEnemy *other)
{
	assert(other);

	SetCollisionCost(other->GetCollisionCost());
	SetGraphicId(other->GetGraphicId());
	SetExplosionTemplateId(other->GetExplosionTemplateId());
	SetBulletTemplateId(other->GetBulletTemplateId());
	SetHealth(other->GetHealth());
	SetSpeed(other->GetSpeed());
	SetRotationSpeed(other->GetRotationSpeed());
	SetOnlyFireForward(other->m_onlyFireForward==1);
	SetFireRate(other->m_fireRate);
	m_chanceOfFiring=other->m_chanceOfFiring;
	SetScoreReward(other->GetScoreReward());

	// Not copied from template
	m_numWayPoints=0;
	SAFE_DELETE_ARRAY(m_wayPoints);

	//GetNewDestination();
	//ChangeMode(eEnemyModeRotating);
}

/**************************************************************************************************
**************************************************************************************************/
CWorldEntityEnemy::~CWorldEntityEnemy()
{
	SAFE_DELETE_ARRAY(m_wayPoints);
}

/**************************************************************************************************
Desc:
Date: 15/03/2003
**************************************************************************************************/
void CWorldEntityEnemy::UpdateSpecific()
{
	if (m_currentMode==eEnemyModeRotating)
	{
		if (RotateTowardDestination())
			ChangeMode(eEnemyModeMoving);
		//else
		// Move forward whatever
			MoveForward();
	}
	else if (m_currentMode==eEnemyModeMoving)
	{
		MoveForward();
	}

	// Check first if we are doing wave points
	if (m_numWayPoints>0)
	{
		if (AreWeAtDestination())
			ReachedWavePoint();
	}

	if (m_numWayPoints==0 && AreWeAtDestination())
		GetNewDestination();

	// Every now and then fire - TBD how fast
	// Do not fire unless vaguely facing downward
	// was 120 and 240
	if (m_onlyFireForward)
	{
		if (GetRotation().y<DEGTORAD(120) || GetRotation().y>DEGTORAD(240))
			return;
	}

	if (rand()%m_chanceOfFiring==0)
		Fire();
	else
		m_lastFireTime = SDL_GetTicks(); // missed the chance
}

/**************************************************************************************************
Desc: Find new dest, dont let it be too close
Date: 15/03/2003
**************************************************************************************************/
void CWorldEntityEnemy::GetNewDestination()
{
	CVector3 pos=GetCentrePosition();
	CVector3 newDestination(pos);

	newDestination.y=pos.y;

	float dist=0.0f;

	// depends on map size!
	while(dist<20)
	{
		int x=(rand()%(WORLD.GetMap()->GetScreenWidth()-20  ))+10;
		int y=(rand()%(WORLD.GetMap()->GetScreenHeight()/2));

		newDestination.x=static_cast<float>(x);
		newDestination.y=static_cast<float>(y);
		newDestination.z=0;

		CVector3 dif=pos-newDestination;

#if defined(USE_DIRECTX)
		dist=D3DXVec3Length(&dif);
#else
		dist=dif.Length();
#endif
	}

	SetDestination(newDestination);

	//DebugOutput("Got new destination %f %f which is %f away\n",m_destination.x,m_destination.y,dist);

	ChangeMode(eEnemyModeRotating);
}

void CWorldEntityEnemy::SetDestination(const CVector3& dest)
{
	m_lastDestination=m_destination;
	m_destination=dest;
}

// We only rotate around the y axis
bool CWorldEntityEnemy::RotateTowardDestination()
{
	float currentRotation=GetRotation().y;
	CVector3 currentPosition=GetCentrePosition();

	// Find the angle we want from the destination minus where we are
	CVector3 requiredDirection=m_destination-currentPosition;

#if defined(USE_DIRECTX)
	D3DXVec3Normalize(&requiredDirection,&requiredDirection);
#else
	requiredDirection.Normalise();
#endif

	// Which direction is forward at start up
	CVector3 forwardFacing=CVector3(0.0f,-1.0f,0.0f);

	// Now you need the angle to rotate about this axis.
	// You can calculate this by taking the arccos of the dot product of your start and end vectors:
	// cos A = v1.v2
#if defined(USE_DIRECTX)
	float requiredRotation = acosf(D3DXVec3Dot(&forwardFacing, &requiredDirection));
#else
	// acosf not on WinCE
	float requiredRotation = (float)acos(forwardFacing.Dot(requiredDirection));
#endif

	// need cross to work out which half it is in i.e. between 0 and PI or PI and 2PI
	// This problem occurs because cos(alpha) = cos(-alpha)
	// So if one vector is to the right or left of the other
	// Cross gives vector that is
	CVector3 res;
#if defined(USE_DIRECTX)
	D3DXVec3Cross(&res,&forwardFacing,&requiredDirection);
#else
	res=forwardFacing.Cross(requiredDirection);
#endif

	// If the result is up then current is to the right of forward facing else it is to the left
	// If it is to the left we need to adjust it into the second half (PI to 2PI)
	if (res.z<0.0f)
		requiredRotation=2*PI-requiredRotation;

// test
//SetRotationY(requiredRotation);
//return true;





	float absoluteDifference=max(currentRotation,requiredRotation)-min(currentRotation,requiredRotation);

	float difference=requiredRotation-currentRotation;

	// If the difference between the 2 is > PI we rotate the other way
	if (absoluteDifference>=PI)
	{
		// Calc rotating the other way
		if (currentRotation>requiredRotation)
			difference=requiredRotation+2*PI-currentRotation;
		else
			difference=currentRotation+2*PI-requiredRotation;
	}

	assert(difference<=PI && difference>=-PI);

	// allow a bit of devience for testing when we are correct - must depend on rotation speed
	if (absoluteDifference<=GetRotationSpeed())
	{
		// set it to an exact value
		SetRotationY(requiredRotation);
		return true;
	}

	if (difference>0)
		RotateAroundAxisY(GetRotationSpeed());
	else
		RotateAroundAxisY(-GetRotationSpeed());

	return false;
}



/**************************************************************************************************
Desc:
Date: 15/03/2003
**************************************************************************************************/
bool CWorldEntityEnemy::AreWeAtDestination()
{
	CVector3 pos=GetCentrePosition();

	float dx=pos.x-m_destination.x;
	float dy=pos.y-m_destination.y;

	float distSqr=(dx*dx)+(dy*dy);

	//DebugOutput("Distance is now %f\n",sqrt(distSqr));

	if (distSqr<=GetSpeed()*GetSpeed())
	{
		return true;
	}

	return false;
}
/**************************************************************************************************
Desc: Rotate 2d vector around the origin
Date: 11/11/2002
**************************************************************************************************
void CUseful::RotateVectorAroundOrigin(D3DXVECTOR2 *vec, float angle)
{
	assert(vec);

	float cosAngle=cos(angle);
	float sinAngle=sin(angle);

	float x=vec->x * cosAngle - vec->y * sinAngle;
	float y=vec->x * sinAngle + vec->y * cosAngle;

	vec->x=x;
	vec->y=y;
}
*/

/**************************************************************************************************
Desc: for some reason we cannot move where we want to - must be a collision
Date: 21/03/2003
**************************************************************************************************/
void CWorldEntityEnemy::CantMoveThere(bool collidedWithPlayer)
{
	SetToLastPosition();
/*
	if (m_currentMode==eEnemyModeReversing)
	{
		// if it is player they may have run into us so no need to change here
		if (!collidedWithPlayer)
		// It cannot have worked so:

		GetNewDestination();
	}
	else
		ChangeMode(eEnemyModeReversing);
*/
}

/**************************************************************************************************
Desc:
Date: 21/03/2003
**************************************************************************************************/
void CWorldEntityEnemy::ChangeMode(EEnemyMode newMode)
{
	m_currentMode=newMode;
	m_timeOfModeChange= SDL_GetTicks();
}

// Sets us to travel between way points
void CWorldEntityEnemy::TravelWayPoints(CVector3 *pnts,int numPoints)
{
	assert(pnts);
	assert(numPoints>1);

	SAFE_DELETE_ARRAY(m_wayPoints);
	m_numWayPoints=numPoints;

	m_wayPoints=new CVector3[numPoints];
	memcpy(m_wayPoints,pnts,sizeof(CVector3)*numPoints);

	// Start
	m_currentWayPoint=0;
	SetStartCentrePosition(m_wayPoints[m_currentWayPoint]);
	SetCentrePosition(m_wayPoints[m_currentWayPoint]);
	SetDestination(m_wayPoints[m_currentWayPoint+1]);
	SetRotationY(PI);
	ChangeMode(eEnemyModeRotating);
}

// We have reached wave point - go to next
void CWorldEntityEnemy::ReachedWavePoint()
{
	m_currentWayPoint++;
	if (m_currentWayPoint>=m_numWayPoints)
		m_currentWayPoint=0;

	int destinationWavePoint=m_currentWayPoint+1;
	if (destinationWavePoint>=m_numWayPoints)
		destinationWavePoint=0;

	SetDestination(m_wayPoints[destinationWavePoint]);
	ChangeMode(eEnemyModeRotating);
}
