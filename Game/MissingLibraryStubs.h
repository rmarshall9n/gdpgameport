#pragma once
/*
	KD: 24/10/13
	The original version of this demo used some libraries I have developed over the years. These are not provided
	and so this file is used to hold stubs for those functions. You will need to write your own versions of these.
*/

#include <iostream>

// From an error system library:
#define EsCheckAlloc(x) assert(x!=0)

void EsOutput(const char* str,...);

void UserMessage(const char* message,const char *title);

#define EsError(x)  {EsOutput(x);assert(false);}

#define EsAssert(x) assert(x)

// Some things that were in windows.h:
#if defined(CAANOO)
#define MAX_PATH	260
#endif

//ry void SetCurrentDirectory(const char *dir);
void GetCurrentDirectory(size_t bufSize,char *buf);

#define TOCHAR(x) x

#define LONG32 long
#define INT32 int
