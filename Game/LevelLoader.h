#pragma once

#include "LevelChunkData.h"
#include "TextFile.h"

/**
* \class CLevelLoader
* \brief Purely to handle level loading
* \author Keith Ditchburn
* \date 22/12/2004
*/
class CLevelLoader
{
private:
	ELevelChunkKeywords m_currentChunk;
	Utility::CTextFile *m_textFile;

	// Chunk data
	TGraphicEntityChunk m_graphicEntityChunk;
	TEnemyEntityChunk m_enemyEntityChunk;
	TBulletEntityChunk m_bulletEntityChunk;
	TExplosionEntityChunk m_explosionEntityChunk;
	TRouteChunk m_routeChunk;

	void ChunkStarting(ELevelChunkKeywords newChunk);
	void ChunkEnding(ELevelChunkKeywords chunk);
	bool HandleChunkKeyword(char *line);

	bool HandleGraphicsKeyword( char *line);
	bool HandleEnemyEntityKeyword( char *line);
	bool HandleBulletKeyword( char *line);
	bool HandleExplosionKeyword( char *line);
	bool HandleRouteChunkKeyword( char *line);

	void CleanGraphicEntityChunk();
	void CleanEnemyEntityChunk();
	void CleanBulletEntityChunk();
	void CleanExplosionEntityChunk();
	void CleanRouteChunk();
public:
	CLevelLoader(void);
	~CLevelLoader(void);

	bool LoadLevel(const char* filename);
};
