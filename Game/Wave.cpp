#include "Common.h"
#include "Tick.h"
#include "wave.h"
#include "Ai.h"
#include "WorldModel.h"
#include "WorldEntityEnemy.h"

#include "String.h"

CWave::CWave(void): m_waveData(0),m_numEntitiesSpawned(0),m_tickOfLastSpawning(0),m_entitiesInWave(0)
{
}

CWave::~CWave(void)
{
	SAFE_FREE(m_waveData->name);
	SAFE_DELETE_ARRAY(m_waveData->enemyTemplateIds);
	SAFE_DELETE_ARRAY(m_waveData->wayPoints);
	SAFE_DELETE(m_waveData);
	SAFE_DELETE_ARRAY(m_entitiesInWave);
}

void CWave::Initialise(TWaveData *waveData)
{
	assert(waveData);
	assert(waveData->name);
	assert(waveData->enemyTemplateIds);
	assert(waveData->wayPoints);

	if (m_waveData)
	{
		SAFE_FREE(m_waveData->name);
		SAFE_DELETE_ARRAY(m_waveData->enemyTemplateIds);
		SAFE_DELETE_ARRAY(m_waveData->wayPoints);
		SAFE_DELETE(m_waveData);
	}

	// Just assign pointers
	m_waveData=waveData;

	// To remember the entities in this wave
	SAFE_DELETE_ARRAY(m_entitiesInWave);

	m_entitiesInWave=new CWorldEntityEnemy*[m_waveData->numEnemies];
	memset(m_entitiesInWave,0,m_waveData->numEnemies*sizeof(CWorldEntityEnemy*));
}

// Start the wave
void CWave::Start()
{
	m_numEntitiesSpawned=0;
	memset(m_entitiesInWave,0,m_waveData->numEnemies*sizeof(CWorldEntityEnemy*));
	SpawnEnemy();
}

// returns true when finished spawning
bool CWave::Update()
{
	if (m_numEntitiesSpawned>=m_waveData->numEnemies)
		return true; // finished spawning

	if (WORLD.GetTick()->GetTick()-m_tickOfLastSpawning>m_waveData->entryRate)
		SpawnEnemy();

	return false;
}

void CWave::SpawnEnemy()
{
	// Why this upward ref?
	CWorldEntityEnemy *ent=WORLD.GetAI()->SpawnEnemy(m_waveData->enemyTemplateIds[m_numEntitiesSpawned]);
	if (!ent)
		return; // no room

	// remember pointer
	m_entitiesInWave[m_numEntitiesSpawned]=ent;

	ent->TravelWayPoints(m_waveData->wayPoints,m_waveData->numWayPoints);
	ent->SetActive(true);

	m_tickOfLastSpawning=WORLD.GetTick()->GetTick();
	m_numEntitiesSpawned++;
}

// Determine if all enemies in the wave are dead
bool CWave::IsWaveFinished()
{
	if (m_numEntitiesSpawned<m_waveData->numEnemies)
		return false;

	for (int i=0;i<m_numEntitiesSpawned;i++)
	{
		if (m_entitiesInWave[i]->GetActive())
			return false;
	}

	return true;
}

// Kill all remaining enemies
void CWave::Kill()
{
	for (int i=0;i<m_numEntitiesSpawned;i++)
	{
		if (m_entitiesInWave[i] && m_entitiesInWave[i]->GetActive())
		{
			m_entitiesInWave[i]->TakeDamage(10000); // tbd? Bit smelly.
		}
	}
}
