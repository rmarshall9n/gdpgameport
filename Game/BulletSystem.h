#pragma once

#include "WorldEntity.h"
#include "WorldEntityBullet.h"
#include "LevelChunkData.h"

// Everything to do with bullets!
class CBulletSystem
{
private:
	int m_maxBullets;
	int m_bulletEntityStartIndex;

	std::vector<CWorldEntityBullet*> m_bulletTemplates;
public:

	CBulletSystem(void);
	~CBulletSystem(void);

	CWorldEntityBullet *GetBulletTemplateById(int id) const;

	void Initialise(int bulletEntityStartIndex, int maxBullets);
	void Fire(const CVector3& startPos,const CVector3 &rot,int bulletTemplateId,CWorldEntity::ESide side);

	int GetBulletStartIndex() const {return m_bulletEntityStartIndex;}
	int GetMaxBullets() const {return m_maxBullets;}

	int AddBulletTemplate(const char *name, const char *graphicName, const char *explosionName, const float speed, const int damage, const int multi);
	int AddBulletTemplate(const TBulletEntityChunk& data);

	int GetBulletTemplateIdByName(const char *name);

	void Reset();
};
