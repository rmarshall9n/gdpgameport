#include "Common.h"
#include "levelloader.h"
#include "WorldModel.h"
#include "visualisation.h"
#include "BulletSystem.h"
#include "ExplosionManager.h"
#include "Ai.h"
#include "worldEntityManager.h"
#include "memoryutils.h"

#if !defined(CAANOO)
    #include <direct.h> //ry remove
#endif
/**
* \brief constructor
* \author Keith Ditchburn 22/12/2004
*/
CLevelLoader::CLevelLoader(void) : m_currentChunk(eNoChunk), m_textFile(NULL)
{
	memset(&m_graphicEntityChunk,0,sizeof(TGraphicEntityChunk));
	memset(&m_enemyEntityChunk,0,sizeof(TEnemyEntityChunk));
	memset(&m_bulletEntityChunk,0,sizeof(TBulletEntityChunk));
	memset(&m_explosionEntityChunk,0,sizeof(TExplosionEntityChunk));
	memset(&m_routeChunk,0,sizeof(TRouteChunk));
}

/**
* \brief destructor
* \author Keith Ditchburn 22/12/2004
*/
CLevelLoader::~CLevelLoader(void)
{
	delete m_textFile;

	CleanGraphicEntityChunk();
	CleanEnemyEntityChunk();
	CleanBulletEntityChunk();
	CleanExplosionEntityChunk();
	CleanRouteChunk();
}

void CLevelLoader::CleanExplosionEntityChunk()
{
	SAFE_FREE(m_explosionEntityChunk.name);
	SAFE_FREE(m_explosionEntityChunk.graphicName);

	memset(&m_explosionEntityChunk,0,sizeof(TExplosionEntityChunk));
}

void CLevelLoader::CleanBulletEntityChunk()
{
	SAFE_FREE(m_bulletEntityChunk.name);
	SAFE_FREE(m_bulletEntityChunk.graphicName);
	SAFE_FREE(m_bulletEntityChunk.explosionName);

	memset(&m_bulletEntityChunk,0,sizeof(TBulletEntityChunk));

	m_bulletEntityChunk.speed=1;
	m_bulletEntityChunk.damage=1;
}

/**
* \brief free memory
* \author Keith Ditchburn 22/12/2004
*/
void CLevelLoader::CleanGraphicEntityChunk()
{
	SAFE_DELETE(m_graphicEntityChunk.filename);
	SAFE_FREE(m_graphicEntityChunk.name);

	memset(&m_graphicEntityChunk,0,sizeof(TGraphicEntityChunk));

	m_graphicEntityChunk.frames=0;
	m_graphicEntityChunk.frameSpeed=1.0f;
	m_graphicEntityChunk.mode=0; // loop
}

/**
* \brief free memory
* \author Keith Ditchburn 22/12/2004
*/
void CLevelLoader::CleanEnemyEntityChunk()
{
	SAFE_FREE(m_enemyEntityChunk.graphicName);
	SAFE_FREE(m_enemyEntityChunk.entityName);
	SAFE_FREE(m_enemyEntityChunk.bulletName);
	SAFE_FREE(m_enemyEntityChunk.explosionName);

	memset(&m_enemyEntityChunk,0,sizeof(TEnemyEntityChunk));

	// Defaults for optional params (that are not 0):
	m_enemyEntityChunk.collisionCost=1;
	m_enemyEntityChunk.health=1;
	m_enemyEntityChunk.moveSpeed=1;
	m_enemyEntityChunk.rotSpeed=0.5f;
	m_enemyEntityChunk.onlyFireForward=true;
	m_enemyEntityChunk.fireRate=400; // default ms
	m_enemyEntityChunk.chanceOfFiring=1; // always
}

void CLevelLoader::CleanRouteChunk()
{
	SAFE_FREE(m_routeChunk.routeName);
	SAFE_DELETE_ARRAY(m_routeChunk.wayPoints);
	memset(&m_routeChunk,0,sizeof(TRouteChunk));
}

/**
* \brief Load the level
* \param filename - the filename of the text file containing the level information
* \return success
* \author Keith Ditchburn 22/12/2004
*/
bool CLevelLoader::LoadLevel(const char* filename)
{
	assert(!m_textFile);
	m_textFile=new Utility::CTextFile(CToolBox::MakePath(filename));

	if (!m_textFile->Open(Utility::CTextFile::eOpenForRead))
	{
		EsOutput(("CLevelLoader: Could not open level file %s\n"),CToolBox::MakePath(filename));
		return false;
	}

	Utility::CFilename fname(filename);

	// Set to path of level file for loading
	// all data is in this directory or relative to it

#if !defined(CAANOO)
	char originalPath[MAX_PATH];
	CToolBox::GetRootDirectory(MAX_PATH,originalPath);

	char dataPath[MAX_PATH];
	sprintf(dataPath,"%s\\%s",originalPath,fname.GetPath());

	// TBD: removed for WINCE

	_chdir(dataPath);
#endif


	EsOutput("CLevelLoader: processing level %s\n",filename);

	// Indicate comments
	m_textFile->AddComment("#");
	m_textFile->AddComment("//");

	// Indicate data seperators
	m_textFile->AddDataSeperator(',');
	m_textFile->AddDataSeperator(' ');

	// Parse the file
	char line[kMaxLineLength];
	while(m_textFile->ReadLine(line,kMaxLineLength))
	{
		// If we are in a chunk keyword may belong to that chunk
		// Otherwise may be a main chunk keyword
		bool handledKeyword=HandleChunkKeyword(line);
		if (!handledKeyword)
		{
			// Look through chunk headers
			for (int i=0;i<eNumLevelChunkKeywords;i++)
			{
				if (m_textFile->ContainsKeyword(line,gChunkKeywords[i]))
				{
					ChunkStarting((ELevelChunkKeywords)i);
					handledKeyword=true;
					break;
				}
			}
		}

		if (!handledKeyword)
		{
			EsOutput(("CLevelLoader: unidentified keyword %s while in chunk %s\n"),
				line,gChunkKeywords[m_currentChunk]);
		}
	}

	// Must end current chunk
	ChunkEnding(m_currentChunk);

	m_textFile->Close();

	EsOutput(("CLevelLoader: finished processing level %s\n"),filename);

	// Set current directory back again
#if !defined(CAANOO)
	_chdir(originalPath);
#endif

	return true;
}

/**
* \brief Beginning of a new chunk
* \author Keith Ditchburn 22/12/2004
*/
void CLevelLoader::ChunkStarting(ELevelChunkKeywords newChunk)
{
	ChunkEnding(m_currentChunk);

	m_currentChunk=newChunk;

	//EsOutput(("CLevelLoader: Starting new chunk %s\n"),gChunkKeywords[m_currentChunk]);

	// Carry out entering chunk actions
	switch(m_currentChunk)
	{
	case eNoChunk:
		break;
	case eGraphicEntity:
		CleanGraphicEntityChunk();
		break;
	case eBulletEntity:
		CleanBulletEntityChunk();
		break;
	case eExplosionEntity:
		CleanExplosionEntityChunk();
		break;
	case eEnemyEntity:
		CleanEnemyEntityChunk();
		break;
	case eRouteData:
		CleanRouteChunk();
		break;
	case eWaveData:
		EsOutput("Wave data disabled - TODO\n");
		break;
	default:
		EsError("CLevelLoader:ChunkStarting:Unknown chunk\n");
		break;
	}
}

/**
* \brief End of a chunk - create / save
* \author Keith Ditchburn 22/12/2004
*/
void CLevelLoader::ChunkEnding(ELevelChunkKeywords chunk)
{
//ry	EsOutput(("CLevelLoader: Ending chunk %s\n"),gChunkKeywords[chunk]);

	switch(chunk)
	{
	case eNoChunk:
		break;
	case eGraphicEntity:
		{
			EsOutput(("CLevelLoader: adding graphic entity. Name: %s Filename %s\n"),
				m_graphicEntityChunk.name,m_graphicEntityChunk.filename->GetPathAndFilename());

			// Now use viz to add - will be later referenced by name
			int id=-1;

			char path[80];
            strcpy (path,"Game/Data/");
            strcat (path,m_graphicEntityChunk.filename->GetPathAndFilename());

			if (!VIZ.CreateSprite(path,
				m_graphicEntityChunk.name,&id,false,m_graphicEntityChunk.frames,m_graphicEntityChunk.frameSize,
				m_graphicEntityChunk.frameSpeed,m_graphicEntityChunk.mode))
			//int id=VIZ.CreateSprite(m_graphicEntityChunk.filename->GetPathAndFilename(),
			//	m_graphicEntityChunk.name,false,m_graphicEntityChunk.frames,m_graphicEntityChunk.frameSize);
				EsOutput(("CLevelLoader: Problem loading graphic resource\n"));
		}
		break;
	case eBulletEntity:
		{
			EsOutput(("CLevelLoader: adding bullet entity. Name: %s\n"),
				m_bulletEntityChunk.name);

			// Now add to bullet system
			int id=WORLD.GetBulletSystem()->AddBulletTemplate(m_bulletEntityChunk);
			if (id==-1)
				EsOutput(("CLevelLoader: Problem loading bullet type\n"));
		}
		break;
	case eExplosionEntity:
		{
			EsOutput(("CLevelLoader: adding explosion entity. Name: %s\n"),
				m_explosionEntityChunk.name);

			// Now add to explosion system
			int id=WORLD.GetExplosionManager()->AddExplosionTemplate(m_explosionEntityChunk);
			if (id==-1)
				EsOutput(("CLevelLoader: Problem loading explosion type\n"));
		}
		break;
	case eEnemyEntity:
		{
			EsOutput(("CLevelLoader: adding enemy entity. Name: %s\n"),
				m_enemyEntityChunk.entityName);

			// Now add to AI
			int id=WORLD.GetAI()->AddEnemyTemplate(m_enemyEntityChunk);
			if (id==-1)
				EsOutput(("CLevelLoader: Problem loading enemy type\n"));
		}
		break;
	case eRouteData:
		{
			EsOutput(("CLevelLoader: adding route. Name: %s\n"),
				m_routeChunk.routeName);

			// Now add to AI
			int id=WORLD.GetAI()->AddRoute(m_routeChunk);
			if (id==-1)
				EsOutput(("CLevelLoader: Problem loading route\n"));
		}
		break;
	case eWaveData:
		{
			EsOutput(("Wave data disabled : TODO\n"));
		}
		break;
	default:
		EsError(("CLevelLoader:ChunkEnding:Unknown chunk\n"));
		break;
	}

	EsOutput(("\n"));
}

/**
* \brief a new keyword has been found - process it if in a chunk
* \return true if handled else false
* \author Keith Ditchburn 22/12/2004
*/
bool CLevelLoader::HandleChunkKeyword(char *line)
{
	assert(line);

	switch(m_currentChunk)
	{
	case eNoChunk:
		// Nothing to do
		return false;
		break;
	case eGraphicEntity:
		return HandleGraphicsKeyword(line);
		break;
	case eBulletEntity:
		return HandleBulletKeyword(line);
		break;
	case eExplosionEntity:
		return HandleExplosionKeyword(line);
		break;
	case eEnemyEntity:
		return HandleEnemyEntityKeyword(line);
		break;
	case eRouteData:
		return HandleRouteChunkKeyword(line);
		break;
	case eWaveData:
		EsOutput(("Wave data disabled"));
		break;
	default:
		// This is definately an error
		EsError(("Unknown chunk\n"));
		break;
	}

	// Not a keyword we know anything about
	return false;
}


/**
* \brief whilst in the chunk a keyword found
* \return true if handled else false
* \author Keith Ditchburn 22/12/2004
*/
bool CLevelLoader::HandleGraphicsKeyword( char *line)
{
	assert(line);

	char buf[kMaxLineLength];

	for (int i=0;i<eNumGraphicEntity;i++)
	{
		// Look for keyword in line - if found result points to start of data
		char *dataPointer=m_textFile->ExtractKeywordValue(line,gGraphicsKeywords[i]);
		if (dataPointer)
		{

			//EsOutput(("CLevelLoader: graphics data found=%s under keyword %s\n"),dataPointer,gGraphicsKeywords[i]);

			switch((EGraphicEntityChunk)i)
			{
			case eGraphicEntityFilename:
				if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
					EsError(("CLevelLoader: could not load string\n"));
				m_graphicEntityChunk.filename=new Utility::CFilename(buf);
				break;
			case eGraphicEntityName:
				if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
					EsError(("CLevelLoader: could not load string\n"));
				m_graphicEntityChunk.name=Utility::CMemory::DuplicateString(buf);
				break;
			case eGraphicEntityFrames:
				if (!m_textFile->LoadInt(&m_graphicEntityChunk.frames,dataPointer))
					EsError(("CLevelLoader: could not load int\n"));
				break;
			case eGraphicEntityFrameSize:
				if (!m_textFile->LoadInt(&m_graphicEntityChunk.frameSize,dataPointer))
					EsError(("CLevelLoader: could not load int\n"));
				break;
			case eGraphicEntityFrameSpeed:
				if (!m_textFile->LoadFloat(&m_graphicEntityChunk.frameSpeed,dataPointer))
					EsError(("CLevelLoader: could not load float\n"));
				break;
			case eGraphicEntityFrameMode:
				if (!m_textFile->LoadInt(&m_graphicEntityChunk.mode,dataPointer))
					EsError(("CLevelLoader: could not load int\n"));
				break;
			default:
				EsError(("CLevelLoader: unhandled graphic keyword\n"));
				break;
			}
			return true;
		}
	}

	return false;
}

/**
* \brief whilst in the chunk a keyword found
* \return true if handled else false
* \author Keith Ditchburn 22/12/2004
*/
bool CLevelLoader::HandleRouteChunkKeyword( char *line)
{
	assert(line);

	char buf[kMaxLineLength];

	for (int i=0;i<eNumRoute;i++)
	{
		// Look for keyword in line - if found result points to start of data
		char *dataPointer=m_textFile->ExtractKeywordValue(line,gRouteKeywords[i]);
		if (dataPointer)
		{
			switch((ERouteChunk)i)
			{
			case eRouteChunkName:
				if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
					EsError(("CLevelLoader: could not load string\n"));
				assert(!m_routeChunk.routeName);
				m_routeChunk.routeName=Utility::CMemory::DuplicateString(buf);
			break;
			case eRouteNumWayPoints:
				if (!m_textFile->LoadInt(&m_routeChunk.numWayPoints,dataPointer))
					EsError(("CLevelLoader: could not load int\n"));
				assert(!m_routeChunk.wayPoints);
				m_routeChunk.wayPoints = new CVector3[m_routeChunk.numWayPoints];
				break;
			case eRouteWayPoint:
				assert(m_routeChunk.currentPoint<m_routeChunk.numWayPoints);
//				if (!m_textFile->LoadVector(&m_routeChunk.wayPoints[m_routeChunk.currentPoint],dataPointer))
				if (!m_textFile->LoadThreeFloats(&m_routeChunk.wayPoints[m_routeChunk.currentPoint].x,
					&m_routeChunk.wayPoints[m_routeChunk.currentPoint].y,
					&m_routeChunk.wayPoints[m_routeChunk.currentPoint].z,dataPointer))
					EsError(("CLevelLoader: could not load vector\n"));
				m_routeChunk.currentPoint++;
				break;
			default:
				EsError(("CLevelLoader: unhandled route keyword\n"));
				break;
			}
			return true;
		}
	}

	return false;
}

bool CLevelLoader::HandleBulletKeyword( char *line)
{
	assert(line);

	char buf[kMaxLineLength];

	for (int i=0;i<eNumBulletEntity;i++)
	{
		// Look for keyword in line - if found result points to start of data
		char *dataPointer=m_textFile->ExtractKeywordValue(line,gBulletKeywords[i]);
		if (dataPointer)
		{
			//EsOutput(("CLevelLoader: bullet data found=%s under keyword %s\n"),dataPointer,gBulletKeywords[i]);

			switch((EBulletEntityChunk)i)
			{
			case eBulletName:
				if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
					EsError(("CLevelLoader: could not load string\n"));
				m_bulletEntityChunk.name=Utility::CMemory::DuplicateString(buf);
				break;
			case eBulletGraphicName:
				if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
					EsError(("CLevelLoader: could not load string\n"));
				m_bulletEntityChunk.graphicName=Utility::CMemory::DuplicateString(buf);
				break;
			case eBulletExplosionName:
				if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
					EsError(("CLevelLoader: could not load string\n"));
				m_bulletEntityChunk.explosionName=Utility::CMemory::DuplicateString(buf);
				break;
			case eBulletSpeed:
				if (!m_textFile->LoadFloat(&m_bulletEntityChunk.speed,dataPointer))
					EsError(("CLevelLoader: could not load float\n"));
				break;
			case eBulletDamage:
				if (!m_textFile->LoadInt(&m_bulletEntityChunk.damage,dataPointer))
					EsError(("CLevelLoader: could not load int\n"));
				break;
			case eBulletMulti:
				if (!m_textFile->LoadInt(&m_bulletEntityChunk.multi,dataPointer))
					EsError(("CLevelLoader: could not load int\n"));
				break;
			default:
				EsError(("CLevelLoader: unhandled bullet keyword\n"));
				break;
			}
			return true;
		}
	}

	return false;
}

bool CLevelLoader::HandleExplosionKeyword( char *line)
{
	assert(line);

	char buf[kMaxLineLength];

	for (int i=0;i<eNumExplosionEntity;i++)
	{
		// Look for keyword in line - if found result points to start of data
		char *dataPointer=m_textFile->ExtractKeywordValue(line,gExplosionKeywords[i]);
		if (dataPointer)
		{
			//EsOutput(("CLevelLoader: explosion data found=%s under keyword %s\n"),dataPointer,gExplosionKeywords[i]);

			switch((EExplosionEntityChunk)i)
			{
			case eExplosionName:
				if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
					EsError(("CLevelLoader: could not load string\n"));
				m_explosionEntityChunk.name=Utility::CMemory::DuplicateString(buf);
				break;
			case eExplosionGraphicName:
				if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
					EsError(("CLevelLoader: could not load string\n"));
				m_explosionEntityChunk.graphicName=Utility::CMemory::DuplicateString(buf);
				break;
			default:
				EsError(("CLevelLoader: unhandled bullet keyword\n"));
				break;
			}
			return true;
		}
	}

	return false;
}

/**
* \brief whilst in the chunk a keyword found
* \return true if handled else false
* \author Keith Ditchburn 22/12/2004
*/
bool CLevelLoader::HandleEnemyEntityKeyword( char *line)
{
	assert(line);

	char buf[kMaxLineLength];

	for (int i=0;i<eNumEnemyEntity;i++)
	{
		// Look for keyword in line - if found result points to start of data
		char *dataPointer=m_textFile->ExtractKeywordValue(line,gEnemyEntityKeywords[i]);
		if (dataPointer)
		{
			//EsOutput(("CLevelLoader: enemy entity data found=%s under keyword %s\n"),
			//	dataPointer,gEnemyEntityKeywords[i]);
			switch((EEnemyEntityChunk)i)
			{
				case eEnemyEntityName:
					if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
						EsError(("CLevelLoader: could not load string\n"));
					m_enemyEntityChunk.entityName=Utility::CMemory::DuplicateString(buf);
					break;
				case eEnemyEntityGraphicName:
					if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
						EsError(("CLevelLoader: could not load string\n"));
					m_enemyEntityChunk.graphicName=Utility::CMemory::DuplicateString(buf);
					break;
				case eEnemyEntityBulletName:
					if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
						EsError(("CLevelLoader: could not load string\n"));
					m_enemyEntityChunk.bulletName=Utility::CMemory::DuplicateString(buf);
					break;
				case eEnemyEntityExplosionName:
					if (!m_textFile->LoadAString(buf,kMaxLineLength,dataPointer))
						EsError(("CLevelLoader: could not load string\n"));
					m_enemyEntityChunk.explosionName=Utility::CMemory::DuplicateString(buf);
					break;
				case eEnemyEntityCollisionCost:
					if (!m_textFile->LoadInt(&m_enemyEntityChunk.collisionCost,dataPointer))
						EsError(("CLevelLoader: could not load int\n"));
					break;

				case eEnemyEntityHealth:
					if (!m_textFile->LoadInt(&m_enemyEntityChunk.health,dataPointer))
						EsError(("CLevelLoader: could not load int\n"));
					break;

				case eEnemyEntitySpeed:
					if (!m_textFile->LoadFloat(&m_enemyEntityChunk.moveSpeed,dataPointer))
						EsError(("CLevelLoader: could not load float\n"));
					break;
				case eEnemyEntityRotationSpeed:
					if (!m_textFile->LoadFloat(&m_enemyEntityChunk.rotSpeed,dataPointer))
						EsError(("CLevelLoader: could not load float\n"));
					break;

				case eEnemyEntityOnlyFireForward:
					if (!m_textFile->LoadInt(&m_enemyEntityChunk.onlyFireForward,dataPointer))
						EsError(("CLevelLoader: could not load float\n"));
					break;

				case eEnemyEntityFireRate:
					if (!m_textFile->LoadInt(&m_enemyEntityChunk.fireRate,dataPointer))
						EsError(("CLevelLoader: could not load int\n"));
					break;
				case eEnemyEntityChanceOfFiring:
					if (!m_textFile->LoadInt(&m_enemyEntityChunk.chanceOfFiring,dataPointer))
						EsError(("CLevelLoader: could not load int\n"));
					break;
				case eEnemyEntityChunkScore:
					if (!m_textFile->LoadInt(&m_enemyEntityChunk.score,dataPointer))
						EsError(("CLevelLoader: could not load int\n"));
					break;
				default:
					EsError(("CLevelLoader: unhandled world entity keyword\n"));
					break;
			}
			return true;
		}
	}

	return false;
}



