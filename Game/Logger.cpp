#include "Logger.h"

CLogger::CLogger()
{
    m_file = fopen("logFile.txt", "w");
}

CLogger::~CLogger()
{
    fclose(m_file);
}

CLogger& CLogger::GetInstance()
{
    if(!m_instance)
        m_instance = new CLogger();

    return *m_instance;
}

void CLogger::Destroy()
{
    delete m_instance;
}

void CLogger::LogError(char* error)
{
    fprintf(m_file, "ERROR: %s\n", error);
}

void CLogger::LogMessage(char* error)
{
    fprintf(m_file, "> %s\n", error);
}
