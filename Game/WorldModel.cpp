//**************************************************************************************************
// WorldModel.cpp: implementation of the CWorldModel class.
// Description: Provides the interfaces to the world model component
// Created by: Keith Ditchburn 12/03/2003
//**************************************************************************************************
#include <stdio.h>

#include "Common.h"
#include "Tick.h"
#include "WorldModel.h"
#include "WorldEntityScenery.h"
#include "WorldEntityEnemy.h"
#include "WorldEntityPlayer.h"
#include "WorldEntityBullet.h"
#include "WorldEntityBackground.h"
#include "BulletSystem.h"
#include "ExplosionManager.h"
#include "PowerUpManager.h"
#include "Map.h"
#include "Visualisation.h"
#include "Ai.h"
#include "level.h"
#include "memoryutils.h"

#include "Toolbox.h"

#include "LevelLoader.h"
#include "Wave.h"

#include "Ui.h"
#include "UiScreen.h"

#include "Logger.h"

const float kWorldScale=2.0f;

// Limits for tweaking
#if defined(WINCE)
const int kMaxEntities=128;
const int kMaxBullets=64;
const int kMaxEnemies=24;
const int kMaxExplosions=24;
const int kMaxPowerUps=4;
const int kFirePowerMax=100;
#else
const int kMaxEntities=512;
const int kMaxBullets=256;
const int kMaxEnemies=64;
const int kMaxExplosions=64;
const int kMaxPowerUps=4;
const int kFirePowerMax=100;
#endif

// These should ultimately be in level data or ini file:

#if defined(WINCE)
const float kPlayerSpeed=4.0f;		// movement speed per press
#else
const float kPlayerSpeed=2.5f;		// movement speed per press
#endif
//const int kPlayerMaxHealth=200;		// max health of player


CWorldModel* CWorldModel::m_instance=0;

// Singleton functions
void CWorldModel::CreateInstance()
{
	if (m_instance!=0)
	{
//ry		EsError("Trying to create a second instance of world!!");
		return;
	}

	m_instance=new CWorldModel();
}


void CWorldModel::DestroyInstance()
{
	if (m_instance==0)
	{
//ry		EsError("Instance already destroyed!!");
		return;
	}

	delete m_instance;
	m_instance=0;
}

/**************************************************************************************************
**************************************************************************************************/
CWorldModel::CWorldModel() : m_worldEntityManager(0),m_map(0),m_bulletSystem(0),m_explosionManager(0),
                             m_powerUpManager(0),m_ai(0), m_tick(0), m_isExiting(false)

{
    m_joystick = NULL;

	m_playerGraphic=-1;
	m_healthBarGraphic=-1;
	m_healthBarBorderGraphic=-1;
	m_healthPowerUpGraphic=-1;
	m_numbersGraphic=-1;
	m_fireBarGraphic=-1;
	m_hapiLogoGraphic=-1;

	m_backgroundGraphic=-1;

	m_hudScreenId=-1;
	m_hudHealthBarId=-1;
	m_hudHealthBarBorderId=-1;
	m_hudFireBarId=-1;

	m_startScreenId=-1;

	m_firePower=kFirePowerMax;

	m_laserSound = 0;
	m_explosionSound=0;
	m_hitSound=0;
	m_collectSound=0;
	m_clickSound=0;

	m_gameMode=eGameModeFrontScreen;

#ifdef _DEBUG
	m_showFps=true;
	m_invincible=false;
//ry	HAPI->SetShowFPS(true,0,0);
#else
	m_showFps=false;
	m_invincible=false;
//ry    HAPI->SetShowFPS(false);
#endif
    m_previousGameTime = SDL_GetTicks();

	m_playerTickMove.x=0;
	m_playerTickMove.y=0;
	m_playerTickMove.z=0;
	m_playerFire=false;

	// TBD: take the length of a tick from data?

	// Note: 20 is time between ticks so 1000/20 is 50 updates a second
#if defined(WINCE)
	m_tick=new CTick(30);
#else
	m_tick=new CTick(20);
#endif
}

/**************************************************************************************************
**************************************************************************************************/
CWorldModel::~CWorldModel()
{
    SDL_JoystickClose(m_joystick);

    Mix_FreeChunk(m_laserSound);
    Mix_FreeChunk(m_explosionSound);
	Mix_FreeChunk(m_hitSound);
	Mix_FreeChunk(m_collectSound);
	Mix_FreeChunk(m_clickSound);

    m_laserSound = 0;
    m_explosionSound = 0;
    m_hitSound = 0;
    m_collectSound = 0;
    m_clickSound = 0;

	delete m_worldEntityManager;
	delete m_map;
	delete m_ai;
	delete m_bulletSystem;
	delete m_explosionManager;
	delete m_powerUpManager;
	delete m_tick;
}

/**************************************************************************************************
Desc: Get the visualisation to load the graphic entities we require
Date: 12/03/2003
**************************************************************************************************/
bool CWorldModel::Initialise(int screenWidth,int screenHeight)
{
	srand(SDL_GetTicks());

    /* Check and open joystick device */
	if (SDL_NumJoysticks() > 0) {
		m_joystick = SDL_JoystickOpen(0);
		if(!m_joystick) {
			fprintf (LOGFile, "Couldn't open joystick 0: %s\n", SDL_GetError ());
		}
	}

	// Create managers etc.
	m_map=new CMap;
	m_map->SetScreenDimensions(screenWidth,screenHeight);

	m_worldEntityManager=new CWorldEntityManager;
	m_bulletSystem=new CBulletSystem;
	m_explosionManager=new CExplosionManager;
	m_powerUpManager=new CPowerUpManager;
	m_ai=new CAi;

	// Create our UI HUD screen
	m_hudScreenId=UI.AddScreen("HUD");
	CUiScreen* sc=UI.GetScreenById(m_hudScreenId);
	if (!sc)
		return false;

	// Create Front Screen
	m_startScreenId=UI.AddScreen("Start");
	sc=UI.GetScreenById(m_startScreenId);
	if (!sc)
		return false;

	// Note: UI elements are added in LoadLevel
	return true;
}

/**************************************************************************************************
Desc: Load level data
Note: Hard coded for time being
Date: 12/03/2003
**************************************************************************************************/
bool CWorldModel::LoadLevel(const char *filename)
{
	assert(filename);
	assert(m_map);
	assert(m_ai);
	assert(m_bulletSystem);
	assert(m_worldEntityManager);
	assert(m_explosionManager);

	fprintf (LOGFile, "Loading Assets\n");

	if (!LoadStockAssets())
		return false;


	assert(m_backgroundGraphic!=-1);

	m_worldEntityManager->Initialise(kMaxEntities,kMaxBullets,kMaxEnemies,kMaxExplosions,kMaxPowerUps,m_backgroundGraphic);

    // Player - centre of arena (ignoring player sprite size)
	CVector3 pos = CVector3(0.5f * m_map->GetScreenWidth(), m_map->GetScreenHeight() - 64.0f, 0.0f);

	m_worldEntityManager->CreatePlayer(m_playerGraphic,pos,0);


	// Set up AI system
	CEntityTypeRange *enemyRange=m_worldEntityManager->GetEnemyRange();
	assert(enemyRange);
	m_ai->Initialise(enemyRange->GetStart(),enemyRange->GetLength());

	// set up the bullet system
	CEntityTypeRange *bulletRange=m_worldEntityManager->GetBulletRange();
	assert(bulletRange);
	m_bulletSystem->Initialise(bulletRange->GetStart(),bulletRange->GetLength());

	// set up an explosion system
	CEntityTypeRange *explosionRange=m_worldEntityManager->GetExplosionRange();
	assert(explosionRange);
	m_explosionManager->Initialise(explosionRange->GetStart(),explosionRange->GetLength());

	// Set up a power up system
	CEntityTypeRange *powerUpRange=m_worldEntityManager->GetPowerUpRange();
	assert (powerUpRange);
	m_powerUpManager->Initialise(powerUpRange->GetStart(),powerUpRange->GetLength(),m_healthPowerUpGraphic);


	// Load Hard Coded Data
    if(!LoadHardCodedData())
    {
        fprintf (LOGFile, "Error Loading Hard Coded Data.\n");
    }

	LoadHardCodedWaveData();
	LoadHardCodedLevelData();

// UI
	CUiScreen* sc=UI.GetScreenById(m_hudScreenId);
	if (!sc)
		return false;

	//// Score text entity
	//CColour scoreCol(255,0,0);
	//m_hudScoreId=sc->AddTextEntity(0,20,"Score=0000",&scoreCol);

	// Health bar
	// Load graphic
	m_hudHealthBarId=sc->AddBarEntity(4,60,true,0,m_worldEntityManager->GetPlayerEntity()->GetMaxHealth(),m_healthBarGraphic);

	// Health bar sourroundings - must do after above as no ordering mech. as yet in UI
	// Note: could not bother storing id as we never do anything with it
	m_hudHealthBarBorderId=sc->AddImageEntity(4,60,m_healthBarBorderGraphic);

	// Fire bar - position set dynamically before render
	m_hudFireBarId=sc->AddBarEntity(0,0,false,0,kFirePowerMax,m_fireBarGraphic);


	// Start Screen
	sc=UI.GetScreenById(m_startScreenId);
	if (!sc)
		return false;

	sc->AddImageEntity(186,100,m_hapiLogoGraphic);

	ChangeGameMode(eGameModeFrontScreen);

	return true;
}

bool CWorldModel::LoadStockAssets()
{
	if (!VIZ.CreateSprite("Game/Data/Ui/fontNumbers.tga","Number Font",&m_numbersGraphic,false,10,16))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}

	// Player
	if (!VIZ.CreateColourKeyedSprite("Game/Data/playerSprite.tga","player",&m_playerGraphic,false))
	{
//ry		UserMessage("Could not load sprite texture: playerSprite.tga","Error : File Not Found");
		return false;
	}

	// Background
	if (!VIZ.CreateColourKeyedSprite("Game/Data/spaceBackground.tga","background",&m_backgroundGraphic,false))
	{
//ry		UserMessage("Could not load sprite texture: spaceBackground.dds","Error : File Not Found");
		return false;
	}

	// Health bar
	if (!VIZ.CreateSprite("Game/Data/Ui/healthBar.tga","healthBar",&m_healthBarGraphic,false))
	{
//ry		UserMessage("Could not load sprite texture: UI/healthBar.tga","Error : File Not Found");
		return false;
	}

	// Health bar border
	if (!VIZ.CreateSprite("Game/Data/Ui/healthBarBorder.tga","healthBarBorder",&m_healthBarBorderGraphic,false))
	{
//ry		UserMessage("Could not load sprite texture: UI/healthBarBorder.tga","Error : File Not Found");
		return false;
	}

	// Health power up
	if (!VIZ.CreateColourKeyedSprite("Game/Data/health.tga","health",&m_healthPowerUpGraphic,false))
	{
//ry		UserMessage("Could not load sprite texture: UI/health.tga","Error : File Not Found");
		return false;
	}

	// Fire power bar
	if (!VIZ.CreateSprite("Game/Data/Ui/fireBar.tga","fireBar",&m_fireBarGraphic,false))
	{
//ry		UserMessage("Could not load sprite texture: UI/fireBar.tga","Error : File Not Found");
		return false;
	}


	// HAPI logo
	if (!VIZ.CreateSprite("Game/Data/Ui/HAPI Logo.tga","fireBar",&m_hapiLogoGraphic,false))
	{
//ry		UserMessage("Could not load sprite texture: UI/HAPI Logo.tga","Error : File Not Found");
		return false;
	}

	// Sounds
    m_explosionSound = Mix_LoadWAV("Game/Data/Sounds/explode2.wav");
	if (m_explosionSound == NULL)
	{
		UserMessage("Could not load sound Sounds//explode.wav","Error : File Not Found");
		return false;
	}

    m_laserSound = Mix_LoadWAV("Game/Data/Sounds/laser2.wav");
	if (m_laserSound == NULL)
	{
		UserMessage("Could not load sound Sounds//laser2.wav","Error : File Not Found");
		return false;
	}
    m_hitSound = Mix_LoadWAV("Game/Data/Sounds/hit.wav");
	if (m_hitSound == NULL)
	{
		UserMessage("Could not load sound Sounds//hit.wav","Error : File Not Found");
		return false;
	}
    m_collectSound = Mix_LoadWAV("Game/Data/Sounds/collect.wav");
	if (m_collectSound == NULL)
	{
		UserMessage("Could not load sound Sounds//collect.wav","Error : File Not Found");
		return false;
	}

    m_clickSound = Mix_LoadWAV("Game/Data/Sounds/click.wav");
	if (m_clickSound == NULL)
	{
		UserMessage("Could not load sound Sounds//click.wav","Error : File Not Found");
		return false;
	}

	return true;
}

/**************************************************************************************************
Desc:
Date: 12/03/2003
**************************************************************************************************/
void CWorldModel::Render()
{
	// Work out frame rate to determine if should interpolate
	m_tick->StartRender();

	float dt=m_tick->GetDt();

	if (eGameModeFrontScreen==m_gameMode)
	{
		// Clear
		VIZ.ClearScreenToColour(0,0,0);

		CUiScreen *start=UI.GetScreenById(m_startScreenId);
		assert(start);

		UI.Render(dt);

		// 180,10
		char buf[2048];

        int y=20;
		int xoffset = 0;

#ifdef CAANOO
		sprintf(buf,"HAPI Game Demo");
		VIZ.RenderText(xoffset + 50,y,buf,0,255,0);

		y+=20;
		sprintf(buf,"Written by Keith Ditchburn 2005");
		VIZ.RenderText(xoffset + 20,y,buf,100,255,0);

		y+=40;
		sprintf(buf,"Joystick - Move Craft");
		VIZ.RenderText(xoffset + 35,y,buf,255,255,255);

		y+=20;
		sprintf(buf,"X - Fire");
		VIZ.RenderText(xoffset + 72,y,buf,255,255,255);

		y+=20;
		sprintf(buf,"Help 1 - Toggle Pause");
		VIZ.RenderText(xoffset + 42,y,buf,255,255,255);

		y+=20;
		sprintf(buf,"Left Bumper - Toggle FPS");
		VIZ.RenderText(xoffset + 10,y,buf,255,255,255);

		static DWORD startTime=0;
		static bool flash=true;

		if (SDL_GetTicks()-startTime>500)
		{
			flash=!flash;
			startTime=SDL_GetTicks();
		}

		if (flash)
		{
			y+=60;

			sprintf(buf,"Press X to Play");
			VIZ.RenderText(xoffset + 40,y,buf,0,255,0);
		}
#else
        sprintf(buf,"HAPI Game Demo");
		VIZ.RenderText(xoffset + 50,y,buf,0,255,0);

		y+=20;
		sprintf(buf,"Written by Keith Ditchburn 2005");
		VIZ.RenderText(xoffset + 20,y,buf,100,255,0);

		y+=40;
		sprintf(buf,"Arrow Keys - Move Craft");
		VIZ.RenderText(xoffset + 35,y,buf,255,255,255);

		y+=20;
		sprintf(buf,"Return - Fire");
		VIZ.RenderText(xoffset + 60,y,buf,255,255,255);

		y+=20;
		sprintf(buf,"F1 - Toggle Pause");
		VIZ.RenderText(xoffset + 50,y,buf,255,255,255);

		y+=20;
		sprintf(buf,"F2 - Toggle FPS");
		VIZ.RenderText(xoffset + 50,y,buf,255,255,255);

		static DWORD startTime=0;
		static bool flash=true;

		if (SDL_GetTicks()-startTime>500)
		{
			flash=!flash;
			startTime=SDL_GetTicks();
		}

		if (flash)
		{
			y+=60;

			sprintf(buf,"Press SPACE to Play");
			VIZ.RenderText(xoffset + 40,y,buf,0,255,0);
		}
#endif

		return;
	}

	if (eGameModeGameOver==m_gameMode)
	{
		static DWORD startTime=0;
		static bool flash=true;

		char buf[2048];
		int y=200;

		sprintf(buf,"GAME OVER");
		VIZ.RenderText(220,y,buf,0,255,0);

		if (SDL_GetTicks()-startTime>500)
		{
			flash=!flash;
			startTime=SDL_GetTicks();
		}

		if (flash)
		{
			y+=60;

			sprintf(buf,"Press START to Continue");
			VIZ.RenderText(180,y,buf,0,255,0);
		}

		// OK to fall through here
	}

	if (m_ai->HasLevelEnded())
	{
		// If level ended then all enemies are in the process of dying
		// ai waits a bit before advancing to next level. During this
		// time display a message and increase score for a bonus

		static DWORD startTime=0;
		static DWORD bonusTime=0;
		static bool flash=true;

		if (SDL_GetTicks()-startTime>500)
		{
			flash=!flash;
			startTime=SDL_GetTicks();
		}

		if (flash)
		{
			char buf[2048];
			int y=200;

			sprintf(buf,"LEVEL %d COMPLETE",m_ai->GetCurrentLevel()+1);
			VIZ.RenderText(205,y,buf,0,255,0);
		}

		// Increase score for end of level bonus
		if (SDL_GetTicks()-bonusTime>180)
		{
			m_worldEntityManager->GetPlayerEntity()->IncreaseScore(5);

			int h=m_worldEntityManager->GetPlayerEntity()->GetHealth();
			m_worldEntityManager->GetPlayerEntity()->SetHealth(h+1);

			bonusTime=SDL_GetTicks();
		}

	}

	// Render world
	std::vector<CWorldEntity*> entityVector=m_worldEntityManager->GetEntityVector();
	for (size_t i=0;i<entityVector.size();i++)
	{
		if (entityVector[i]->GetActive())
			entityVector[i]->Render(dt);
	}

	// Update HUD
	CUiScreen *hud=UI.GetScreenById(m_hudScreenId);
	assert(hud);
	hud->SetBarValue(m_hudHealthBarId,m_worldEntityManager->GetPlayerEntity()->GetHealth());

	// Fire power bar
	if (m_firePower<0)
		hud->SetBarValue(m_hudFireBarId,0);
	else
		hud->SetBarValue(m_hudFireBarId,m_firePower);

	CVector3 pos=m_worldEntityManager->GetPlayerEntity()->GetCentrePosition();
	pos.y+=m_worldEntityManager->GetPlayerEntity()->GetHeight()/2;
	pos.x-=m_worldEntityManager->GetPlayerEntity()->GetWidth()/2;

	hud->ChangePosition(m_hudFireBarId,(int)pos.x,(int)pos.y);

	// Render HUD
	UI.Render(dt);

    if(m_showFps)
    {
        char time[60];
        sprintf(time, "FPS: %d", m_tick->GetFramesPerSecond());
        VIZ.RenderText(20, 10, time, 255, 0, 255);

         m_previousGameTime = SDL_GetTicks();
    }

	// Numbers using font
    char buff[2048];
    sprintf(buff, "Score: %d", m_worldEntityManager->GetPlayerEntity()->GetScore());
    VIZ.RenderText(130,10,buff,100,255,0);

    buff[2048];
    sprintf(buff, "Level: %d", m_ai->GetCurrentLevel()+1);
    VIZ.RenderText(250,10,buff,100,255,0);


	if (m_tick->IsPaused())
	{
		char buf[2048];
		sprintf(buf,"PAUSED");
		VIZ.RenderText(135,100,buf,100,255,0);
	}

	if (m_invincible)
	{
		char buf[2048];
		sprintf(buf,"Invincible");
		VIZ.RenderText(20,40,buf,100,255,0);
	}

#if defined(EMULATOR)
	// Test:
	char buf[2048];
	_stprintf(buf,_T("FPS:%d TPS:%d"),m_tick->GetFramesPerSecond(),m_tick->GetTicksPerSecond());
	VIZ.RenderText(0,20,buf,255,255,255);
#endif
}

/**************************************************************************************************
Desc: Call all the child update functions and check for collisions
Returns dt the time step through the tick
Date: 15/03/2003
**************************************************************************************************/
void CWorldModel::Update()
{
	// For speed on PPC only update UI every tick (see below)
	//ry UI.Update();  doesnt do anything


	// Handle keyboard constantly
	HandleKeyPresses();


	if (m_tick->IsPaused() || m_isExiting)
		return;

	// See if next tick reached
	if (!m_tick->Update())
		return;

	// Move player in response to accumulated key presses
	// If no y movement - factor in the drift
	if (m_playerTickMove.y==0)
		m_playerTickMove.y+=m_map->GetDriftRate()/2;

	m_worldEntityManager->GetPlayerEntity()->Move(m_playerTickMove);

	m_playerTickMove.x=0;
	m_playerTickMove.y=0;

	if (m_playerFire)
	{
		if (m_firePower>0)
		{
			if (m_worldEntityManager->GetPlayerEntity()->Fire())
			{
			    if(Mix_PlayChannel(-1, m_laserSound, 0) == -1) {
                    fprintf (LOGFile, "Error playing sound: %s\n", Mix_GetError());
			    }


				m_firePower-=3;
				if (m_firePower<-3)
					m_firePower=-3;
			}
		}
		else
		{
			// Empty gun sound - only every 2 seconds
			static unsigned long lastTime=0;
			unsigned long timeNow=SDL_GetTicks();

			if (timeNow-lastTime>2000)
			{
				if(Mix_PlayChannel(-1, m_clickSound, 0) == -1) {
                    fprintf (LOGFile, "Error playing sound: %s\n", Mix_GetError());
			    }
				lastTime=timeNow;
			}
		}

	}
	else
	{
		m_firePower+=1;
		if (m_firePower>kFirePowerMax)
			m_firePower=kFirePowerMax;
	}

	m_playerFire=false;

#if defined(WINCE)
	// For speed on PPC only do AI and check for collisions every second tick
	if ((m_tick->GetTick()+1)%2==0) // offset by one so alternate to UI update
		return;
#endif

	m_ai->Update(m_powerUpManager);


	/* Collision Checks */
	std::vector<CWorldEntity*> entityVector=m_worldEntityManager->GetEntityVector();
	for (size_t i=0;i<entityVector.size();i++)
		entityVector[i]->Update();

	CEntityTypeRange *bulletRange=m_worldEntityManager->GetBulletRange();
	CEntityTypeRange *powerUpRange=m_worldEntityManager->GetPowerUpRange();


	// Check for collisions between bullets and other things
//	int bi=bulletRange->GetStart();
// HACK!
int bi=powerUpRange->GetStart();

// TBD: refactor all this

	bool collided=false;
	while (!collided && bi<bulletRange->GetEnd())
	{
		// Only do with alive bullets of course
		if (entityVector[bi]->CanBeCollidedWith())
		{
			// Test am_ainst all other world entities apart from bullets
			int noneBulletEntities=0;
			while (!collided && noneBulletEntities<(int)entityVector.size())
			{
				// Enable this line to prevent collisions between bullets*
				if (noneBulletEntities<powerUpRange->GetStart() ||
					noneBulletEntities>=bulletRange->GetEnd())
				{
					CWorldEntity *nonBulletEntity=entityVector[noneBulletEntities];

					if (nonBulletEntity->CanBeCollidedWith()
						&& m_worldEntityManager->AreEntitiesEnemies(bi,noneBulletEntities)
						&& HasBulletCollidedWithEntity(bi,noneBulletEntities))
					{
						//DebugOutput("Collision between bullet %d and entity %d\n",bi,y);

						if (nonBulletEntity->TakeDamage(entityVector[bi]->GetCollisionCost()))
						{
							if (noneBulletEntities==m_worldEntityManager->GetPlayerIndex())
							{
								// check for dead
								/*if (!m_worldEntityManager->GetPlayerEntity()->GetActive())
								{
									ChangeGameMode(eGameModeGameOver);
									return;
								}*/
								// done after now
							}
							else
							{
								// Explode
                                if(Mix_PlayChannel(-1, m_explosionSound, 0) == -1) {
                                    fprintf (LOGFile, "Error playing sound: %s\n", Mix_GetError());
                                }

								// Increase score
								int scoreReward=nonBulletEntity->GetScoreReward();

								m_worldEntityManager->GetPlayerEntity()->IncreaseScore(scoreReward);

								// Chance of a health power up, but not if it was the player that dies
								m_powerUpManager->ChanceOfPowerUp(nonBulletEntity->GetLastCentrePosition());
							}
						}
						else
						{
							if (bi>=bulletRange->GetStart())
							{
								// hit
								if(Mix_PlayChannel(-1, m_hitSound, 0) == -1) {
                                    fprintf (LOGFile, "Error playing sound: %s\n", Mix_GetError());
                                }
							}
							else
							{
								// collect
								if(Mix_PlayChannel(-1, m_collectSound, 0) == -1) {
                                    fprintf (LOGFile, "Error playing sound: %s\n", Mix_GetError());
                                }
							}
						}

						// Damage the bullet - causes it to vanish
						// Note: will not be with another bullet unless line above* disabled
						entityVector[bi]->TakeDamage(entityVector[noneBulletEntities]->GetCollisionCost());

						// No need to test this bullet am_ainst anything else
						collided=true;
					}
				}
				noneBulletEntities++;
			}
		}
		bi++;
	}

	// Now test player am_ainst enemies
	CEntityTypeRange *enemyRange=m_worldEntityManager->GetEnemyRange();

	int ee=enemyRange->GetStart();
	collided=false;
	while (!collided && ee<enemyRange->GetEnd())
	{
		if (entityVector[ee]->CanBeCollidedWith())
		{
			if (HasBulletCollidedWithEntity(ee,m_worldEntityManager->GetPlayerIndex()))
			{
				// Cost is fixed at 39?
				// TBD: make variables and tweakable - perhaps load from file
				m_worldEntityManager->GetPlayerEntity()->TakeDamage(40);
				entityVector[ee]->TakeDamage(8);

				collided=true;
			}
		}
		ee++;
	}

	// check for player dead
	if (!m_worldEntityManager->GetPlayerEntity()->GetActive())
	{
		ChangeGameMode(eGameModeGameOver);
		return;
	}
}

bool CWorldModel::HasBulletCollidedWithEntity(int bulletId, int entityId)
{
	// Test first on rectangle collision then on per pixel - get entities to do it themselves
	std::vector<CWorldEntity*> entityVector=m_worldEntityManager->GetEntityVector();

	return entityVector[entityId]->CheckForCollision(entityVector[bulletId]);
}

// CANNOO KEY MAP
/*
0   -   a
1   -   x
2   -   b
3   -   y
4   -   l
5   -   r
6   -   home
7   -   hold
8   -   help1
9   -   help2
10   -  stick press
*/

#if defined(CAANOO) // CAANOO
#define MOVE_LEFT_KEY	7
#define MOVE_RIGHT_KEY	7
#define MOVE_UP_KEY		5
#define MOVE_DOWN_KEY	7
#define FIRE_KEY		1
#define SHOW_FPS_KEY	4
#define INVINCIBLE_KEY	9
#define PAUSE_KEY		8
#define START_KEY		1
#define EXIT_KEY		6

#else // SDL
#define MOVE_LEFT_KEY	SDLK_LEFT
#define MOVE_RIGHT_KEY	SDLK_RIGHT
#define MOVE_UP_KEY		SDLK_UP
#define MOVE_DOWN_KEY	SDLK_DOWN
#define FIRE_KEY		SDLK_RETURN
#define SHOW_FPS_KEY	SDLK_F2
#define INVINCIBLE_KEY	SDLK_F5
#define PAUSE_KEY		SDLK_F1
#define START_KEY		SDLK_SPACE
#define EXIT_KEY		SDLK_ESCAPE
#endif

/* GP2X button mapping */
enum CAANOO_KEY_CODE
{
	VK_UP         , // 0
	VK_UP_LEFT    , // 1
	VK_LEFT       , // 2
	VK_DOWN_LEFT  , // 3
	VK_DOWN       , // 4
	VK_DOWN_RIGHT , // 5
	VK_RIGHT      , // 6
	VK_UP_RIGHT   , // 7
	VK_START      , // 8
	VK_SELECT     , // 9
	VK_FL         , // 10
	VK_FR         , // 11
	VK_FA         , // 12
	VK_FB         , // 13
	VK_FX         , // 14
	VK_FY         , // 15
	VK_VOL_UP     , // 16
	VK_VOL_DOWN   , // 17
	VK_TAT          // 18
};

// sent dinput buffer, check for key presses
void CWorldModel::HandleKeyPresses()
{
#if defined(CAANOO)
    // CAANOO JOYSTICK
    if(SDL_JoystickGetAxis(m_joystick, 0) < -3200) { // X Axis (Left)
        UiInput(eMoveLeft);
    }
    else if(SDL_JoystickGetAxis(m_joystick, 0) > +3200) { // X Axis (Right)
        UiInput(eMoveRight);
    }

    if(SDL_JoystickGetAxis(m_joystick, 1) < -3200) { // Y Axis (Up)
        UiInput(eMoveForward);
    }
    else if(SDL_JoystickGetAxis(m_joystick, 1) > +3200) { // Y Axis (Down)
        UiInput(eMoveBackward);
    }
    if( SDL_JoystickGetButton(m_joystick, FIRE_KEY) ) {
        UiInput(eFire);
    }
#else
    // KEYBOARD
    Uint8* keystate = SDL_GetKeyState(NULL);
    if (!m_tick->IsPaused())
    {
        //continuous-response keys
        if(keystate[MOVE_LEFT_KEY])
        {
            UiInput(eMoveLeft);
        }
        if(keystate[MOVE_RIGHT_KEY])
        {
            UiInput(eMoveRight);
        }
        if(keystate[MOVE_UP_KEY])
        {
            UiInput(eMoveForward);
        }
        if(keystate[MOVE_DOWN_KEY])
        {
            UiInput(eMoveBackward);
        }
        if(keystate[FIRE_KEY])
        {
            UiInput(eFire);
        }
    }
#endif

    SDL_Event event;
    if(SDL_PollEvent(&event))
    {
#if defined CAANOO
        // CAANOO JOYSTICK
        if(event.type == SDL_JOYBUTTONDOWN)
        {
            switch( event.jbutton.button )
            {
                case SHOW_FPS_KEY: UiInput(eShowFps); break;
                case INVINCIBLE_KEY: UiInput(eInvincible); break;
                case PAUSE_KEY: UiInput(ePause); break;
                case START_KEY: UiInput(eContinue); break;
                case EXIT_KEY: m_isExiting = true; break;
                default: ;
            }
        }
#else
        // KEYBOARD
        if(event.type == SDL_KEYUP)
        {
            switch( event.key.keysym.sym )
            {
                case SHOW_FPS_KEY: UiInput(eShowFps); break;
                case INVINCIBLE_KEY: UiInput(eInvincible); break;
                case PAUSE_KEY: UiInput(ePause); break;
                case START_KEY: UiInput(eContinue); break;
                case EXIT_KEY: m_isExiting = true; break;
                default: ;
            }
        }
        if( event.type == SDL_QUIT )
        {
            //Quit the program
            m_isExiting = true;
        }
#endif
    }
}

// respond to user input (via keyboard etc.)
void CWorldModel::UiInput(EUiInput type)
{
	std::vector<CWorldEntity*> entityVector=m_worldEntityManager->GetEntityVector();

	switch(type)
	{
// Note: altered to move addition for PPC - need to check works on PC
	case eMoveLeft:
		//m_worldEntityManager->GetPlayerEntity()->Move(CVector3(-1,0.0f,0.0f));
		m_playerTickMove.x=-kPlayerSpeed;
		break;
	case eMoveRight:
		//m_worldEntityManager->GetPlayerEntity()->Move(CVector3(1,0.0f,0.0f));
		m_playerTickMove.x=kPlayerSpeed;
		break;
	case eMoveForward:
		{
		// Move forward at speed+drift rate?
			//float sp=1.0f;//m_map->GetDriftRate();
		m_playerTickMove.y=-kPlayerSpeed;//+m_map->GetDriftRate();//-m_map->GetDriftRate();
		//m_worldEntityManager->GetPlayerEntity()->Move(CVector3(0.0f,-m_map->GetDriftRate(),0.0f));
		break;
		}
	case eMoveBackward:
		m_playerTickMove.y=kPlayerSpeed;
		//m_worldEntityManager->GetPlayerEntity()->Move(CVector3(0.0f,1,0.0f));
		break;
	case eFire:
		m_playerFire=true;

		//if (m_firePower>0)
		//{
		//	if (m_worldEntityManager->GetPlayerEntity()->Fire())
		//	{
		//		m_soundManager->PlaySound(m_laserSound);
		//		m_firePower-=2;
		//		if (m_firePower<-2)
		//			m_firePower=-2;
		//	}
		//}
		//else
		//{
		//	// Empty gun sound - only every 2 seconds
		//	static unsigned long lastTime=0;
		//	unsigned long timeNow=CToolBox::GetTheTime();

		//	if (timeNow-lastTime>2000)
		//	{
		//		//m_soundManager->StopSound(m_clickSound);
		//		m_soundManager->PlaySound(m_clickSound);
		//		lastTime=timeNow;
		//	}
		//}
		break;
	//case eRotateLeft:
	//	m_worldEntityManager->GetPlayerEntity()->RotateAroundAxisY(-0.01f*dt); // tbd
	//	break;
	//case eRotateRight:
	//	m_worldEntityManager->GetPlayerEntity()->RotateAroundAxisY(+0.01f*dt); // tbd
	//	break;
	case ePause:
		m_tick->Pause(!m_tick->IsPaused());
		break;
	case eAiPause:
		m_ai->TogglePause();
	break;
	case eShowFps:

        m_showFps = !m_showFps;
		//ry
		/*if (m_showFps)
			HAPI->SetShowFPS(true,0,0);
		else
			HAPI->SetShowFPS(false);*/
	break;
	case eInvincible:
		m_invincible=!m_invincible;
		m_worldEntityManager->GetPlayerEntity()->MakeInvincible(m_invincible);
	break;
	case eContinue:
		if (eGameModeFrontScreen==m_gameMode)
		{
			ChangeGameMode(eGameModePlaying);
		}
		else
		if (eGameModeGameOver==m_gameMode)
		{
			ChangeGameMode(eGameModeFrontScreen);
		}
	break;
	default:
		// Error con.
//ry		EsError(_T("Unknown input\n"));
		break;
	}
}

bool CWorldModel::LoadHardCodedData()
{
    int id=-1;

    fprintf (LOGFile, "Loading Hard Coded Data\n");

    // ENEMY GRAPHICS
    if (!VIZ.CreateColourKeyedSprite("Game/Data/redDevil.tga","RED_DEVIL_GRAPHIC",&id,false,8,32))
	{
//ry		UserMessage("Could not load sprite texture: Ui/fontNumbers.tga","Error : File Not Found");
		return false;
	}

	if (!VIZ.CreateColourKeyedSprite("Game/Data/purpleFiend.tga","PURPLE_FIEND_GRAPHIC",&id,false,3,32, 0.1, 1))
	{
//ry		UserMessage("Could not load sprite texture: Ui/fontNumbers.tga","Error : File Not Found");
		return false;
	}

	if (!VIZ.CreateColourKeyedSprite("Game/Data/greenSpinner.tga","GREEN_SPINNER_GRAPHIC",&id,false,8,16))
	{
//ry		UserMessage("Could not load sprite texture: fontNumbers.tga","Error : File Not Found");
		return false;
	}

	if (!VIZ.CreateColourKeyedSprite("Game/Data/boss1.tga","BOSS1_GRAPHIC",&id,false,1,128))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}

	if (!VIZ.CreateColourKeyedSprite("Game/Data/redFlipper.tga","RED_FLIPPER_GRAPHIC",&id,false,6,16,1.0,1))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}

	// BULLET GRAPHICS
	if (!VIZ.CreateColourKeyedSprite("Game/Data/bulletSprite0.tga","BULLET_REDDOUBLE_GRAPHIC",&id,false))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}

	if (!VIZ.CreateColourKeyedSprite("Game/Data/bulletSprite1.tga","BULLET_RED_GRAPHIC",&id,false))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}

	if (!VIZ.CreateColourKeyedSprite("Game/Data/bulletSprite2.tga","BULLET_GREEN_GRAPHIC",&id,false))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}

	if (!VIZ.CreateColourKeyedSprite("Game/Data/bulletSprite3.tga","BULLET_MULTI_GRAPHIC",&id,false))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}

	if (!VIZ.CreateColourKeyedSprite("Game/Data/bomb1.tga","BULLET_ROUNDBOMB_GRAPHIC",&id,false,4,16))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}

	// EXPLOSION GRAPHICS
	if (!VIZ.CreateSprite("Game/Data/explosion1.tga","EXPLOSION_SMALL_GRAPHIC",&id,false,16,32,1.0))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}
	if (!VIZ.CreateSprite("Game/Data/explosion2.tga","EXPLOSION_LARGEVAR1_GRAPHIC",&id,false,16,64))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}
	if (!VIZ.CreateSprite("Game/Data/explosion3.tga","EXPLOSION_LARGEVAR2_GRAPHIC",&id,false,16,64))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}
	if (!VIZ.CreateSprite("Game/Data/explosion4.tga","EXPLOSION_LARGEVAR3_GRAPHIC",&id,false,16,64))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}
	if (!VIZ.CreateSprite("Game/Data/explosion5.tga","EXPLOSION_LARGEVAR4_GRAPHIC",&id,false,16,64))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}
	if (!VIZ.CreateSprite("Game/Data/explosion6.tga","EXPLOSION_LARGEVAR5_GRAPHIC",&id,false,16,64))
	{
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
	}

    fprintf (LOGFile, "Sprites Created.\n");
    // EXPLOSION DEFINITIONS
    if(WORLD.GetExplosionManager()->AddExplosionTemplate("EXPLOSION_SMALL", "EXPLOSION_SMALL_GRAPHIC") == -1) {
        return false;
    }

    if(WORLD.GetExplosionManager()->AddExplosionTemplate("EXPLOSION_LARGE1", "EXPLOSION_LARGEVAR1_GRAPHIC") == -1) {
        return false;
    }

    if(WORLD.GetExplosionManager()->AddExplosionTemplate("EXPLOSION_LARGE2", "EXPLOSION_LARGEVAR2_GRAPHIC") == -1) {
        return false;
    }

     if(WORLD.GetExplosionManager()->AddExplosionTemplate("EXPLOSION_LARGE3", "EXPLOSION_LARGEVAR3_GRAPHIC") == -1) {
        return false;
    }

    if(WORLD.GetExplosionManager()->AddExplosionTemplate("EXPLOSION_LARGE4", "EXPLOSION_LARGEVAR4_GRAPHIC") == -1) {
        return false;
    }

    if(WORLD.GetExplosionManager()->AddExplosionTemplate("EXPLOSION_LARGE5", "EXPLOSION_LARGEVAR5_GRAPHIC") == -1) {
        return false;
    }

    fprintf (LOGFile, "Explosions Created.\n");
	// BULLET DEFINITIONS
    if(WORLD.GetBulletSystem()->AddBulletTemplate("BULLET_PLAYER", "BULLET_REDDOUBLE_GRAPHIC", "EXPLOSION_SMALL", 10.0f, 1, 1) == -1) {
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
    }
    if(WORLD.GetBulletSystem()->AddBulletTemplate("BULLET_RED_MEDIUMSP_SMALLEXP", "BULLET_RED_GRAPHIC", "EXPLOSION_SMALL", 3.0f, 1, 1) == -1) {
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
    }
    if(WORLD.GetBulletSystem()->AddBulletTemplate("BULLET_GREEN_FASTSP_SMALLEXP", "BULLET_GREEN_GRAPHIC", "EXPLOSION_SMALL", 6.0f, 2, 1) == -1) {
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
    }
    if(WORLD.GetBulletSystem()->AddBulletTemplate("BULLET_MULTI", "BULLET_MULTI_GRAPHIC", "EXPLOSION_LARGE1", 3.4f, 2, 1) == -1) {
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
    }
    if(WORLD.GetBulletSystem()->AddBulletTemplate("BULLET_BOMB_SLOWSP_LARGEEXP", "BULLET_ROUNDBOMB_GRAPHIC", "EXPLOSION_LARGE1", 2.0f, 20, 60) == -1) {
//ry		UserMessage("Could not load sprite texture: UI/fontNumbers.tga","Error : File Not Found");
		return false;
    }

    fprintf (LOGFile, "Bullets Created.\n");

    // ENEMY DEFINITIONS
    if(WORLD.GetAI()->AddEnemyTemplate("PURPLE_FIEND1", "PURPLE_FIEND_GRAPHIC", "BULLET_BOMB_SLOWSP_LARGEEXP", "EXPLOSION_LARGE2", 1, 18, 1.0f, 0.08f, 0, 3000, 1, 100 ) == -1) {

        return false;
    }

    if(WORLD.GetAI()->AddEnemyTemplate("RED_DEVIL1", "RED_DEVIL_GRAPHIC", "BULLET_RED_MEDIUMSP_SMALLEXP", "EXPLOSION_LARGE2", 1, 8, 2.5f, 0.08f, 1, 70, 2, 100 ) == -1) {

        return false;
    }

    if(WORLD.GetAI()->AddEnemyTemplate("GREEN_SPINNER1", "GREEN_SPINNER_GRAPHIC", "BULLET_GREEN_FASTSP_SMALLEXP", "EXPLOSION_LARGE3", 1, 2, 3.5f, 0.1f, 0, 80, 2, 50 ) == -1) {

        return false;
    }

    if(WORLD.GetAI()->AddEnemyTemplate("RED_FLIPPER1", "RED_FLIPPER_GRAPHIC", "BULLET_GREEN_FASTSP_SMALLEXP", "EXPLOSION_LARGE4", 1, 2, 3.2f, 0.1f, 0, 70, 2, 50 ) == -1) {

        return false;
    }

    if(WORLD.GetAI()->AddEnemyTemplate("BOSS1", "BOSS1_GRAPHIC", "BULLET_PLAYER", "EXPLOSION_LARGE5", 1, 30, 0.8f, 0.8f, 0, 3, 2, 200 ) == -1) {

        return false;
    }

    fprintf (LOGFile, "Enemies Created.\n");

    return true;
}

// Hard coded wave data until I get around to adding it to the loader
void CWorldModel::LoadHardCodedWaveData()
{
	// test, wave 1
	TWaveData *waveData=NULL;

	// test, wave 3  - first one
	waveData=new TWaveData;
	waveData->name="Wave1";
	waveData->entryRate=20; // ticks
	waveData->numEnemies=7;
	waveData->enemyTemplateIds=new int[waveData->numEnemies];
	waveData->enemyTemplateIds[0]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[1]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[2]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[3]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[4]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[5]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[6]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->numWayPoints=5;
	waveData->wayPoints=new CVector3[waveData->numWayPoints];
	waveData->wayPoints[0]=CVector3(200,20,0);
	waveData->wayPoints[1]=CVector3(280,150,0);
	waveData->wayPoints[2]=CVector3(200,250,0);
	waveData->wayPoints[3]=CVector3(120,300,0);
	waveData->wayPoints[4]=CVector3(400,200,0);
	int wave1=m_ai->AddWave(waveData);

	// test, wave 2
	waveData=new TWaveData;
	waveData->name="Wave2";
	waveData->entryRate=150;
	waveData->numEnemies=1;
	waveData->enemyTemplateIds=new int[waveData->numEnemies];
	waveData->enemyTemplateIds[0]=m_ai->GetEnemyTemplateIdByName("PURPLE_FIEND1");
	waveData->numWayPoints=2;
	waveData->wayPoints=new CVector3[waveData->numWayPoints];
	waveData->wayPoints[0]=CVector3(20,70,0);
	waveData->wayPoints[1]=CVector3(450,80,0);
	int wave2=m_ai->AddWave(waveData);

	waveData=new TWaveData;
	waveData->name="Wave3";
	waveData->entryRate=60; // ms
	waveData->numEnemies=3;
	waveData->enemyTemplateIds=new int[waveData->numEnemies];
	waveData->enemyTemplateIds[0]=m_ai->GetEnemyTemplateIdByName("RED_FLIPPER1");
	waveData->enemyTemplateIds[1]=m_ai->GetEnemyTemplateIdByName("RED_FLIPPER1");
	waveData->enemyTemplateIds[2]=m_ai->GetEnemyTemplateIdByName("RED_FLIPPER1");
	waveData->numWayPoints=4;
	waveData->wayPoints=new CVector3[waveData->numWayPoints];
	waveData->wayPoints[0]=CVector3(100,20,0);
	waveData->wayPoints[1]=CVector3(150,200,0); // fires straight at player - good
	waveData->wayPoints[2]=CVector3(800,0,0); // purposefully off screen
	waveData->wayPoints[3]=CVector3(300,450,0);
	int wave3=m_ai->AddWave(waveData);

	waveData=new TWaveData;
	waveData->name="Wave4";
	waveData->entryRate=17;
	waveData->numEnemies=10;
	waveData->enemyTemplateIds=new int[waveData->numEnemies];
	waveData->enemyTemplateIds[0]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[1]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[2]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[3]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[4]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[5]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[6]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[7]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[8]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[9]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->numWayPoints=8;
	waveData->wayPoints=new CVector3[waveData->numWayPoints];
	waveData->wayPoints[0]=CVector3(245,20,0);
	waveData->wayPoints[1]=CVector3(230,120,0);
	waveData->wayPoints[2]=CVector3(260,220,0);
	waveData->wayPoints[3]=CVector3(230,320,0);
	waveData->wayPoints[4]=CVector3(260,420,0);
	waveData->wayPoints[5]=CVector3(245,500,0);
	waveData->wayPoints[6]=CVector3(300,320,0);
	waveData->wayPoints[7]=CVector3(100,120,0);
	int wave4=m_ai->AddWave(waveData);

	// wave 5 - boss
	waveData=new TWaveData;
	waveData->name="Wave5";
	waveData->entryRate=150;
	waveData->numEnemies=1;
	waveData->enemyTemplateIds=new int[waveData->numEnemies];
	waveData->enemyTemplateIds[0]=m_ai->GetEnemyTemplateIdByName("BOSS1");
	waveData->numWayPoints=3;
	waveData->wayPoints=new CVector3[waveData->numWayPoints];
	waveData->wayPoints[0]=CVector3(10,10,0);
	waveData->wayPoints[1]=CVector3(450,300,0);
	waveData->wayPoints[2]=CVector3(250,80,0);
	int wave5=m_ai->AddWave(waveData);

	DWORD timeBetweenWaves=15*1000;

	// Create the first level
	CLevel *newLevel=new CLevel;
	// Chance of power up is last var
	// 0 = every time
	// 4 = every 4th time on average etc.
	newLevel->AddWaveId(wave1,timeBetweenWaves,4);
	newLevel->AddWaveId(wave2,timeBetweenWaves,0);
	newLevel->AddWaveId(wave3,timeBetweenWaves,4);
	newLevel->AddWaveId(wave4,timeBetweenWaves,2);
	newLevel->AddWaveId(wave5,timeBetweenWaves,0);
	m_ai->AddLevel(newLevel);

	// Some other waves for the next level
	waveData=new TWaveData;
	waveData->name="Wave6";
	waveData->entryRate=15;
	waveData->numEnemies=12;
	waveData->enemyTemplateIds=new int[waveData->numEnemies];
	waveData->enemyTemplateIds[0]=m_ai->GetEnemyTemplateIdByName("RED_FLIPPER1");
	waveData->enemyTemplateIds[1]=m_ai->GetEnemyTemplateIdByName("RED_FLIPPER1");
	waveData->enemyTemplateIds[2]=m_ai->GetEnemyTemplateIdByName("RED_FLIPPER1");
	waveData->enemyTemplateIds[3]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[4]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[5]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[6]=m_ai->GetEnemyTemplateIdByName("RED_FLIPPER1");
	waveData->enemyTemplateIds[7]=m_ai->GetEnemyTemplateIdByName("RED_FLIPPER1");
	waveData->enemyTemplateIds[8]=m_ai->GetEnemyTemplateIdByName("RED_FLIPPER1");
	waveData->enemyTemplateIds[9]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[10]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->enemyTemplateIds[11]=m_ai->GetEnemyTemplateIdByName("GREEN_SPINNER1");
	waveData->numWayPoints=8;
	waveData->wayPoints=new CVector3[waveData->numWayPoints];
	waveData->wayPoints[0]=CVector3(0,20,0);
	waveData->wayPoints[1]=CVector3(300,120,0);
	waveData->wayPoints[2]=CVector3(100,220,0);
	waveData->wayPoints[3]=CVector3(550,320,0);
	waveData->wayPoints[4]=CVector3(60,420,0);
	waveData->wayPoints[5]=CVector3(245,500,0);
	waveData->wayPoints[6]=CVector3(300,320,0);
	waveData->wayPoints[7]=CVector3(100,120,0);
	int wave6=m_ai->AddWave(waveData);

	// Like no. 2 but different direction
	waveData=new TWaveData;
	waveData->name="Wave7";
	waveData->entryRate=15;
	waveData->numEnemies=1;
	waveData->enemyTemplateIds=new int[waveData->numEnemies];
	waveData->enemyTemplateIds[0]=m_ai->GetEnemyTemplateIdByName("PURPLE_FIEND1");
	waveData->numWayPoints=3;
	waveData->wayPoints=new CVector3[waveData->numWayPoints];
	waveData->wayPoints[0]=CVector3(20,70,0);
	waveData->wayPoints[1]=CVector3(450,350,0);
	waveData->wayPoints[2]=CVector3(50,500,0);
	int wave7=m_ai->AddWave(waveData);

	// Level 2
	timeBetweenWaves=12*1000;
	newLevel=new CLevel;
	newLevel->AddWaveId(wave6,timeBetweenWaves,4);
	newLevel->AddWaveId(wave7,2*1000,0);
	newLevel->AddWaveId(wave7,2*1000,0);
	newLevel->AddWaveId(wave7,timeBetweenWaves,0);
	newLevel->AddWaveId(wave5,timeBetweenWaves*5,0); // Big gap before boss
	m_ai->AddLevel(newLevel);

	// Level 3
	timeBetweenWaves=12*1000;
	newLevel=new CLevel;
	newLevel->AddWaveId(wave6,timeBetweenWaves,4);
	newLevel->AddWaveId(wave6,2*1000,0);
	newLevel->AddWaveId(wave1,timeBetweenWaves,4);
	newLevel->AddWaveId(wave2,timeBetweenWaves,0);
	newLevel->AddWaveId(wave7,2*1000,0);
	newLevel->AddWaveId(wave4,timeBetweenWaves,2);
	newLevel->AddWaveId(wave7,2*1000,0);
	newLevel->AddWaveId(wave5,timeBetweenWaves*4,0); // Big gap before boss
	m_ai->AddLevel(newLevel);

// TODO: Could change chance of health power up per level but maybe not needed
}

// Should go in UI - TBD
// Render numbers using the loaded numbers graphic
// Starting at x,y
#if defined(WINCE)
const int kCharWidth=8;
const int kCharHeight=16;
const int kCharGap=kCharWidth+2; // for screen positioning only
#else
const int kCharWidth=16;
const int kCharHeight=32;
const int kCharGap=kCharWidth+2;
#endif
void CWorldModel::RenderNumber(int x,int y,int number,int numDigits)
{
	// m_numbersGraphic
	char buf[64];
	sprintf(buf,"%d",number);

	int currentX=x;

	// 6 digits always so if number < 100000 add zeros at start

	// 0 rectangle
	SDL_Rect rectForZero;
	rectForZero.x = 0;
    rectForZero.y = 0;
	rectForZero.w = kCharWidth;
	rectForZero.h = kCharHeight;

	int numDigitsInNumber=1;
	int counter=number;
	while(counter>=10)
	{
		counter/=10;
		numDigitsInNumber++;
	}

	while(numDigitsInNumber<numDigits)
	{
		// Render a zero
		VIZ.RenderSprite(m_numbersGraphic,currentX,y,&rectForZero);

		numDigitsInNumber++;
		currentX+=kCharGap;
	}

	for (size_t i=0;i<strlen(buf);i++)
	{
		int numb=buf[i]-'0';
		assert(numb>=0 && numb<10);

		// Only works when square :(
		//VIZ.RenderSpriteAnimate(m_numbersGraphic,currentX,y,number);

		SDL_Rect rect;

		rect.x=numb*kCharWidth;
		rect.y=0;
		rect.w=rect.x+kCharWidth;
		rect.h=kCharHeight;

		VIZ.RenderSprite(m_numbersGraphic,currentX,y,&rect);

		currentX+=kCharGap;
	}
}

// Until I get around to adding them to the data file
void CWorldModel::LoadHardCodedLevelData()
{
	// Data per level
	// 1. Chance of a power up and the range of health values
	int chanceOfPowerUp=4;
	int rangeOfHealthValues=15;
	int minimumHealthValue=5;

	m_powerUpManager->SetChanceOfPowerUp(chanceOfPowerUp);
	m_powerUpManager->SetHealthRange(minimumHealthValue,rangeOfHealthValues);

	// 2. Player movement speed
	// Note: now this must always be 1 as speed is passed in as part of move direction
	float playerSpeed=1.0f;

	m_worldEntityManager->GetPlayerEntity()->SetSpeed(playerSpeed);

	// 3. Player initial and max health
	int playerHealth=200;

	m_worldEntityManager->GetPlayerEntity()->SetHealth(playerHealth);
	m_worldEntityManager->GetPlayerEntity()->SetMaxHealth(playerHealth);

	// 4 number of lives - not being used
	int playerLives=3;
	m_worldEntityManager->GetPlayerEntity()->SetNumLives(playerLives);


	// Make player invincible if required
	m_worldEntityManager->GetPlayerEntity()->MakeInvincible(m_invincible);

	m_worldEntityManager->GetPlayerEntity()->SetScore(0);

	// 4. Wave sequences
}

void CWorldModel::ChangeGameMode(EGameMode newMode)
{
	m_gameMode=newMode;

	switch(m_gameMode)
	{
		case eGameModePlaying:
			Reset();
			m_tick->Pause(false);
			UI.TurnScreenOn(m_hudScreenId);
			UI.TurnScreenOff(m_startScreenId);
		break;
		case eGameModeGameOver:
			m_tick->Pause(true);
			//m_ai->TogglePause();
		break;
		case eGameModeFrontScreen:
			m_tick->Pause(true);
			UI.TurnScreenOff(m_hudScreenId);
			UI.TurnScreenOn(m_startScreenId);
		break;

		default:
//ry			EsError(_T("Unknown game mode"));
		break;
	}
}

// reset everything
void CWorldModel::Reset()
{
	m_bulletSystem->Reset();
	m_explosionManager->Reset();
//	m_powerUpManager->Reset();
	m_worldEntityManager->Reset();
	m_ai->Reset();

	// TODO - reset player stats
	CVector3 pos=CVector3(0.5f*m_map->GetScreenWidth(),m_map->GetScreenHeight()-64.0f,0.0f);
	m_worldEntityManager->GetPlayerEntity()->SetCentrePosition(pos);
	m_worldEntityManager->GetPlayerEntity()->SetActive(true);

	LoadHardCodedLevelData();
}
