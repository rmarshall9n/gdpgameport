// WorldEntityScenery.h: interface for the CWorldEntityScenery class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WORLDENTITYSCENERY_H__702C4ED0_C561_4CEA_8F66_D03519FA319E__INCLUDED_)
#define AFX_WORLDENTITYSCENERY_H__702C4ED0_C561_4CEA_8F66_D03519FA319E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WorldEntity.h"

class CWorldEntityScenery : public CWorldEntity
{
private:
	void UpdateSpecific();
public:
	CWorldEntityScenery(const char *name);
	~CWorldEntityScenery();
};

#endif // !defined(AFX_WORLDENTITYSCENERY_H__702C4ED0_C561_4CEA_8F66_D03519FA319E__INCLUDED_)
