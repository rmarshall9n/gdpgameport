#pragma once

#include "WorldEntityExplosion.h"
#include "LevelChunkData.h"

class CExplosionManager
{
private:
	int m_maxExplosions;
	int m_explosionEntityStartIndex;

	std::vector<CWorldEntityExplosion*> m_explosionTemplates;
	CWorldEntityExplosion* GetExplosionTemplateById(int id) const;
public:
	CExplosionManager(void);
	~CExplosionManager(void);
	void Initialise(int explosionEntityStartIndex, int maxExplosions);

	int AddExplosionTemplate(const TExplosionEntityChunk& data);
	int AddExplosionTemplate(const char* name, const char* graphicName);

	int GetExplosionTemplateIdByName(const char* name);

	void Explode(const CVector3 &pos, int explosionTemplateId);
	void Reset();
};
