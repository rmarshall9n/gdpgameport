#ifndef UIENTITYIMAGE1_H
#define UIENTITYIMAGE1_H

#include "UiEntity.h"

/**
 * \class CUiEntityImage
 * \brief A straight forward image entity
 * \author Keith Ditchburn \date 24 September 2005
*/
class CUiEntityImage : public CUiEntity
{
private:
	int m_spriteId;
public:
	CUiEntityImage(int spriteId);
	~CUiEntityImage(void);

	void Update();
	void Render(float dt);
};

#endif // UIENTITYIMAGE1_H
