#pragma once

#include "Rectangle.h"

// Graphic data surface
class CSurface
{
private:
	BYTE *m_data;
	int m_width;
	int m_height;
	bool m_deleteDataOnExit;
	bool m_hasAlpha;

	inline BYTE* GetData() const {return m_data;}

	// Blit into destination starting at x,y. Use the area passed in or all if null
	// NOTE: dangerous as does no clipping of rectangle
	void Blit(BYTE *destination,int destPitch,int dx,int dy,const CRectangle &sourceRect);
	void BlitTransparent(BYTE *destination,int destPitch,int dx,int dy,const CRectangle &sourceRect);
public:
	// Constructors
	// Create from passed data
	CSurface(BYTE *data,int width,int height,bool deleteOnExit=true);
	// Create new blank surface
	CSurface(int width,int height);

	// Destructor
	~CSurface(void);

	// Gets
	inline int GetWidth() const {return m_width;}
	inline int GetHeight() const {return m_height;}

	// Note: may be changed later if I use a part of another texture
	//inline int GetPitch() const {return m_width;}

	// Functionality



	// for surfaces - best one as clips rectangles
	void Blit(CSurface *destination,int dx,int dy,CRectangle *sourceRectIn=0);
	void BlitTransparent(CSurface *destination,int dx,int dy,CRectangle *sourceRectIn=0);



	void Fill(BYTE r,BYTE g,BYTE b,BYTE a=255);
	void Recolour(BYTE r,BYTE g,BYTE b, BYTE a, BYTE nr,BYTE ng,BYTE nb,BYTE na);
};
