#include "common.h"
#include "level.h"

CLevel::CLevel(void) : m_currentWave(0)
{
	m_waveVector.reserve(16);
}

CLevel::~CLevel(void)
{
	m_waveVector.clear();
}

void CLevel::AddWaveId(int waveId,DWORD timeUntilNextWave,int chanceOfPowerUp)
{
	TWaveSettings sets;

	sets.waveId=waveId;
	sets.chanceOfPowerUp=chanceOfPowerUp;
	sets.timeUntilNextWave=timeUntilNextWave;

	m_waveVector.push_back(sets);
}

// return false if level ended
void CLevel::AdvanceToNextWave()
{
	m_currentWave++;
}

bool CLevel::HaveAllWavesBeenLaunched()
{
	if (m_currentWave>=(int)m_waveVector.size())
		return true;

	return false;
}
