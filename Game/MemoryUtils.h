#pragma once

//ry #include "common.h"

namespace Utility{

class CFilename;

/**
 * \class CMemory
 * \brief handles basic memory operations
 * \author Keith Ditchburn
 * \date 20/05/2004
*/
class CMemory
{
private:
	CMemory(void){};
	virtual ~CMemory(void)=0;
public:
	static char* DuplicateString(const char* string);
	static char* DuplicateStringChar(const char* string);
	static BYTE* LoadFileIntoMemory(const CFilename &filename,DWORD32 *numBytesRead);
};

}
