#include "Common.h"
#include "WorldModel.h"
#include "worldentitybullet.h"
#include "map.h"
#include "BulletSystem.h"

CWorldEntityBullet::CWorldEntityBullet(const char *name): CWorldEntity(name), m_multi(0)
{
}

CWorldEntityBullet::~CWorldEntityBullet(void)
{
}

void CWorldEntityBullet::InitFromTemplate(const CWorldEntityBullet* other)
{
	assert(other);

	SetCollisionCost(other->GetCollisionCost());
	SetSpeed(other->GetSpeed());
	SetGraphicId(other->GetGraphicId());
	SetHealth(other->GetHealth());
	SetExplosionTemplateId(other->GetExplosionTemplateId());
	SetLifeTime(other->GetLifeTime());
	SetMulti(other->GetMulti());

	// If multi enabled then use life time to determine when to die
	// requires random setting here which is a bit clumsy TBD
	if (GetMulti()>0)
		SetLifeTime(50+rand()%200);
}

// Only called if active
void CWorldEntityBullet::UpdateSpecific()
{
	MoveForward();

	CVector3 pos=GetCentrePosition();

	// Check for out of bounds
	int w=WORLD.GetMap()->GetScreenWidth();
	int h=WORLD.GetMap()->GetScreenHeight();

	float halfMyWidth=0.5f*GetWidth();
	float halfMyHeight=0.5f*GetHeight();

	// Die without exploding if out of bounds
	if (pos.x<halfMyWidth || pos.x>w+halfMyWidth)
		DieNow();
	else
	if (pos.y<halfMyHeight || pos.y>h+halfMyHeight)
		DieNow();

	// If multi enabled then so is life time
}

void CWorldEntityBullet::HandlePreDeathActions()
{
	if (GetMulti()==0)
		return;

	CVector3 pos=GetLastCentrePosition();

	for (int i=0;i<GetMulti();i++)
	{
		// If multi set then spawn bullets
		CVector3 rot(0,0,0);

		// random direction
		float d=(float)(rand()%360);
		rot.y=d/26.648f;

		// Also to prevent too much similar patterns randominse the position a wee bit
		pos.x += 0;//ry (rand() % (1.5, -1.5)) + -1.5;
		pos.y += 0;//ry (rand() % (1.5, -1.5)) + -1.5;

		WORLD.GetBulletSystem()->Fire(pos,rot,3,GetSide()); // template ID TBD
	}
}
