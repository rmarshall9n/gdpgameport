#pragma once

/**
 * \class CColour
 * \brief BAsic colour class
 * \author Keith Ditchburn \date 24 September 2005
*/
class CColour
{
public:
	BYTE m_red;
	BYTE m_green;
	BYTE m_blue;
	BYTE m_alpha;
public:
	CColour() : m_red(255),m_green(255),m_blue(255),m_alpha(255) {}
	CColour(const CColour &other)
	{
		m_red=other.m_red;
		m_green=other.m_green;
		m_blue=other.m_blue;
		m_alpha=other.m_alpha;
	}
	CColour(BYTE r,BYTE g,BYTE b,BYTE a=255) : m_red(r),m_green(g),m_blue(b),m_alpha(a) {}
	~CColour(void) {};
};
