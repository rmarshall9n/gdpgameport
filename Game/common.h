#pragma once

// Precompiled header

// When running on EMULATOR
//#define EMULATOR

#include <cassert>
#include <vector>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
// For min max std
#include <algorithm>
using std::min;
using std::max;

// Macros
#define MAX(a,b) (((a)>(b))?(a):(b))
#define MIN(a,b) (((a)<(b))?(a):(b))

// Some useful types
typedef unsigned short WORD;
typedef unsigned long DWORD;
typedef unsigned int DWORD32;
typedef unsigned char BYTE;

#define SAFE_FREE(x) {if (x) free(x);x=0;}
#define SAFE_DELETE(x) {if (x) delete x;x=0;}
#define SAFE_DELETE_ARRAY(x) {if (x) delete[] x;x=0;}

// Do not normally put own headers here but exception for this widely used one
//ry #include "Vector3.h"

//ry typedef CVector3 TVector3;

#define ReleaseAssert(expr) if (!(expr)) EsOutput(_T("ReleaseAssert failed, %s : %d\n"),__FILE__, __LINE__);

//#include "tchar.h"
//#pragma warning(disable : 4996) // disable deprecated warnings
//#include "strsafe.h"
#define FORMATTED_STRING StringCbPrintf
#define FORMATTED_STRING_VALIST StringCbVPrintf
#define COPY_STRING StringCbCopy
#define COPY_STRING_N StringCbCopyN
#define CAT_STRING StringCbCat
#define CMP_STRING _tcscmp
#define CMP_STRING_N _tcsncmp

// Now removed from error system so dummy:
//#define EsCheckSt(hr)   Es.CheckForFsError(__FILE__, __LINE__,hr)
#define EsCheckSt(hr) (hr)


// Original libraries used - changed for GDP 24/10/13CVector3
#include "MissingLibraryStubs.h"
//#include <UtilityComponent.h>
//#include <ErrorSystem.h>
//#include <StringComponent.h>
//#include <ZLibrary.h>
