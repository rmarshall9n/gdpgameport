#include "Common.h"
#include "sprite.h"

#include <assert.h>
#include <string.h>

CSprite::CSprite(const char *name) : m_surface(0), m_currentSurface(0), m_numFrames(0),
                    m_frameSize(0),	m_frameSpeed(1), m_spriteWidth(0),m_spriteHeight(0),
                    m_frameMode(0)
{
	m_name = new char[strlen(name)+1];
	strcpy(m_name, name);
}

CSprite::~CSprite(void)
{
    if(m_surface)
        SDL_FreeSurface(m_surface);

	delete []m_name;
}

/**
 * \brief Add a new surface to this sprite
 * \param
 * \return id
*/
void CSprite::AddSurface(SDL_Surface* surface, bool createCollisionMask,
                         int numFrames, int frameSize, float frameSpeed,
                         int mode)
{
    assert(!m_surface);

	m_numFrames = numFrames;
	m_frameSize = frameSize;
	m_frameSpeed = frameSpeed;
	m_frameMode = mode;
	m_surface = surface;

	// NOTE: animation:
	if (m_numFrames)
	{
		// Different to surface dimensions:
		m_spriteWidth = m_frameSize;
		m_spriteHeight = m_frameSize;
	}
	else
	{
		m_spriteWidth = m_surface->w;
		m_spriteHeight = m_surface->h;
	}
}

/**
 * \brief Render current surface to passed surface
 * \param
 * \return sucess
*/
void CSprite::Render(SDL_Surface* destination, int x, int y, SDL_Rect *rect)
{
	assert(destination);
	assert(m_surface);

    SDL_Rect offset = {x, y};

	// Blit it
	SDL_BlitSurface(m_surface, rect, destination, &offset);
}

void CSprite::RenderAnimate(SDL_Surface *destination, int x, int y,
                            int frameNumber)
{
	assert(m_numFrames > 0);
	assert(frameNumber < m_numFrames);
	assert(m_surface);

    SDL_Rect offset = {x, y};

	SDL_Rect rect = {0, 0, 0, 0};
	GetFrameRect(frameNumber, &rect);

	// Blit it
	SDL_BlitSurface(m_surface, &rect, destination, &offset);
}

void CSprite::GetFrameRect(int frameNumber, SDL_Rect *rect)
{
	assert(rect);
	assert(m_surface);

	// frame number begins at 0
	// how many across:
	int numAcross = m_surface->w / m_frameSize;

	int c = frameNumber / numAcross;
	int r = frameNumber - (c * numAcross);

	int index = c * numAcross + r;

	assert(index < m_numFrames);

	rect->x = r * m_frameSize;
	rect->w = m_frameSize;

	rect->y = c * m_frameSize;
	rect->h = m_frameSize;
}

// retrieve width and height
void CSprite::Get2DDimensions(int *width, int *height) const
{
	*width = m_spriteWidth;
	*height = m_spriteHeight;
}
