#pragma once

//#include "Common.h"


class CToolBox
{
private:

public:
	CToolBox(void);
	~CToolBox(void);

	//static void DebugOutputCall(const char *str,...)
	//{
	//	char tmp[2048];

	//	// Parse the variable list
	//	va_list ptr ;
	//	va_start(ptr,str) ;
	//	v_stprintf(tmp,str,ptr) ;
	//	//StringCbVPrintf(tmp,2048,str,ptr);

	//	OutputDebugString(tmp);
	//}

	static void GetRootDirectory(int bufSize,char *buf);
	static void SplitThePath(const char *path, char *drv, char *dir, char *fname, char *ext);
	static DWORD GetTheTime();
	static char* MakePath(const char *pathIn);
	static void PointPath(int bufSize,char *path);

	static float ConvertValueFloatX(float xin);
	static float ConvertValueFloatY(float yin);

	static int ConvertValueX(int xin);
	static int ConvertValueY(int yin);
	static float RandRangeFloat(float min,float max);
};

//#define DebugOutput CToolBox::DebugOutputCall

#define PI (3.141592654f)
#define DEGTORAD(degree) ((PI / 180.0f) * (degree))
#define RADTODEG(radian) ((180.0f /PI) * (radian))



