#pragma once

#include "common.h"
#include "Filename.h"

#include "Vector3.h"
#include <stdio.h>

namespace Utility {

	/**
	* \class CTextFile
	* \brief Handles reading/writing to text file
	* UNICODE note - uses char except for final write or read where ASCII is used
	* \author Keith Ditchburn 20/12/2004
	*/
	class CTextFile
	{
	public:
		enum EOpenChoice
		{
			eOpenForWrite,
			eOpenForRead
		};
	private:
		enum ETextFileMode
		{
			eTextFileUnopend,
			eTextFileReading,
			eTextFileWriting
		};

		ETextFileMode m_mode;
		CFilename *m_filename;
		FILE *m_fp;

		std::vector<char*> m_commentsVector;
		std::vector<char> m_dataSeperatorsVector;

		bool ShouldLineBeIgnored(char*line);
		char *GetData(char *line, char *data);
		void ExtractFillCharacters(char *line);
		bool IsFillCharacter(char c);
	public:
		CTextFile(const CFilename &filename);
		CTextFile(const char *filename);
		~CTextFile(void);

		bool Open(EOpenChoice choice);
		void Close(){fclose(m_fp);m_mode=eTextFileUnopend;}

		void WriteLine(const char *str,...);
		void WriteBlankLine() {WriteLine("\n");}

		bool ReadLine(char *lineBuffer,int maxChars,bool ignoreCommentedLines=true);

		void AddComment(const char *comment);
		void AddDataSeperator(char seperator);

		int GetStreamError() { return ferror(m_fp);}

		bool LoadVector(CVector3 *vec,char *line);
		bool LoadAString(char *buf,int bufLength,char *line);
		bool LoadTwoFloats(float *f1,float *f2,char *line);
		bool LoadThreeFloats(float *f1,float *f2,float *f3,char *line);
		bool LoadThreeInts(int *i1,int *i2,int *i3,char *line);

		char* LoadFloat(float *fl,char *line);
		char *LoadInt(int *in,char *line);

		// Statics utility functions
		static char* ExtractKeywordValue( char *line,const char *keyword);
		static bool ContainsKeyword( char *line,const char *keyword);


	};

}
