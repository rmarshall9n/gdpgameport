#include "Common.h"
#include "worldentityexplosion.h"

CWorldEntityExplosion::CWorldEntityExplosion(const char *name) : CWorldEntity(name)
{
}

CWorldEntityExplosion::~CWorldEntityExplosion(void)
{
}

void CWorldEntityExplosion::InitFromTemplate(const CWorldEntityExplosion* other)
{
	SetCollisionCost(other->GetCollisionCost());
	SetSpeed(other->GetSpeed());
	SetGraphicId(other->GetGraphicId());
	SetHealth(other->GetHealth());
	SetLifeTime(other->GetLifeTime());
}

void CWorldEntityExplosion::UpdateSpecific()
{
	// tbd
}
