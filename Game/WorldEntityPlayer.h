//**************************************************************************************************
//
// WorldEntityPlayer.h: interface for the CWorldEntityPlayer class.
//
// Description: Player controlled entity
//
// Created by: Keith Ditchburn 15/03/2003
//
//**************************************************************************************************

#if !defined(AFX_WORLDENTITYPLAYER_H__31467DB3_386B_4ED0_B367_345E68E5FA0B__INCLUDED_)
#define AFX_WORLDENTITYPLAYER_H__31467DB3_386B_4ED0_B367_345E68E5FA0B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WorldEntity.h"

class CWorldEntityPlayer : public CWorldEntity
{
private:
	void UpdateSpecific();
	int m_score;
	int m_lives;
public:
	CWorldEntityPlayer(const char *name);
	~CWorldEntityPlayer();

	void SetNumLives(int set) {m_lives=set;}
	inline int GetNumLives() const {return m_lives;}

	void Move(const CVector3& dir);

	inline void IncreaseScore(int inc) {m_score+=inc;}
	inline int GetScore() const {return m_score;}
	inline void SetScore(int set) {m_score=set;}

	void ResetSpecific(){}
};

#endif // !defined(AFX_WORLDENTITYPLAYER_H__31467DB3_386B_4ED0_B367_345E68E5FA0B__INCLUDED_)
