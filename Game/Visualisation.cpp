#include "Common.h"
#include ".\visualisation.h"
#include "Sprite.h"

#include <string.h>
#include <SDL_image.h>

#include "Logger.h"
CVisualisation* CVisualisation::m_instance=0;

// Singleton functions
void CVisualisation::CreateInstance()
{
	if (m_instance != 0)
    {
	    fprintf (LOGFile, "Trying to create a second instance of viz!!\n");
		return;
	}

	m_instance = new CVisualisation();
}

void CVisualisation::DestroyInstance()
{
	if (m_instance == 0)
	{
	    fprintf (LOGFile, "Instance already destroyed!!\n");
		return;
	}

	delete m_instance;
	m_instance = 0;
}


CVisualisation::CVisualisation(void) : m_screenSurface(0), m_font(0)
{
	m_spriteVector.reserve(64);
    m_font = TTF_OpenFont("Game/Data/Fonts/Arial.TTF", 12);

    if( m_font == NULL )
    {
        fprintf(LOGFile, "TTF_OpenFont: %s\n", TTF_GetError());
    }
}

CVisualisation::~CVisualisation(void)
{
	// Remove all sprites
	for (std::vector<CSprite*>::iterator p = m_spriteVector.begin(); p < m_spriteVector.end(); ++p)
	{
		if (*p)
		{
			delete (*p);
			*p=0;
		}
	}

	m_spriteVector.clear();

	if (m_screenSurface)
	{
		m_screenSurface=0;
	}

	TTF_CloseFont(m_font);
	m_font = 0;
}

/**
 * \brief Initialise the component
 * \param
 * \return
*/
bool CVisualisation::Initialise(SDL_Surface* surface)
{
    m_screenSurface = surface;

	return true;
}

void CVisualisation::ClearScreenToColour(BYTE r,BYTE g,BYTE b)
{
	assert(m_screenSurface);

	SDL_FillRect(m_screenSurface, NULL, SDL_MapRGB(m_screenSurface->format,r,g,b));
}

/**
 * \brief create a sprite from the passed texture filename
 * \param
 * \return
*/
bool CVisualisation::CreateSprite(const char* filename, const char *name, int *id,
			bool createCollisionMask, int numFrames, int frameSize, float frameSpeed, int mode)
{
	assert(createCollisionMask==false);

	SDL_Surface* surface = IMG_Load(filename);

    if(!surface)
    {
        fprintf(LOGFile, "Error loading image: %s\n", IMG_GetError());
        return false;
    }


	CSprite *newSprite = new CSprite(name);
	newSprite->AddSurface(surface, createCollisionMask, numFrames, frameSize, frameSpeed, mode);

	// Add to our vector
	m_spriteVector.push_back(newSprite);

	*id = (int)(m_spriteVector.size() - 1);

	return true;
}

bool CVisualisation::CreateColourKeyedSprite(const char* filename, const char *name, int *id,
			bool createCollisionMask, int numFrames, int frameSize, float frameSpeed, int mode)
{
	assert(createCollisionMask==false);

	SDL_Surface* surface = IMG_Load(filename);

    if(!surface)
    {
        fprintf(LOGFile, "Error loading image: %s\n", IMG_GetError());
        return false;
    }

    // use this line to remove transparency and increase performance.
    SDL_Surface* tempSurface = SDL_DisplayFormat(surface);
    SDL_FreeSurface(surface);

    if(!tempSurface)
    {
        fprintf(LOGFile, "Error loading image: %s\n", IMG_GetError());
        return false;
    }

    Uint32 colorkey = SDL_MapRGB( tempSurface->format, 255, 0, 255 );
    SDL_SetColorKey( tempSurface, SDL_SRCCOLORKEY, colorkey );

	CSprite *newSprite = new CSprite(name);
	newSprite->AddSurface(tempSurface, createCollisionMask, numFrames, frameSize, frameSpeed, mode);

	// Add to our vector
	m_spriteVector.push_back(newSprite);

	*id = (int)(m_spriteVector.size() - 1);

	return true;
}

bool CVisualisation::RenderSprite(int id, int x, int y, SDL_Rect *rect)
{
	CSprite *sprite = GetSpriteById(id);
	if (!sprite)
		return false;

	sprite->Render(m_screenSurface, x, y, rect);

	return true;
}

bool CVisualisation::RenderSpriteAnimate(int graphicId,int x,int y,int frameNumber)
{
	CSprite *sprite=GetSpriteById(graphicId);

	if (!sprite)
		return false;

	sprite->RenderAnimate(m_screenSurface, x, y, frameNumber);
	return true;
}

int CVisualisation::GetSpriteIdByName(const char* name)
{
	std::vector <CSprite*>::const_iterator p;
	int count=0;
	for (p=m_spriteVector.begin();p!=m_spriteVector.end();++p)
	{
	    const char* otherName = (*p)->GetName();

		if (strcmp(name, otherName)==0)
			return count;

		count++;
	}
	return -1;
}

void CVisualisation::GetSprite2DDimensions(int graphicId, int *width,int *height) const
{
	assert(graphicId>=0 && graphicId<(int)m_spriteVector.size());

	m_spriteVector[graphicId]->Get2DDimensions(width, height);
}

int CVisualisation::GetSpriteAnimationData(int graphicId, float *frameSpeed, int *frameMode) const
{
	assert(graphicId>=0 && graphicId<(int)m_spriteVector.size());

	return m_spriteVector[graphicId]->GetAnimationData(frameSpeed, frameMode);
}

void CVisualisation::RenderText(int x, int y, char *text, BYTE r, BYTE g, BYTE b, BYTE a)
{
    SDL_Rect offset = {x, y};
    SDL_Color col = {r, g, b, a};

    SDL_Surface *textSurface = TTF_RenderText_Solid(m_font, text, col);
    if(!textSurface)
    {
        fprintf(LOGFile, "TTF_RenderText : %s\n", TTF_GetError());
    }
    else
    {
        SDL_BlitSurface(textSurface, NULL, m_screenSurface, &offset);
        SDL_FreeSurface(textSurface);
    }
}
