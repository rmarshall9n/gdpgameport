#include "common.h"
#include "UiEntityImage.h"
#include "Visualisation.h"

CUiEntityImage::CUiEntityImage(int spriteId) : CUiEntity(0), m_spriteId(spriteId)
{
}

CUiEntityImage::~CUiEntityImage(void)
{
}

void CUiEntityImage::Update()
{
}

void CUiEntityImage::Render(float dt)
{
	VIZ.RenderSprite(m_spriteId,GetScreenX(),GetScreenY());
}
