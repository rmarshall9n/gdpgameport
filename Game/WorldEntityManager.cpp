#include "Common.h"
#include "worldentitymanager.h"

#include "WorldEntity.h"
#include "WorldEntityBackground.h"
#include "WorldEntityBullet.h"
#include "WorldEntityEnemy.h"
#include "WorldEntityPlayer.h"
#include "WorldEntityScenery.h"
#include "WorldEntityExplosion.h"

// Constructor
CWorldEntityManager::CWorldEntityManager(void): m_bulletRange(0), m_enemyRange(0),
	m_explosionRange(0),m_powerUpRange(0),m_playerIndex(-1)
{

}

// Destructor
CWorldEntityManager::~CWorldEntityManager(void)
{
//ry	EsOutput(_T("World Entity Vector grew to %d\n"),m_entityVector.size());

	std::vector <CWorldEntity*>::iterator p;
	for (p=m_entityVector.begin();p!=m_entityVector.end();++p)
		delete (*p);

	m_entityVector.clear();

	if (m_bulletRange)
		delete m_bulletRange;

	if (m_enemyRange)
		delete m_enemyRange;

	if (m_explosionRange)
		delete m_explosionRange;

	if (m_powerUpRange)
		delete m_powerUpRange;
}

void CWorldEntityManager::Reset()
{
	std::vector <CWorldEntity*>::iterator p;
	for (p=m_entityVector.begin();p!=m_entityVector.end();++p)
	{
		(*p)->Reset();
	}
}


// Initialise values
void CWorldEntityManager::Initialise(int maxEntities,int maxBullets,int maxEnemies,
									 int maxExplosions,int maxPowerUps,int backgroundGraphicId)
{
	assert(!m_bulletRange);
	assert(!m_enemyRange);
	assert(!m_explosionRange);
	assert(!m_powerUpRange);

	m_entityVector.reserve(maxEntities);

	// index 0 is reserved for background and 1 for player:


	// Have to create background first for render order
	CreateBackground(backgroundGraphicId);


	// player is index 1
	m_entityVector.push_back(0);

	// Create the ranges

// Power ups
	m_powerUpRange=new CEntityTypeRange;
	m_powerUpRange->m_start=(int)m_entityVector.size();
	m_powerUpRange->m_length=maxPowerUps;

	for (int i=0;i<m_powerUpRange->m_length;i++)
	{
		// Make it a bullet:
		CWorldEntityBullet *newEntry=new CWorldEntityBullet("PowerUpInst");

		newEntry->SetActive(false);

		//All other values filled when created
		m_entityVector.push_back(newEntry);
	}

// Bullets
	m_bulletRange=new CEntityTypeRange;
	m_bulletRange->m_start=(int)m_entityVector.size();
	m_bulletRange->m_length=maxBullets;

	for (int i=0;i<m_bulletRange->m_length;i++)
	{
		CWorldEntityBullet *newEntry=new CWorldEntityBullet("BulletInst");

		newEntry->SetActive(false);
		//All other values filled by bullet manager as created
		m_entityVector.push_back(newEntry);
	}

// Enemies before explosions so they cover us
	m_enemyRange=new CEntityTypeRange;
	m_enemyRange->m_start=(int)m_entityVector.size();
	m_enemyRange->m_length=maxEnemies;

	for (int i=0;i<m_enemyRange->m_length;i++)
	{
		CWorldEntityEnemy *newEnemy=new CWorldEntityEnemy("EnemyInst");

		newEnemy->SetActive(false);
		newEnemy->SetSide(CWorldEntity::eSideEnemy);
		//All other values filled by AI as created
		m_entityVector.push_back(newEnemy);
	}

// explosions
	m_explosionRange=new CEntityTypeRange;
	m_explosionRange->m_start=(int)m_entityVector.size();
	m_explosionRange->m_length=maxExplosions;

	for (int i=0;i<m_explosionRange->m_length;i++)
	{
		CWorldEntityExplosion *newEntry=new CWorldEntityExplosion("ExplosionInst");

		newEntry->SetActive(false);

		//All other values filled by explosion manager as created
		m_entityVector.push_back(newEntry);
	}


}

// Background has no collision and is neutral
CWorldEntity* CWorldEntityManager::CreateBackground(int graphicId)
{
	CWorldEntityBackground *newBack=new CWorldEntityBackground("Background");
	newBack->SetGraphicId(graphicId);
	newBack->SetCollisionCost(0);
	newBack->SetSide(CWorldEntity::eSideNeutral);
	newBack->Initialise();

	m_entityVector.push_back(newBack);

	return m_entityVector[m_entityVector.size()-1];
}

CWorldEntity* CWorldEntityManager::CreatePlayer(int graphicId,const CVector3 &startPos,
												int bulletType)
{
	CWorldEntityPlayer *newEntry=new CWorldEntityPlayer("ThePlayerInst");

	newEntry->SetGraphicId(graphicId);
	newEntry->SetCollisionCost(1);
	newEntry->SetSide(CWorldEntity::eSidePlayer);
	//newEntry->SetMaxHealth(health);
	//newEntry->SetHealth(health);
	newEntry->SetStartCentrePosition(startPos);
	//newEntry->SetSpeed(speed); done elsewhere
	newEntry->SetBulletTemplateId(0); // tbd

	// always 0
	m_entityVector[1]=newEntry;
	//m_entityVector.push_back(newEntry);

	// Remember player index
//	m_playerIndex=m_entityVector.size()-1;
		m_playerIndex=1;

	return m_entityVector[m_playerIndex];
}



// Are these two entities enemies to each other?
bool CWorldEntityManager::AreEntitiesEnemies(int ent1,int ent2) const
{
	CWorldEntity::ESide side1=GetEntity(ent1)->GetSide();
	CWorldEntity::ESide side2=GetEntity(ent2)->GetSide();

	if (side1==side2)
		return false;

	if (side1==CWorldEntity::eSideNeutral)
		return false;

	if (side2==CWorldEntity::eSideNeutral)
		return false;

	return true;
}





