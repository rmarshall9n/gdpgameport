#pragma once

#include "WorldEntity.h"

class CWorldEntityBackground : public CWorldEntity
{
private:
	float m_splitY;
public:
	void UpdateSpecific();
	void Initialise();
	CWorldEntityBackground(const char *name);
	~CWorldEntityBackground();
	void Render(float dt);

	void ResetSpecific(){};

};
