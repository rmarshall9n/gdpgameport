/**
*
* \file Filename.cpp
*
* \brief Small class to sort out problems with filenames etc. that I have had in the past
* Just keeps it altogether nicely
*
* \author Keith Ditchburn 21/06/2002
*
*/

#include "common.h"
#include "Toolbox.h"
#include "Filename.h"
#include "MemoryUtils.h"
#include "stdio.h"

#if !defined(CAANOO)
#include <io.h>
#endif


namespace Utility{

	const DWORD32 kFilenameVersion=1;

	// The strings that match the keywords
	char gFileExtensions[CFilename::eNumFileTypes][64] =
	{
            ".bmp",
			".tga",
			".tif",
			".jpg",
			".png",
			".gif",
			".3ds",
			".x",
			".tgp",
			".tgm",
			".bt",
			".ico",
			".wmf",
			".pcx",
			".dds",
			".dib",
			".txt"
	};
	/*	eFileTypeUnknown=-1,
	eFileTypeBitmap,
	eFileTypeTarga,
	eFileTypeTif,
	eFileTypeGif,
	eFileTypeJpg,
	eFileTypePng,
	eFileType3DS,
	eFileTypeX,
	eFileTypeT2Project,
	eFileTypeT2Material,
	eFileTypeBt,
	eFileTypeIco,
	eFileTypeWmf,
	eFileTypePcx,
	eFileTypeDds,
	eFileTypeDib,
	eNumFileTypes
	*/
















	/**
	* \brief
	* \date  07/12/2002
	*/
	bool CFilename::operator == (const CFilename& c) const
	{
		if (strcmp(c.m_pathAndFilename,m_pathAndFilename)==0)
			return true;

		return false;
	}

	/**
	* \brief
	* \date  07/12/2002
	*/
	bool CFilename::operator != (const CFilename& c) const
	{
		if (strcmp(c.m_pathAndFilename,m_pathAndFilename)==0)
			return false;

		return true;
	}

	/**
	* \brief
	* \date  07/12/2002
	*/
	bool CFilename::operator < (const CFilename& c) const
	{
		if (strcmp(c.m_pathAndFilename,m_pathAndFilename)<0)
			return true;

		return false;
	}

	/**
	* \brief
	* \date  07/12/2002
	*/
	bool CFilename::operator > (const CFilename& c) const
	{
		if (strcmp(c.m_pathAndFilename,m_pathAndFilename)>0)
			return true;

		return false;
	}

	/**
	*/
	CFilename::CFilename()
	{
		m_filename=NULL;
		m_path=NULL;
		m_pathAndFilename=NULL;
		m_filenameMinusExtension=NULL;
	}

	/**
	* \brief  Construct via full path containing path and filename
	* \date  21/06/2002
	*/
	CFilename::CFilename(const char *pathAndFilename)
	{
		EsAssert(pathAndFilename);
		m_filename=NULL;
		m_path=NULL;
		m_pathAndFilename=NULL;
		m_filenameMinusExtension=NULL;

		m_filename=ExtractFilenameFromPath(pathAndFilename);
		m_path=ExtractPathFromPathAndFilename(pathAndFilename);
		m_pathAndFilename=CMemory::DuplicateString(pathAndFilename);
		m_filenameMinusExtension=ExtractFilenameFromPathNoExtension(m_filename);
	}

	/**
	* \brief  Construct via path and filename
	* \date  21/06/2002
	*/
	CFilename::CFilename(const char *path,const char *filename)
	{
		EsAssert(path);
		EsAssert(filename);

		m_filename=NULL;
		m_path=NULL;
		m_pathAndFilename=NULL;
		m_filenameMinusExtension=NULL;

		// Check path has \ at end
		size_t pathLength=strlen(path);
		if (pathLength>1 && path[pathLength-1]!='\\')
		{
			m_path=(char*)malloc((pathLength+2)*sizeof(char));
			EsCheckAlloc(m_path);

			//_stprintf(m_path,_T("%s\\"),path);
//ry			EsCheckSt(FORMATTED_STRING(m_path,(pathLength+2)*sizeof(char),"%s\\",path));
			//StringCbPrintf(m_path,pathLength+2,_T("%s\\"),path);
		}
		else
		{
			m_path=CMemory::DuplicateString(path);
		}

		m_filename=CMemory::DuplicateString(filename);

		char buf[MAX_PATH];
//ry		EsCheckSt(FORMATTED_STRING(buf,MAX_PATH*sizeof(char),"%s%s",m_path,filename));

		m_pathAndFilename=CMemory::DuplicateString(buf);

		m_filenameMinusExtension=ExtractFilenameFromPathNoExtension(m_filename);
	}

	/**
	* \brief  Construct via other filename
	* \date  21/06/2002
	*/
	CFilename::CFilename(CFilename *filename)
	{
		EsAssert(filename);

		m_filename=NULL;
		m_path=NULL;
		m_pathAndFilename=NULL;
		m_filenameMinusExtension=NULL;

		m_filename=CMemory::DuplicateString(filename->GetFilename());
		m_path=CMemory::DuplicateString(filename->GetPath());
		m_pathAndFilename=CMemory::DuplicateString(filename->GetPathAndFilename());
		m_filenameMinusExtension=ExtractFilenameFromPathNoExtension(m_filename);
	}

	/**
	* \brief  Construct via other const filename
	* \date  21/06/2002
	*/
	CFilename::CFilename(const CFilename &filename)
	{
		m_filename=NULL;
		m_path=NULL;
		m_pathAndFilename=NULL;
		m_filenameMinusExtension=NULL;

		m_filename=CMemory::DuplicateString(filename.GetFilename());

		// It is possible to not have a path as file may be in current dir - so:
		if (filename.GetPath())
			m_path=CMemory::DuplicateString(filename.GetPath());

		m_pathAndFilename=CMemory::DuplicateString(filename.GetPathAndFilename());
		m_filenameMinusExtension=ExtractFilenameFromPathNoExtension(m_filename);
	}



	/**
	*/
	CFilename::~CFilename()
	{
		ReleaseMemory();
	}

	/**
	* \brief
	* \date  21/06/2002
	*/
	void CFilename::SetViaPathAndFilename(const char *pathAndFilename)
	{
		EsAssert(pathAndFilename);

		ReleaseMemory();

		m_filename=ExtractFilenameFromPath(pathAndFilename);
		m_path=ExtractPathFromPathAndFilename(pathAndFilename);
		m_pathAndFilename=CMemory::DuplicateString(pathAndFilename);
		m_filenameMinusExtension=ExtractFilenameFromPathNoExtension(m_filename);

		EsAssert(m_filename);
		EsAssert(m_path);
		EsAssert(m_pathAndFilename);
		EsAssert(m_filenameMinusExtension);
	}

	/**
	* \brief
	* \date  21/06/2002
	*/
	void CFilename::SetViaPathAndFilename(const char *path, const char *filename)
	{
		EsAssert(path);
		EsAssert(filename);

		ReleaseMemory();

		m_path=CMemory::DuplicateString(path);
		m_filename=CMemory::DuplicateString(filename);

		char buf[MAX_PATH];
		//_stprintf(buf,_T("%s%s"),path,filename);
//		EsCheckSt(FORMATTED_STRING(buf,MAX_PATH*sizeof(char),_T("%s%s"),path,filename));

		m_pathAndFilename=CMemory::DuplicateString(buf);
		m_filenameMinusExtension=ExtractFilenameFromPathNoExtension(m_filename);

		EsAssert(m_filename);
		EsAssert(m_path);
		EsAssert(m_pathAndFilename);
		EsAssert(m_filenameMinusExtension);
	}

	/**
	* \brief
	* \date  21/06/2002
	*/
	void CFilename::ReleaseMemory()
	{
		SAFE_FREE(m_filename);
		SAFE_FREE(m_path);
		SAFE_FREE(m_pathAndFilename);
		SAFE_FREE(m_filenameMinusExtension);
	}

	/**
	* \brief  Allows path to be changed. There must be a filename and path set prior to this call
	* \date  21/06/2002
	*/
	void CFilename::ChangePath(const char *newPath)
	{
		EsAssert(newPath);
		EsAssert(m_filename);
		EsAssert(m_path);
		EsAssert(m_pathAndFilename);
		EsAssert(m_filenameMinusExtension);

		char newPathAndFilename[MAX_PATH];

		// Path may not have \ check
		if (strlen(newPath)>0 && newPath[strlen(newPath)-1]!='\\')
        {
			//_stprintf(newPathAndFilename,_T("%s\\%s"),newPath,m_filename);
//ry			EsCheckSt(FORMATTED_STRING(newPathAndFilename,MAX_PATH*sizeof(char),_T("%s\\%s"),newPath,m_filename));
        }
		else
        {
			//_stprintf(newPathAndFilename,_T("%s%s"),newPath,m_filename);
//ry			EsCheckSt(FORMATTED_STRING(newPathAndFilename,MAX_PATH*sizeof(char),_T("%s%s"),newPath,m_filename));
        }

		SetViaPathAndFilename(newPathAndFilename);
	}

	/**
	* \brief  Allows filename to be changed. Again must be set prior to this
	* \date  21/06/2002
	*/
	void CFilename::ChangeFilename(const char *newFilename)
	{
		EsAssert(newFilename);
		EsAssert(m_filename);
		EsAssert(m_path);
		EsAssert(m_pathAndFilename);
		EsAssert(m_filenameMinusExtension);

		char newPathAndFilename[MAX_PATH];
		//_stprintf(newPathAndFilename,_T("%s%s"),m_path,newFilename);
//ry		EsCheckSt(FORMATTED_STRING(newPathAndFilename,MAX_PATH*sizeof(char),_T("%s%s"),m_path,newFilename));

		SetViaPathAndFilename(newPathAndFilename);
	}

	/**
	* \brief Allows filename only to be changed. Again must be set prior to this
	* \date  10/12/2004
	*/
	void CFilename::ChangeFilenameOnly(const char *newFilename)
	{
		EsAssert(newFilename);
		EsAssert(m_filename);
		EsAssert(m_path);
		EsAssert(m_pathAndFilename);
		EsAssert(m_filenameMinusExtension);

		char newPathAndFilename[MAX_PATH];
		char ext[MAX_PATH];

		GetExtension(ext,MAX_PATH);

		//_stprintf(newPathAndFilename,_T("%s%s%s"),m_path,newFilename,ext);
//ry		EsCheckSt(FORMATTED_STRING(newPathAndFilename,MAX_PATH*sizeof(char),_T("%s%s%s"),m_path,newFilename,ext));

		SetViaPathAndFilename(newPathAndFilename);

	}




	/**
	* \brief  Uses file helper
	* \date  12/12/2002
	*/
	//void CFilename::Save()
	//{
	//	FILE_HELPER.Write(kFilenameVersion);

	//	FILE_HELPER.WriteString(m_filename);
	//	FILE_HELPER.WriteString(m_filenameMinusExtension);
	//	FILE_HELPER.WriteString(m_path);
	//	FILE_HELPER.WriteString(m_pathAndFilename);
	//}

	/**
	* \brief  Uses file helper
	* \date  12/12/2002
	*/
	//void CFilename::Load()
	//{
	//	ReleaseMemory();

	//	EsAssert(!m_filename);
	//	EsAssert(!m_path);
	//	EsAssert(!m_pathAndFilename);
	//	EsAssert(!m_filenameMinusExtension);

	//	DWORD32 saveVersion;
	//	FILE_HELPER.Read(&saveVersion);

	//	// Later on have to handle any later version:
	//	EsAssert(saveVersion==kFilenameVersion);

	//	m_filename=FILE_HELPER.ReadString();
	//	m_filenameMinusExtension=FILE_HELPER.ReadString();
	//	m_path=FILE_HELPER.ReadString();
	//	m_pathAndFilename=FILE_HELPER.ReadString();
	//}



	/**
	* \brief  Looks on disk to see if file exists
	* \date  30/06/2002
	*/
	bool CFilename::DoesFileExist() const
	{
		return DoesFileExist(m_pathAndFilename);
	}

	/**
	* \brief  Allows the file extension to be changed
	* \date  25/07/2002
	*/
	void CFilename::ChangeExtension(const char *newExt)
	{
		EsAssert(newExt);
		EsAssert(strlen(newExt)>0);

		// make sure there are no . (dots)
		int offset=0;
		while(newExt[offset]=='.')
			offset++;

		//size_t len=_tcslen(m_filenameMinusExtension)+_tcslen(newExt)+1;

		char buf[MAX_PATH];
		//_stprintf(buf,_T("%s.%s"),m_filenameMinusExtension,&newExt[offset]);
//ry		EsCheckSt(FORMATTED_STRING(buf,MAX_PATH*sizeof(char),_T("%s.%s"),m_filenameMinusExtension,&newExt[offset]));

		ChangeFilename(buf);
	}

	/**
	* \brief  Allows the file extension to be changed to type
	* \date  25/07/2002
	*/
	void CFilename::ChangeExtension(EFileType type)
	{
		char buf[MAX_PATH];
		if (GetExtensionFromFileType(type,buf,MAX_PATH))
			ChangeExtension(buf);
	}

	/**
	* \brief  Work out file type from extensions
	* \date  30/07/2002
	*/
	CFilename::EFileType CFilename::GetFileType() const
	{
		char *ext=ExtractExtensionFromPath(m_filename);
		EsAssert(ext);

		EFileType type=eFileTypeUnknown;

		for (int i=0;i<eNumFileTypes;i++)
		{
			if (strcmp(ext,gFileExtensions[i])==0)
			{
				type=(EFileType)i;
				SAFE_FREE(ext);
				return type;
			}
		}

		SAFE_FREE(ext);

		return type;
	}

	/**
	* \brief  Work out file type from extensions - as above but static for all
	* \date  27/10/2004
	*/
	CFilename::EFileType CFilename::GetFileTypeFromExtension(const char *ext)
	{
		EsAssert(ext);

		EFileType type=eFileTypeUnknown;

		for (int i=0;i<eNumFileTypes;i++)
		{
			if (strcmp(ext,gFileExtensions[i])==0)
			{
				type=(EFileType)i;
				return type;
			}
		}

		return type;
	}

	/**
	* \brief other way around - get the extension from the type
	* caller must pass in an array to hold the extension
	* \date  27/10/2004
	*/
	bool CFilename::GetExtensionFromFileType(CFilename::EFileType type,char *ext,size_t bufSize)
	{
		EsAssert(ext);

		if (type>=eNumFileTypes)
			return false;

		//_tcscpy(ext,gFileExtensions[type]);
//ry		EsCheckSt(COPY_STRING(ext,bufSize*sizeof(char),gFileExtensions[type]));

		return true;
	}
	/**
	* \brief
	* \date  09/08/2002
	*/
	void CFilename::Set(const CFilename &filename)
	{
		ReleaseMemory();

		m_filename=CMemory::DuplicateString(filename.GetFilename());
		m_path=CMemory::DuplicateString(filename.GetPath());
		m_pathAndFilename=CMemory::DuplicateString(filename.GetPathAndFilename());
		m_filenameMinusExtension=ExtractFilenameFromPathNoExtension(m_filename);
	}

	/**
	* \brief
	* \date  22/11/2002
	*/
	void CFilename::SetToUniqueTempFilename()
	{
		char *temp=GetUniqueTemporaryFilename();

		SetViaPathAndFilename(temp);

		SAFE_FREE(temp);
	}

	/**
	* \brief  Allows text to be added to filename part
	* \date  22/11/2002
	*/
	void CFilename::AppendToFilename(const char *text)
	{
		size_t len=strlen(m_filename)+strlen(text)+1;

		char *m_temp=new char[len];
		EsCheckAlloc(m_temp);

		char extension[MAX_PATH];
		GetExtension(extension,MAX_PATH);

		//_tcscpy(m_temp,m_filenameMinusExtension);
//ry		EsCheckSt(COPY_STRING(m_temp,len*sizeof(char),m_filenameMinusExtension));

		//_tcscat(m_temp,text);
//ry		EsCheckSt(CAT_STRING(m_temp,len*sizeof(char),text));
		//_tcscat(m_temp,extension);
//ry		EsCheckSt(CAT_STRING(m_temp,len*sizeof(char),extension));

		ChangeFilename(m_temp);

		delete[] m_temp;
	}

	/**
	* \brief  Since we dont hold extension var must copy into past char string
	* \date  22/11/2002
	*/
	void CFilename::GetExtension(char *ext,size_t bufSize) const
	{
		EsAssert(ext);

		char *buf=ExtractExtensionFromPath(m_filename);
		//_tcscpy(ext,buf);
//ry		EsCheckSt(StringCbCopy(ext,bufSize*sizeof(char),buf));

		SAFE_FREE(buf);
	}

	/**
	* \brief  Best explained by an example:
	* if filename path is c:\projects\uidata\new\
	* and passed in path is c:\projects\uidata
	* then we convert our path to new\
	* Note: path may end up as null
	* \date  21/12/2002
	*/
	bool CFilename::TrimPathToPath(const char *path)
	{
		// find first occurance of path in our path
		char *p=strstr(m_path,path);

		if (p)
		{
			size_t newLength=strlen(p)-strlen(path);

			char *newPath=(char*)malloc((newLength+1)*sizeof(char));
			EsCheckAlloc(newPath);
			char *pNewPath=newPath;

			p+=strlen(path);

			while(*p!=0)
				*pNewPath++=*p++;

			*pNewPath=0;

			ChangePath(newPath);

			SAFE_FREE(newPath);

			return true;
		}

		return false;
	}

	// Utility filename functions:
	/**
	* \brief  Taking a path trim it to just a filename,
	* Note: Caller must free returned pointer
	* \date  12/09/01
	*/
	char* CFilename::ExtractFilenameFromPath(const char * const passedPath)
	{
		//char path[MAX_PATH];
		char fname[MAX_PATH];
		char drv[MAX_PATH];
		char dir[MAX_PATH];
		char ext[MAX_PATH];

		CToolBox::SplitThePath(passedPath,drv,dir,fname,ext);

		char buf[MAX_PATH];

		//_stprintf(buf,_T("%s%s"),fname,ext);
//ry		if (EsCheckSt(FORMATTED_STRING(buf,MAX_PATH*sizeof(char),_T("%s%s"),fname,ext)))
		{
			// trying to track bug
//ry			EsOutput(_T("passed=%s drv=%s dir=%s fname=%s ext=%s buf=%s\n"),passedPath,drv,dir,fname,ext,buf);
		}

		return CMemory::DuplicateString(buf);
	}

	/**
	* \brief  As above but minus extensions
	* \date  22/06/2002
	*/
	char* CFilename::ExtractFilenameFromPathNoExtension(const char *const passedPath)
	{
		char fname[MAX_PATH];
		char drv[MAX_PATH];
		char dir[MAX_PATH];
		char ext[MAX_PATH];

		CToolBox::SplitThePath(passedPath,drv,dir,fname,ext);

		return CMemory::DuplicateString(fname);
	}

	/**
	* \brief
	* \date  21/06/2002
	*/
	char* CFilename::ExtractPathFromPathAndFilename(const char *pathAndFilename)
	{
		//char path[MAX_PATH];
		char fname[MAX_PATH];
		char drv[MAX_PATH];
		char dir[MAX_PATH];
		char ext[MAX_PATH];

		CToolBox::SplitThePath(pathAndFilename,drv,dir,fname,ext);

		char buf[MAX_PATH];

		//_stprintf(buf,_T("%s%s"),drv,dir);
//ry		if (EsCheckSt(FORMATTED_STRING(buf,MAX_PATH*sizeof(char),_T("%s%s"),drv,dir)))
		{
			// trying to track bug
//ry			EsOutput(_T("pathAndFilename=%s drv=%s dir=%s fname=%s ext=%s\n"),pathAndFilename,drv,dir,fname,ext);
		}

		return CMemory::DuplicateString(buf);
	}

	/**
	* \brief  Taking a path trim it to just its extension
	* Note: Caller must free returned pointer
	* \date  12/09/01
	*/

	char* CFilename::ExtractExtensionFromPath(const char *passedPath)
	{
		char fname[MAX_PATH];
		char drv[MAX_PATH];
		char dir[MAX_PATH];
		char ext[MAX_PATH];

		CToolBox::SplitThePath(passedPath,drv,dir,fname,ext);

		return CMemory::DuplicateString(ext);
	}

	/**
	* \brief  See if a file exists, must pass full path.
	* \date  03/05/2002
	*/
	bool CFilename::DoesFileExist(const char *const filename)
	{
#if defined(CAANOO)
//ry		EsOutput(_T("DoesFileExist: not implemented on WINCE"));
		return true;
#else
		bool result = ( _access(filename, 0) != -1);
		return result;
#endif
	}

	/**
	* \brief  See if a file exists
	* \date  03/05/2002
	*/
	bool CFilename::DoesFileExist(const CFilename &filename)
	{
		#if defined(CAANOO)
//ry		EsOutput(_T("DoesFileExist: not implemented on WINCE"));
		return true;
		#else
		bool result = ( _access(filename.GetPathAndFilename(), 0) != -1);
		return result;
		#endif
	}

	/**
	* \brief  See if a path exists
	* \date  29/01/2005
	*/
	bool CFilename::DoesPathExist(const char *const path)
	{
		// Can use DoesFileExist for this
		return DoesFileExist(path);
	}

	/**
	* \brief  return a pointer to a unique temp filename, caller must free
	* Uses KJD_ prefex so can clean up undeleted temporary files by looking for KJD_n
	* \date  16/06/2002
	*/
	char* CFilename::GetUniqueTemporaryFilename()
	{
		#if defined(WINCE)
		EsError(_T("GetUniqueTemporaryFilename: not implemented on WINCE"));
		return 0;
		#else
		return 0;//ry _ttempnam(_T("c:\\"),_T("KJD_"));
		#endif

		//could use:
		//GetTempFileName(
	}



	/**
	* \brief  Copy file from fromPath to toPath
	* \date  12/07/2002
	*/
	//#define BUFSIZE	2048
	//bool CFilename::CopyFileToFile(const char *fromPath, const char *toPath)
	//{
	//	EsOutput(_T("Copying file %s to file %s\n"),fromPath,toPath);

	//	if (!DoesFileExist(fromPath))
	//	{
	//		EsOutput(_T("Copy FAILED source file does not exist\n"));
	//		return false;
	//	}

	//	EsAssert(fromPath);
	//	EsAssert(toPath);
	//	if (_tcslen(fromPath)==_tcslen(toPath) && CMP_STRING(fromPath,toPath)!=0)
	//	{
	//		EsOutput(_T("Trying to copy a file to itself!\n"));
	//		return false;
	//	}

	//	bool ret=CopyFile(fromPath,toPath,false);
	//	if (ret==0)
	//	{
	//		EsOutput(_T("Copy FAILED Error Code %d\n"),GetLastError());
	//	}

	//	return (ret==true);
	//}

	/**
	* \brief
	* \date  28/07/2002
	*/
	//bool CFilename::CopyFileToFile(CFilename *from, CFilename *to)
	//{
	//	EsAssert(to);
	//	EsAssert(from);

	//	return CopyFileToFile(from->GetPathAndFilename(),to->GetPathAndFilename());
	//}

	/**
	* \brief for legacy formats sometimes need in 8:3 format
	* \param uniqueId - required so trimming does not create duplicates
	* \date 10/12/2004
	*/
	void CFilename::ConvertTo83Format(int uniqueId)
	{
		// Use just the first 6 chars of the filename+the unique id
		char filenameOnly[7];
//ry		char uniqueBuf[3];

		strncpy(filenameOnly,m_filenameMinusExtension,6);
		filenameOnly[6]=0;

		//int len=_stprintf(uniqueBuf,_T("%d"),uniqueId);
//ry		EsCheckSt(FORMATTED_STRING(uniqueBuf,3*sizeof(char),"%d",uniqueId));

		//	EsAssert(len<3);

		//_tcscat(filenameOnly,uniqueBuf);
//ry		EsCheckSt(CAT_STRING(filenameOnly,7*sizeof(char),uniqueBuf));

		ChangeFilenameOnly(filenameOnly);
	}

/**
* \brief  Make a relative path, only works if path is a subdirectory of current directory
* \date  27/03/2002
*/
bool CFilename::MakePathRelative(const char *path,char *newPath)
{
	EsAssert(path);
	EsAssert(newPath);

	char m_curDir[MAX_PATH];
	CToolBox::GetRootDirectory(MAX_PATH,m_curDir);

	// If the passed path is not a subdirectory of current this fn does not work
	// This can be detected by checking that the subdir is the first part of the newPath
	// Note: the +1 is used to skip the drive letter as it is often in a different case and
	// changing it when using char is tricky - this works but may not be best way!
	if (strncmp(path+1,m_curDir+1,strlen(m_curDir)-2)!=0)
	{
		//EsOutput(_T("MakePathRelative: The directory %s is not a subdirectory of current %s\n"),
		//	path,m_curDir);

		// Have to return original?
		//_tcscpy(newPath,path);
//ry		EsCheckSt(COPY_STRING(newPath,MAX_PATH,path));
		return false;
	}

	size_t len=strlen(path);
	// Get to start

	bool same=true;
	size_t count=0;
	while(same && count<len)
	{
		int a=path[count];
		int b=m_curDir[count];

		// note: need to ignore case in second one as had problem where drive letter was c in one and C in the
		// other - this is not the best way of doing it!
//#pragma TODO("I need to write a toupper for char strings")
		if (a!=b && a!=b-32)
			same=false;
		count++;
	}

	EsAssert(!same);

	// copy
	size_t newCount=0;
	while(count<len)
	{
		newPath[newCount]=path[count];
		count++;
		newCount++;
	}
	newPath[newCount]=0;

	return true;
}

/**
* \brief caller must free
* \date  26/10/04
*/
char* CFilename::ConvertRelativeToFull(const char *path)
{
	char m_curDir[MAX_PATH];
	CToolBox::GetRootDirectory(MAX_PATH,m_curDir);

	char buf[MAX_PATH];
	//_stprintf(buf,_T("%s\\%s"),m_curDir,path);
//ry	EsCheckSt(FORMATTED_STRING(buf,MAX_PATH,"%s\\%s",m_curDir,path));

	char *fullPath=Utility::CMemory::DuplicateString(buf);
	return fullPath;
}
}

