#pragma once

#include "Common.h"
#include "Filename.h"

#include "Vector3.h"

// The main keywords that start a data chunk
enum ELevelChunkKeywords
{
	eNoChunk=0,
	eGraphicEntity,
	eBulletEntity,
	eExplosionEntity,
	eEnemyEntity,
	eRouteData,
	eWaveData,
	eNumLevelChunkKeywords
};

// eGraphicEntity
enum EGraphicEntityChunk
{
	eGraphicEntityFilename,
	eGraphicEntityName,
	eGraphicEntityFrames,
	eGraphicEntityFrameSize,
	eGraphicEntityFrameSpeed,
	eGraphicEntityFrameMode,
	eNumGraphicEntity
};

struct TGraphicEntityChunk
{
	const Utility::CFilename *filename;
	char *name;
	int frames;
	int frameSize;
	float frameSpeed;
	int mode; // 0=loop, 1=reverse
};

// eBulletEntity
enum EBulletEntityChunk
{
	eBulletName,
	eBulletGraphicName,
	eBulletExplosionName,
	eBulletSpeed,
	eBulletDamage,
	eBulletMulti,
	eNumBulletEntity
};

struct TBulletEntityChunk
{
	char *name;
	char *graphicName;
	char *explosionName;
	float speed;
	int damage;
	int multi;
};

// eExplosionEntity
enum EExplosionEntityChunk
{
	eExplosionName,
	eExplosionGraphicName, // later could have a few?
	eNumExplosionEntity
};

struct TExplosionEntityChunk
{
	char *name;
	char *graphicName;
};

// eEnemyEntity
enum EEnemyEntityChunk
{
	eEnemyEntityName,
	eEnemyEntityGraphicName,
	eEnemyEntityBulletName,
	eEnemyEntityExplosionName,
	eEnemyEntityCollisionCost,
	eEnemyEntityHealth,
	eEnemyEntitySpeed,
	eEnemyEntityRotationSpeed,
	eEnemyEntityOnlyFireForward,
	eEnemyEntityFireRate,
	eEnemyEntityChanceOfFiring,
	eEnemyEntityChunkScore,
	eNumEnemyEntity
};

struct TEnemyEntityChunk
{
	char *entityName;
	char *graphicName;
	char *bulletName;
	char *explosionName;
	int collisionCost;
	int health;
	float moveSpeed;
	float rotSpeed;
	int onlyFireForward; // 1 or 0
	int fireRate; // ms
	int chanceOfFiring;
	int score;
};

// eEnemyEntity
enum ERouteChunk
{
	eRouteChunkName,
	eRouteNumWayPoints,
	eRouteWayPoint,
	eNumRoute
};

struct TRouteChunk
{
	char *routeName;
	int numWayPoints;
	CVector3 *wayPoints;

	int currentPoint; // temp counter used during load
};

struct TWaveChunk
{
	char *waveName;
	int entryRate;
	char *routeName;
	int numEntities;

	int currentEntity; // temp counter used during load
};

#define kMaxLineLength 1024

// Unavoidable globals
extern char gChunkKeywords[eNumLevelChunkKeywords][kMaxLineLength];
extern char gGraphicsKeywords[eNumGraphicEntity][kMaxLineLength];
extern char gBulletKeywords[eNumBulletEntity][kMaxLineLength];
extern char gEnemyEntityKeywords[eNumEnemyEntity][kMaxLineLength];
extern char gExplosionKeywords[eNumExplosionEntity][kMaxLineLength];
extern char gRouteKeywords[eNumRoute][kMaxLineLength];

