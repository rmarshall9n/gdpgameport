SDL Shooter Game Demo
=========================================
Author: Ryan Marshall
Size: 5MB

This game has been a direct port of Keith Ditchburns 2005 HAPI Game Demo.



Installation
=========================================
1. Extract the Shooter - Demo.zip file directly to a SD card
2. Insert the SD card into the CAANOO
3. You will find the launcher in the �Game� group of the main menu.



Control Scheme
=========================================
Caanoo
Movement		-	Joystick
Fire / Start		-	X
Toggle FPS		-	Left Bumper
Toggle Invincibility	-	Help 2
Pause			-	Help 1
Quit			-	Home

PC
Movement		-	Arrow Keys
Fire			-	Return
Toggle FPS		-	F2
Toggle Invincibility	-	F5
Pause			-	F1
Quit			-	Escape
Start			-	Space